// ==UserScript==
// @id             iitc-plugin-splitOrnaments-AbbadonPrime
// @name           IITC plugin: Split Anomaly ornaments and beacons into their own layers for Abbadon Prime
// @author         Machap
// @category       Layer
// @version        0.1.0.20190501.0000002
// @downloadURL    https://ingress.love/iitc-ja/nya/split_ornaments_apk.user.js
// @updateURL      https://ingress.love/iitc-ja/nya/split_ornaments_apk.meta.js
// @description    [2019-05-01] Split Anomaly ornaments and beacons into their own layers for Abbadon Prime
// @include        https://ingress.com/intel*
// @include        http://ingress.com/intel*
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          https://intel.ingress.com/*
// @include        https://intel.ingress.com/*
// @match          http://*.ingress.com/intel*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    if (typeof window.plugin !== 'function') window.plugin = function () { };


    // PLUGIN START ////////////////////////////////////////////////////////
    window.plugin.splitOrnaments_apk = function () { };

    var STORAGE_KEY = 'plugin_ornament_splitlayer';
    var DEFAULT_AP4 = 5;
    var DEFAULT_FRACKER = 5;
    var DEFAULT_LS = 5;
    var DEFAULT_VOLATILE = 5;
    var DEFAULT_ARTIFACT = 5;
    var ORNAMENT_LAYER_SPLIT_FRACKER,ORNAMENT_LAYER_SPLIT_AP4,ORNAMENT_LAYER_SPLIT_VOLATILE,ORNAMENT_LAYER_SPLIT_LS,ORNAMENT_LAYER_SPLIT_ARTIFACT;

    //ローカルストレージからデータ読み出し
    window.plugin.splitOrnaments_apk.loadSettings = function() {
        if (!localStorage[STORAGE_KEY]) {
            window.plugin.splitOrnaments_apk.settings.fracker = DEFAULT_FRACKER;
            window.plugin.splitOrnaments_apk.settings.ap4 = DEFAULT_AP4;
            window.plugin.splitOrnaments_apk.settings.ls = DEFAULT_LS;
            window.plugin.splitOrnaments_apk.settings.volatile = DEFAULT_VOLATILE;
            window.plugin.splitOrnaments_apk.settings.artifact = DEFAULT_ARTIFACT;
            window.plugin.splitOrnaments_apk.saveSettings();
        } else {
            window.plugin.splitOrnaments_apk.settings = JSON.parse(localStorage[STORAGE_KEY]);

            window.plugin.splitOrnaments_apk.settings.fracker = parseInt(window.plugin.splitOrnaments_apk.settings.fracker);
            window.plugin.splitOrnaments_apk.settings.ap4 = parseInt(window.plugin.splitOrnaments_apk.settings.ap4);
            window.plugin.splitOrnaments_apk.settings.volatile = parseInt(window.plugin.splitOrnaments_apk.settings.volatile);
            window.plugin.splitOrnaments_apk.settings.ls = parseInt(window.plugin.splitOrnaments_apk.settings.ls);;
            window.plugin.splitOrnaments_apk.settings.artifact = parseInt(window.plugin.splitOrnaments_apk.settings.artifact);;
        }

        var datacheck = [ 2, 5, 7, 10 ];
        if (datacheck.indexOf(window.plugin.splitOrnaments_apk.settings.fracker) === -1) {
            window.plugin.splitOrnaments_apk.settings.fracker = DEFAULT_FRACKER;
        }
        if (datacheck.indexOf(window.plugin.splitOrnaments_apk.settings.ap4) === -1) {
            window.plugin.splitOrnaments_apk.settings.ap4 = DEFAULT_AP4;
        }
        if (datacheck.indexOf(window.plugin.splitOrnaments_apk.settings.volatile) === -1) {
            window.plugin.splitOrnaments_apk.settings.volatile = DEFAULT_VOLATILE;
        }
        if (datacheck.indexOf(window.plugin.splitOrnaments_apk.settings.ls) === -1) {
            window.plugin.splitOrnaments_apk.settings.ls = DEFAULT_LS;
        }
        if (datacheck.indexOf(window.plugin.splitOrnaments_apk.settings.artifact) === -1) {
            window.plugin.splitOrnaments_apk.settings.artifact = DEFAULT_ARTIFACT;
        }

        window.ORNAMENT_LAYER_SPLIT_FRACKER = window.plugin.splitOrnaments_apk.settings.fracker / 10;
        window.ORNAMENT_LAYER_SPLIT_VOLATILE = window.plugin.splitOrnaments_apk.settings.volatile / 10;
        window.ORNAMENT_LAYER_SPLIT_AP4 = window.plugin.splitOrnaments_apk.settings.ap4 / 10;
        window.ORNAMENT_LAYER_SPLIT_LS = window.plugin.splitOrnaments_apk.settings.ls / 10;
        window.ORNAMENT_LAYER_SPLIT_ARTIFACT = window.plugin.splitOrnaments_apk.settings.artifact / 10;
    };

    //ローカルストレージ保存
    window.plugin.splitOrnaments_apk.saveSettings = function() {
        localStorage[STORAGE_KEY] = JSON.stringify(window.plugin.splitOrnaments_apk.settings);
    };
    //透明度の適用
    window.plugin.splitOrnaments_apk.updateCSS = function() {
        $("img[src$=\"/peFRACK.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_FRACKER);
        $("img[src$=\"/ap4.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_AP4);
        $("img[src$=\"/ap1_v.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_VOLATILE);
        $("img[src$=\"/ap6_end.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_ARTIFACT);
        $("img[src$=\"/ap2_start.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_LS);
        $("img[src$=\"/ap5_start.png\"]").css("opacity",window.ORNAMENT_LAYER_SPLIT_LS);
        console.log('splitOrnaments-dp CSS updated.');
    };

    //ダイアログの表示
    window.plugin.splitOrnaments_apk.openDialog = function() {
        var contents = [
            '<table>',
            '<tr>',
            '<td>Fracker</td><td>',
            '<select name="opacity-fracker">',
            '<option value="2"', (window.plugin.splitOrnaments_apk.settings.fracker == 2) ? ' selected' : '', '>0.2</option>',
            '<option value="5"', (window.plugin.splitOrnaments_apk.settings.fracker == 5) ? ' selected' : '', '>0.5</option>',
            '<option value="7"', (window.plugin.splitOrnaments_apk.settings.fracker == 7) ? ' selected' : '', '>0.7</option>',
            '<option value="10"', (window.plugin.splitOrnaments_apk.settings.fracker == 10) ? ' selected' : '', '>1.0</option>',
            '</td></tr>',
            '<tr><td>&nbsp;</td><td></td></tr>',
            '<tr>',
            '<td>Ornament</td><td>',
            '<select name="opacity-ap4">',
            '<option value="2"', (window.plugin.splitOrnaments_apk.settings.ap4 == 2) ? ' selected' : '', '>0.2</option>',
            '<option value="5"', (window.plugin.splitOrnaments_apk.settings.ap4 == 5) ? ' selected' : '', '>0.5</option>',
            '<option value="7"', (window.plugin.splitOrnaments_apk.settings.ap4 == 7) ? ' selected' : '', '>0.7</option>',
            '<option value="10"', (window.plugin.splitOrnaments_apk.settings.ap4 == 10) ? ' selected' : '', '>1.0</option>',
            '</td></tr>',
            '<tr><td>&nbsp;</td><td></td></tr>',
            '<tr>',
            '<td>Volatile</td><td>',
            '<select name="opacity-volatile">',
            '<option value="2"', (window.plugin.splitOrnaments_apk.settings.volatile == 2) ? ' selected' : '', '>0.2</option>',
            '<option value="5"', (window.plugin.splitOrnaments_apk.settings.volatile == 5) ? ' selected' : '', '>0.5</option>',
            '<option value="7"', (window.plugin.splitOrnaments_apk.settings.volatile == 7) ? ' selected' : '', '>0.7</option>',
            '<option value="10"', (window.plugin.splitOrnaments_apk.settings.volatile == 10) ? ' selected' : '', '>1.0</option>',
            '</td></tr>',
            '<tr><td>&nbsp;</td><td></td></tr>',
            '<tr>',
            '<td>Research(RES/ENL)</td><td>',
            '<select name="opacity-ls">',
            '<option value="2"', (window.plugin.splitOrnaments_apk.settings.ls == 2) ? ' selected' : '', '>0.2</option>',
            '<option value="5"', (window.plugin.splitOrnaments_apk.settings.ls == 5) ? ' selected' : '', '>0.5</option>',
            '<option value="7"', (window.plugin.splitOrnaments_apk.settings.ls == 7) ? ' selected' : '', '>0.7</option>',
            '<option value="10"', (window.plugin.splitOrnaments_apk.settings.ls == 10) ? ' selected' : '', '>1.0</option>',
            '</td></tr>',
            '<tr><td>&nbsp;</td><td></td></tr>',
            '<tr>',
            '<td>Artifact Drop</td><td>',
            '<select name="opacity-artifact">',
            '<option value="2"', (window.plugin.splitOrnaments_apk.settings.artifact == 2) ? ' selected' : '', '>0.2</option>',
            '<option value="5"', (window.plugin.splitOrnaments_apk.settings.artifact == 5) ? ' selected' : '', '>0.5</option>',
            '<option value="7"', (window.plugin.splitOrnaments_apk.settings.artifact == 7) ? ' selected' : '', '>0.7</option>',
            '<option value="10"', (window.plugin.splitOrnaments_apk.settings.artifact == 10) ? ' selected' : '', '>1.0</option>',
            '</td></tr>',
            '</table>'
        ].join('');

        var dialogBox = '<div id="ornamentSplit-settings">' + contents + '</div>';

        var closeCallback = function() {
            window.plugin.splitOrnaments_apk.settings.fracker = parseInt($('[name=opacity-fracker]').val());
            window.plugin.splitOrnaments_apk.settings.ap4 = parseInt($('[name=opacity-ap4]').val());
            window.plugin.splitOrnaments_apk.settings.volatile = parseInt($('[name=opacity-volatile]').val());
            window.plugin.splitOrnaments_apk.settings.ls = parseInt($('[name=opacity-ls]').val());
            window.plugin.splitOrnaments_apk.settings.artifact = parseInt($('[name=opacity-artifact]').val());
            window.plugin.splitOrnaments_apk.saveSettings();

            window.ORNAMENT_LAYER_SPLIT_FRACKER = window.plugin.splitOrnaments_apk.settings.fracker /10;
            window.ORNAMENT_LAYER_SPLIT_VOLATILE = window.plugin.splitOrnaments_apk.settings.volatile /10;
            window.ORNAMENT_LAYER_SPLIT_AP4 = window.plugin.splitOrnaments_apk.settings.ap4 /10;
            window.ORNAMENT_LAYER_SPLIT_LS = window.plugin.splitOrnaments_apk.settings.ls /10;
            window.ORNAMENT_LAYER_SPLIT_ARTIFACT = window.plugin.splitOrnaments_apk.settings.artifact /10;
            window.plugin.splitOrnaments_apk.updateCSS();

        };

        dialog({
            html: dialogBox,
            title: 'Anomary Ornament split Settings',
            closeCallback: closeCallback
        });
    }

    // 初期設定
    window.plugin.splitOrnaments_apk.setup = function () {

        window.plugin.splitOrnaments_apk.loadSettings();
        //設定ファイアログのバインド
        var openDialog = '<a onclick="window.plugin.splitOrnaments_apk.openDialog();return false;">Ornament Split opt</a>';
        $('#toolbox').append(openDialog);

        // iitc-mobileなどtestビルド版が使えない場合の存在チェックとレイヤ追加
        if (!window.ornaments._frackers) {
            window.ornaments._frackers = L.layerGroup();
            window.addLayerGroup('Frackers', window.ornaments._frackers, true);
        }
        if (!window.ornaments._beacons) {
            window.ornaments._beacons = L.layerGroup();
            window.addLayerGroup('Beacons', window.ornaments._beacons, true);
        }

        // それぞれのレイヤー追加
        window.ornaments._ornamentLayer = L.layerGroup();
        window.ornaments._volatileLayer = L.layerGroup();
        window.ornaments._researchresLayer = L.layerGroup();
        window.ornaments._researchenlLayer = L.layerGroup();
        window.ornaments._artifactLayer = L.layerGroup();
        window.addLayerGroup('Ornament', window.ornaments._ornamentLayer, true);
        window.addLayerGroup('Volatile', window.ornaments._volatileLayer, true);
        window.addLayerGroup('Research(RES)', window.ornaments._researchresLayer, true);
        window.addLayerGroup('Research(ENL)', window.ornaments._researchenlLayer, true);
        window.addLayerGroup('Artifact', window.ornaments._artifactLayer, true);

        //move already loaded items to the new layers
        $.each(window.ornaments._portals, function (guid, ornamentList) {
            $.each(ornamentList, function (index, marker) {
                var ornament = window.portals.options.data.ornaments[index];

                var layer = null; // その他のオーナメント
                var opaticy = 0.5;
                switch(ornament) {
                    case 'ap4':
                        layer = window.ornaments._ornamentLayer; // Ornament
                        break;
                    case 'ap1_v':
                        layer = window.ornaments._volatileLayer; // Volatile
                        break;
                    case 'ap2_start':
                        layer = window.ornaments._researchresLayer; // RES research
                        break;
                    case 'ap5_start':
                        layer = window.ornaments._researchenlLayer; // ENL research
                        break;
                    case 'ap6_end':
                        layer = window.ornaments._artifactLayer; // artifact
                        break;
                    case 'peFRACK':
                        layer = window.ornaments._frackers; // fracker
                        break;
                }
                if (ornament.startsWith("pe") && ornament != "peFRACK") { layer = window.ornaments._beacons; }// beacons
                if (layer != null) { // その他のオーナメントは削除
                    marker.addTo(layer);
                    window.ornaments._layer.removeLayer(marker);
                }
            });
        });

        // IITCのaddPortalメソッドのオーバーライド
        window.ornaments.addPortal = function (portal) {
            var guid = portal.options.guid;

            window.ornaments.removePortal(portal);

            var size = window.ornaments.OVERLAY_SIZE;
            var latlng = portal.getLatLng();

            if (portal.options.data.ornaments) {
                window.ornaments._portals[guid] = portal.options.data.ornaments.map(function (ornament) {
                    var layer = window.ornaments._layer; //anomaly ornaments

                    var icon = L.icon({
                        iconUrl: "//commondatastorage.googleapis.com/ingress.com/img/map_icons/marker_images/" + ornament + ".png",
                        iconSize: [size, size],
                        iconAnchor: [size / 2, size / 2],
                        className: 'no-pointer-events'
                    });

                    switch(ornament) {
                        case 'ap4':
                            layer = window.ornaments._ornamentLayer; // Ornament
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_AP4 }).addTo(layer);
                            break;
                        case 'ap1_v':
                            layer = window.ornaments._volatileLayer; // Volatile
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_VOLATILE }).addTo(layer);
                            break;
                        case 'ap2_start':
                            layer = window.ornaments._researchresLayer; // RES research
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_LS }).addTo(layer);
                            break;
                        case 'ap5_start':
                            layer = window.ornaments._researchenlLayer; // ENL research
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_LS }).addTo(layer);
                            break;
                        case 'ap6_end':
                            layer = window.ornaments._artifactLayer; // artifact
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_ARTIFACT }).addTo(layer);
                            break;
                        case 'peFRACK':
                            layer = window.ornaments._frackers; // fracker
                            return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ORNAMENT_LAYER_SPLIT_FRACKER }).addTo(layer);
                            break;
                    }
                    if (ornament.startsWith("pe") && ornament != "peFRACK") { layer = window.ornaments._beacons; }// beacons
                    return L.marker(latlng, { icon: icon, clickable: false, keyboard: false, opacity: window.ornaments.OVERLAY_OPACITY }).addTo(layer);
                });
            }
        }

        // IITCのremovePortalメソッドのオーバーライド
        window.ornaments.removePortal = function (portal) {
            var guid = portal.options.guid;
            if (window.ornaments._portals[guid]) {
                window.ornaments._portals[guid].forEach(function (marker) {
                    window.ornaments._layer.removeLayer(marker);
                    window.ornaments._beacons.removeLayer(marker);
                    window.ornaments._frackers.removeLayer(marker);
                    window.ornaments._ornamentLayer.removeLayer(marker);
                    window.ornaments._volatileLayer.removeLayer(marker);
                    window.ornaments._researchresLayer.removeLayer(marker);
                    window.ornaments._researchenlLayer.removeLayer(marker);
                });
                delete window.ornaments._portals[guid];
            }
        }
        console.log('splitOrnaments-dp loaded.');
    };

    window.plugin.splitOrnaments_apk.settings = {};
    var setup = window.plugin.splitOrnaments_apk.setup;

    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') {
        setup();
    }

    // PLUGIN END ////////////////////////////////////////////////////////
} // WRAPPER END ////////////////////////////////////////////////////////

var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
