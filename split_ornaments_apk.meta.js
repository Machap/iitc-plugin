// ==UserScript==
// @id             iitc-plugin-splitOrnaments-AbbadonPrime
// @name           IITC plugin: Split Anomaly ornaments and beacons into their own layers for Abbadon Prime
// @author         Machap
// @category       Layer
// @version        0.1.0.20190501.0000002
// @downloadURL    https://ingress.love/iitc-ja/nya/split_ornaments_apk.user.js
// @updateURL      https://ingress.love/iitc-ja/nya/split_ornaments_apk.meta.js
// @description    [2019-05-01] Split Anomaly ornaments and beacons into their own layers for Abbadon Prime
// @include        https://ingress.com/intel*
// @include        http://ingress.com/intel*
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          https://intel.ingress.com/*
// @include        https://intel.ingress.com/*
// @match          http://*.ingress.com/intel*
// @grant          none
// ==/UserScript==
