// ==UserScript==
// @id             iitc-plugin-custom-grid@ja
// @name           IITC-ja Plugin: Simple Player tracker
// @category       Layer
// @version        0.11.1.20190208.21101
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://a-detection-algorithm.site/plugins/simple-player-tracker-ja.meta.js
// @downloadURL    https://a-detection-algorithm.site/plugins/simple-player-tracker-ja.user.js
// @updateURLorg      https://azuki.tk/plugin/simple-player-tracker.user.js
// @downloadURLorg    https://azuki.tk/plugin/simple-player-tracker.user.js
// @description    [iitc-ja-2019-01-24] Draw trails for the path a user took onto the map based on status messages in COMMs. Uses up to three hours of data. Does not request chat data on its own, even if that would be useful.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    //PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
    //(leaving them in place might break the 'About IITC' page or break update checks)
    plugin_info.buildName = 'iitc';
    plugin_info.dateTimeVersion = '20190321.00000';
    plugin_info.pluginId = 'grid-ex';
    //END PLUGIN AUTHORS NOTE



    // PLUGIN START ////////////////////////////////////////////////////////

    window.GRID_EX_PROJECT_ID = "";
    window.GRID_EX_DEFINITION = [];

    window.GRID_EX_TOP_LATITUDE = 0;
    window.GRID_EX_LEFT_LONGITUDE = 0;

    window.GRID_EX_NUM_COLS = 0;
    window.GRID_EX_NUM_ROWS = 0;

    window.GRID_EX_GRID_LENGTH = 0;

    window.GRID_EX_SHOW_ORNAMENT = false;

    window.GRID_EX_ALPHABETS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    // use own namespace for plugin
    window.plugin.grid_ex = function() {};

    var portals = [];
    var portalLabels = [];
    var searchedLabelIndicator = null;

    var showPortalMaxZoom = 15;
    var portalNameButton = null;

    var showGridMaxZoom = 14;

    var isGrid250Visible = true;

    var setup = function() {

        $.ajaxSetup({
            cache: false
        });

        window.plugin.grid_ex.setupCSS();

        window.$('#toolbox').append(' <a onclick="window.plugin.grid_ex.openOptDialog()" title="Change Grid Settings">Grid EX</a>');

        window.$('head').append('<style>' +
            '#grid_exSetbox{text-align: center; }' +
            '#grid_exSetbox a.disabled, #bkmrksSetbox a.disabled:hover { border-color: #666; color: #666; text-decoration: none; }' +
            '#grid_exSetbox a { background: rgba(8,48,78,.9); border: 1px solid #ffce00; color: #ffce00; display: block; margin: 10px auto; padding: 3px 0; text-align: center; width: 80%; }' +
            '</style>');


        if (window.localStorage) {
            try {
                window.GRID_EX_PROJECT_ID = window.localStorage.getItem('GRID_EX_PROJECT_ID') || window.GRID_EX_PROJECT_ID;
            }
            catch(e) { return; }
        }

        if (window.GRID_EX_PROJECT_ID == null) {
            return;
        }

        if (!window.readCookie('GRID_EX_SHOW_PORTAL_NO')) window.writeCookie('GRID_EX_SHOW_PORTAL_NO', '1');

        window.plugin.grid_ex.gridLayerGroup = new L.LayerGroup();
        window.addLayerGroup('Grid EX - 500m (Black)', window.plugin.grid_ex.gridLayerGroup, false);

        window.plugin.grid_ex.gridLayerGroup_250 = new L.LayerGroup();
        window.addLayerGroup('Grid EX - 250m (Blue)', window.plugin.grid_ex.gridLayerGroup_250, true);

        window.plugin.grid_ex.gridLayerGroup_125 = new L.LayerGroup();
        window.addLayerGroup('Grid EX - 125m (Red)', window.plugin.grid_ex.gridLayerGroup_125, true);

        window.plugin.grid_ex.gridPortalNoLayerGroup = new L.LayerGroup();
        window.addLayerGroup('Grid EX - Portal No', window.plugin.grid_ex.gridPortalNoLayerGroup, true);

        window.plugin.grid_ex.gridBlockNoLayerGroup = new L.LayerGroup();
        map.addLayer(window.plugin.grid_ex.gridBlockNoLayerGroup);
        //window.addLayerGroup('Grid EX - Block No', window.plugin.grid_ex.gridBlockNoLayerGroup, true);

        window.plugin.grid_ex.portalNameLayerGroup = new L.LayerGroup();
        map.addLayer(window.plugin.grid_ex.portalNameLayerGroup);

        // window.plugin.grid_ex.portalLabelDummyLayerGroup = new L.LayerGroup();
        // window.addLayerGroup('Grid EX - Portal Name', plugin.grid_ex.portalLabelDummyLayerGroup, true);
        map.on('layeradd', function(obj) {
            if (obj.layer === window.plugin.grid_ex.gridLayerGroup_250) {
                isGrid250Visible = true;
                window.plugin.grid_ex.drawGrid(4, window.plugin.grid_ex.gridLayerGroup_125);
            }
            if (obj.layer === window.plugin.grid_ex.gridPortalNoLayerGroup) {
                window.writeCookie('GRID_EX_SHOW_PORTAL_NO', '1');
                window.plugin.grid_ex.resetPortalLabel();
            }
        });
        map.on('layerremove', function(obj) {
            if(obj.layer === window.plugin.grid_ex.gridLayerGroup_250) {
                isGrid250Visible = false;
                window.plugin.grid_ex.drawGrid(4, window.plugin.grid_ex.gridLayerGroup_125);
            }
            if (obj.layer === window.plugin.grid_ex.gridPortalNoLayerGroup) {
                window.writeCookie('GRID_EX_SHOW_PORTAL_NO', '0');
                window.plugin.grid_ex.resetPortalLabel();
            }
        });

        window.plugin.grid_ex.polygonLayerGroup = new L.LayerGroup();
        window.plugin.grid_ex.drawPolygon();
        window.addLayerGroup('Grid EX - Polygon', plugin.grid_ex.polygonLayerGroup, false);

        window.plugin.grid_ex.createLabelSearchButton();

        if (map.getZoom() > showPortalMaxZoom) {
            window.plugin.grid_ex.createPortalNameButton();
        }

        $.getJSON('https://nvglab.net/ingress/data/' + window.GRID_EX_PROJECT_ID + '_l.json' , function(data) {

            window.GRID_EX_TOP_LATITUDE = Number(data.definition.lat);
            window.GRID_EX_LEFT_LONGITUDE = Number(data.definition.lng);
            window.GRID_EX_NUM_COLS = Number(data.definition.cols);
            window.GRID_EX_NUM_ROWS = Number(data.definition.rows);
            window.GRID_EX_GRID_LENGTH = Number(data.definition.grid_length);
            window.GRID_EX_SHOW_ORNAMENT = Boolean(Number(data.definition.ornament));

            if (map.getZoom() > showGridMaxZoom) {
                window.plugin.grid_ex.drawGridBlockNo();
                window.plugin.grid_ex.resetPortalLabel();
                window.plugin.grid_ex.drawGrid(1, window.plugin.grid_ex.gridLayerGroup);
                window.plugin.grid_ex.drawGrid(2, window.plugin.grid_ex.gridLayerGroup_250);
                window.plugin.grid_ex.drawGrid(4, window.plugin.grid_ex.gridLayerGroup_125);
            }

            $.each(data.portals, function(i, portal){
                portals.push(portal);
            });

            addHook('portalAdded', window.plugin.grid_ex.portalAdded);
            addHook('portalRemoved', window.plugin.grid_ex.portalRemoved);

            map.on('moveend', function() {
                window.plugin.grid_ex.mapMoved();
            });

            window.plugin.grid_ex.drawPortalLabel();
        });

        addHook('mapDataRefreshEnd', window.plugin.grid_ex.drawPortalLabel);

        map.on('zoomend', function() {
            var zoom = map.getZoom();
            //console.log('zoom: ' + zoom);

            if (zoom <= showGridMaxZoom) {
                window.plugin.grid_ex.gridLayerGroup.clearLayers();
                window.plugin.grid_ex.gridLayerGroup_250.clearLayers();
                window.plugin.grid_ex.gridLayerGroup_125.clearLayers();
                window.plugin.grid_ex.gridBlockNoLayerGroup.clearLayers();
                window.plugin.grid_ex.gridPortalNoLayerGroup.clearLayers();
                window.plugin.grid_ex.portalNameLayerGroup.clearLayers();
            } else {
                window.plugin.grid_ex.drawGridBlockNo();
                window.plugin.grid_ex.resetPortalLabel();
                window.plugin.grid_ex.drawGrid(1, window.plugin.grid_ex.gridLayerGroup);
                window.plugin.grid_ex.drawGrid(2, window.plugin.grid_ex.gridLayerGroup_250);
                window.plugin.grid_ex.drawGrid(4, window.plugin.grid_ex.gridLayerGroup_125);
            }

            if (zoom <= showPortalMaxZoom) {
                if (portalNameButton) {
                    window.map.removeControl(portalNameButton);
                    portalNameButton = null;
                }
            } else {
                if (!portalNameButton) window.plugin.grid_ex.createPortalNameButton();
            }

        });
    };

    window.plugin.grid_ex.setupCSS = function() {
        $("<style>")
            .prop("type", "text/css")
            .html("portal_label {"+
                "font-size: 1.1em;"+
                //"color: blue;"+
                "text-shadow: 1px 1px 1px white,"+
                "-1px 1px 1px white,"+
                "1px -1px 1px white,"+
                "-1px -1px 1px white;"+
                "}"+
                "portal_name {"+
                "font-size: 1.0em;"+
                "color: black;"+
                "text-shadow: 1px 1px 1px white,"+
                "-1px 1px 1px white,"+
                "1px -1px 1px white,"+
                "-1px -1px 1px white;"+
                "line-height: 1.0;"+
                "display: block;"
            )
            .appendTo("head");
    };

    window.plugin.grid_ex.resetPortalLabel = function() {

        window.plugin.grid_ex.portalNameLayerGroup.clearLayers();
        portalLabels = [];
        window.plugin.grid_ex.drawPortalLabel();
    };

    window.plugin.grid_ex.drawPolygon = function() {
        $.getJSON('https://nvglab.net/ingress/data/' + window.GRID_EX_PROJECT_ID + '_polygon.json', function(data) {
            $.each(data, function(i, draw_object){
                if (draw_object.type === "polygon") {
                    var latlngs = [];
                    $.each(draw_object.latLngs, function(i, latlng) {
                        latlngs.push([latlng.lat, latlng.lng]);
                    });
                    var polygon = new window.L.polygon(latlngs, {color: draw_object.color}).addTo(plugin.grid_ex.polygonLayerGroup);
                }
            });
        });
    };

    window.plugin.grid_ex.portalLabel = function(guid) {
        var portal = portals.find((p) => {
            return (p.guid === guid);
    });

        if (portal !== undefined) return portal.label;

        return null;
    }

    window.plugin.grid_ex.drawPortalLabel = function() {

        if (map.getZoom() <= showPortalMaxZoom) {
            window.plugin.grid_ex.portalNameLayerGroup.clearLayers();
            portalLabels = [];
            return;
        }

        var showPortalName = true;
        var showPortalNameString = window.readCookie('GRID_EX_SHOW_PORTAL_NAME');
        if (showPortalNameString === 'false') showPortalName = false;

        var showPortalNo = window.readCookie('GRID_EX_SHOW_PORTAL_NO') === '1';

        var bounds = map.getBounds();

        Object.keys(window.portals).forEach(function(guid) {
            var portal = this[guid];
            if (bounds.contains(portal._latlng)) {

                var existingLabel = portalLabels[portal.options.guid];
                if (existingLabel) {
                } else {
                    var html = '<table border="0" cellspacing="0" cellpadding="0" align="center" width=100px>';
                    if (showPortalNo) {
                        var portalLabel = window.plugin.grid_ex.portalLabel(portal.options.guid);
                        if (portalLabel) {
                            var labelColor = "blue";

                            if (window.GRID_EX_SHOW_ORNAMENT) {
                                console.log(portal.options.data.ornaments);
                                var kind = window.plugin.grid_ex.ornamentKind(portal);
                                if (kind == 1) labelColor = "blue";
                                else labelColor = 'gray';
                            }

                            html += `<tr><td align='center'><portal_label><font color=${labelColor}>` + portalLabel + '</font></portal_label></td></tr>';
                        }
                    }
                    if (showPortalName && portal.options.data.title != undefined) {
                        html += '<tr><td align="center"><portal_name>'+portal.options.data.title+'</portal_name></td></tr></table>';
                    } else {
                        html += '</table>';
                    }
                    var labelDivIcon = L.divIcon({ className: 'label-div-icon', html: html, iconAnchor: [50, -10], iconSize: 'auto' });
                    var label = L.marker(portal._latlng, { icon: labelDivIcon }).on('click', window.plugin.grid_ex.onClickPortalLabel);

                    if (portal.options.data.title != undefined) {
                        label.addTo(window.plugin.grid_ex.portalNameLayerGroup);
                        portalLabels[portal.options.guid] = label;
                    }
                }
            }
        }, window.portals);
    }

    window.plugin.grid_ex.ornamentKind = function(portal) {

        var kind = 0;

        if (portal.options.data.ornaments === undefined) return kind;

        var count = portal.options.data.ornaments.length;
        for (var i = 0; i < count; i++) {
            var ornament = portal.options.data.ornaments[i];
            if (ornament.includes("ap", 0)) kind = 1;
            //     vola, etc...
        }

        return kind;
    }

    window.plugin.grid_ex.mapMoved = function() {
        window.plugin.grid_ex.drawPortalLabel();
    };

    window.plugin.grid_ex.portalAdded = function(data) {
        var label = portalLabels[data.portal.options.guid];
        if (!label) {
            window.plugin.grid_ex.drawPortalLabel();
        }
    };

    window.plugin.grid_ex.portalRemoved = function(data) {

        var label = portalLabels[data.portal.options.guid];
        if (label) {
            window.plugin.grid_ex.portalNameLayerGroup.removeLayer(label);
            delete portalLabels[data.portal.options.guid];
        }
    };

    window.plugin.grid_ex.onClickPortalLabel = function(e) {
        portals.some(function(portal) {
            var latString = e.latlng.lat.toString();
            var lngString = e.latlng.lng.toString();

            if (portal.lat === latString && portal.lng === lngString) {
                window.renderPortalDetails(portal.guid);
                return true;
            }
        });
    };

    window.plugin.grid_ex.meterToLatitude = function() {
        var POLE_RADIUS = 6356752.314;
        return 360 / ( 2 * Math.PI * POLE_RADIUS );
    };

    window.plugin.grid_ex.meterToLongitude = function(latitude) {
        var EQUATOR_RADIUS = 6378137;
        return 360 / ( 2 * Math.PI * ( EQUATOR_RADIUS * Math.cos(latitude * Math.PI / 180.0) ) );
    };

    window.plugin.grid_ex.drawGridBlockNoLabel = function(lat, lng, col, row) {
        var colName = '?';
        var numAlphabets = GRID_EX_ALPHABETS.length;
        if (col < numAlphabets) colName = GRID_EX_ALPHABETS[col];
        else if (col < numAlphabets * numAlphabets) colName = GRID_EX_ALPHABETS[parseInt(col/numAlphabets)-1] + GRID_EX_ALPHABETS[col%numAlphabets];

        var rowName = row + 1;

        var name = '<span style="font-size:1.1em; font-style:bold; color:black; padding:2px; letter-spacing:2px;">' + rowName + colName + '</span>';

        var marker = new window.L.Marker([lat, lng], {
            icon: window.L.divIcon({
                iconSize: 'auto',
                iconAnchor: [0, 0],
                html: name
            }),
        });

        marker.setOpacity(0.7);
        window.plugin.grid_ex.gridBlockNoLayerGroup.addLayer(marker);
    };

    window.plugin.grid_ex.drawGridBlockNo = function() {

        var latDelta = window.plugin.grid_ex.meterToLatitude() * window.GRID_EX_GRID_LENGTH;
        var lngDelta = window.plugin.grid_ex.meterToLongitude(window.GRID_EX_TOP_LATITUDE) * window.GRID_EX_GRID_LENGTH;

        lat = window.GRID_EX_TOP_LATITUDE;
        for (var row = 0; row < window.GRID_EX_NUM_ROWS; row++) {
            lng = window.GRID_EX_LEFT_LONGITUDE;
            for (var col = 0; col < window.GRID_EX_NUM_COLS; col++) {
                window.plugin.grid_ex.drawGridBlockNoLabel(lat, lng, col, row);

                lng += lngDelta;
            }
            lat -= latDelta;
        }
    }

    window.plugin.grid_ex.drawGrid = function(divide, layerGroup) {

        layerGroup.clearLayers();

        var lines = [];

        var numCols = window.GRID_EX_NUM_COLS * divide;
        var numRows = window.GRID_EX_NUM_ROWS * divide;

        var latDelta = window.plugin.grid_ex.meterToLatitude() * (window.GRID_EX_GRID_LENGTH / divide);
        var lngDelta = window.plugin.grid_ex.meterToLongitude(window.GRID_EX_TOP_LATITUDE) * (window.GRID_EX_GRID_LENGTH / divide);

        var lng_l = window.GRID_EX_LEFT_LONGITUDE;
        var lng_r = window.GRID_EX_LEFT_LONGITUDE + lngDelta * numCols;
        var lat = window.GRID_EX_TOP_LATITUDE;

        for (var y = 0; y < numRows +1; y++) {
            if (divide == 4) {
                if (!isGrid250Visible) lines.push([[lat, lng_l], [lat, lng_r]]);
                else if (y % 2 == 1) lines.push([[lat, lng_l], [lat, lng_r]]);
            } else {
                lines.push([[lat, lng_l], [lat, lng_r]]);
            }

            lat -= latDelta;
        }

        var lat_t = window.GRID_EX_TOP_LATITUDE;
        var lat_b = window.GRID_EX_TOP_LATITUDE - latDelta * numRows;
        var lng = window.GRID_EX_LEFT_LONGITUDE;

        for (var x = 0; x < numCols+1; x++) {
            if (divide == 4) {
                if (!isGrid250Visible) lines.push([[lat_t, lng], [lat_b, lng]]);
                else if (x % 2 == 1) lines.push([[lat_t, lng], [lat_b, lng]]);
            } else {
                lines.push([[lat_t, lng], [lat_b, lng]]);
            }

            lng += lngDelta;
        }

        var polyLines = [];
        if (divide == 1) polyLines = window.L.multiPolyline(lines, {color: 'black', weight: 2.5, opacity:1.0});
        else if (divide == 2) polyLines = window.L.multiPolyline(lines, {color: 'blue', weight: 1.0, opacity:1.0});
        else if (divide == 4) polyLines = window.L.multiPolyline(lines, {color: 'red', weight: 0.7, opacity:1.0});

        polyLines.addTo(layerGroup);
    };

    window.plugin.grid_ex.openOptDialog = function() {
        var html = '';
        html += '<div id="grid_exSetbox">';
        html += '<a onclick="window.plugin.grid_ex.optCode();return false;">code</a>';
        html += '</div>';
        window.dialog({
            html: html,
            dialogClass: 'ui-dialog-grid_exSet',
            title: 'Grid Settings',
            close: window.plugin.grid_ex.closeOptDialog
        });
    };

    window.plugin.grid_ex.closeOptDialog = function() {
        window.location.reload();
    };

    window.plugin.grid_ex.optCode = function() {
        var result = prompt('code', window.GRID_EX_PROJECT_ID);
        if (result !== null) {
            window.localStorage.setItem('GRID_EX_PROJECT_ID', result);
        }
    };

    window.plugin.grid_ex.createLabelSearchButton = function() {
        var customControl =  window.L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (map) {
                var container = window.L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

                container.style.backgroundColor = 'white';
                container.style.backgroundImage = "url(https://nvglab.net/ingress/data/bin/search.png)";
                container.style.backgroundSize = "28px 28px";
                container.style.width = '28px';
                container.style.height = '28px';

                container.onclick = function(){

                    var search_word = window.prompt("ラベル名で検索（先頭数文字／小文字でも可）", "");

                    if (search_word.length != 0) {
                        if (searchedLabelIndicator !== null) map.removeLayer(searchedLabelIndicator);

                        if (!portals.some(function(portal) {
                            if (portal.label.match(new RegExp('^'+search_word, 'i'))) {
                                map.panTo([portal.lat, portal.lng]);

                                searchedLabelIndicator = L.circleMarker([portal.lat, portal.lng], { radius: 16, weight: 3, color: 'red', opacity:1, fillOpacity: 0, interactive: false }).addTo(map);

                                return true;
                            }
                        })) {
                            alert('みつかりませんでした\n検索できるのはポータルの名前ではなくポータルについているラベル名ですので1A02などのように入力してください。小文字（1a)や途中まで（1A0など）もOK。複数該当する場合は最初に一致したポータルにジャンプします');
                        }
                    }
                };

                return container;
            }
        });

        window.map.addControl(new customControl());
    };

    window.plugin.grid_ex.createPortalNameButton = function() {
        var customControl =  window.L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (map) {
                var container = window.L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

                container.style.backgroundColor = 'white';
                container.style.backgroundImage = "url(https://nvglab.net/ingress/data/bin/p_title.png)";
                container.style.backgroundSize = "28px 28px";
                container.style.width = '28px';
                container.style.height = '28px';

                container.onclick = function(){
                    var showPortalName = true;
                    var showPortalNameString = window.readCookie('GRID_EX_SHOW_PORTAL_NAME');
                    if (showPortalNameString === 'false') showPortalName = false;

                    showPortalName = showPortalName ? false : true;
                    window.writeCookie('GRID_EX_SHOW_PORTAL_NAME', showPortalName, { expires: 14 });

                    window.plugin.grid_ex.resetPortalLabel();
                };

                return container;
            }
        });

        portalNameButton = new customControl();
        window.map.addControl(portalNameButton);
    };

    window.plugin.grid_ex.addRechargeRequestLink = function(guid) {
        $('.linkdetails').append('<aside><a onclick="window.plugin.grid_ex.openRechargeDialog(\''+window.selectedPortal+'\')" title="選択中のポータルへのリチャージ要請を出します">Recharge</a></aside>');
    };

    var RECHAERGE_REQUESTED_GUID = '';

    window.plugin.grid_ex.openRechargeDialog = function(guid) {

        RECHAERGE_REQUESTED_GUID = guid;

        var html = '';
        html += '<div id="grid_exSetbox">';
        html += '<a onclick="window.plugin.grid_ex.startRecharge();return false;">リチャージ要請(５分)</a>';
        html += '<a onclick="window.plugin.grid_ex.stopRecharge();return false;">リチャージ要請(終了)</a>';
        html += '</div>';
        window.dialog({
            html: html,
            dialogClass: 'ui-dialog-grid_exSet',
            title: 'Recharge要請',
        });
    };

    window.plugin.grid_ex.startRecharge = function() {
        window.plugin.grid_ex.onRechargeRequest(300);
    };

    window.plugin.grid_ex.stopRecharge = function(guid) {
        window.plugin.grid_ex.onRechargeRequest(0);
    };

    window.plugin.grid_ex.onRechargeRequest = function(interval) {

        if (window.GRID_EX_PROJECT_ID.indexOf('_oss_') == -1) {
            alert('本番では今の操作でリチャージ要請が送信されます');
            return;
        }

        if (!portals.some(function(portal) {
            if (portal.guid === RECHAERGE_REQUESTED_GUID) {

                if (interval === 0) {
                    alert(portal.label+'へのリチャージ終了要請を送信しました');
                } else {
                    alert(portal.label+'へのリチャージ要請を送信しました。すでに要請済みの場合は５分延陵されます');
                }

                $.ajax({
                    type: "POST",
                    url: "https://v150-95-141-250.a086.g.tyo1.static.cnode.io/igres/api/portals_info/Sapporo20180728",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    crossDomain: true,
                    dataType: 'json',
                    data: JSON.stringify([{
                        "portal_guid": portal.guid,
                        "ttl_recharge": interval
                    }]),
                    success: function(html){
                        //alert(portal.label+'へのリチャージ要請を送信しました');
                        //console.log(html);
                    },
                    error: function(error) {
                        alert('送信に失敗しました');
                    }
                });
                return true;
            }
        })) {
            alert('エラー:\n選択中のポータルはリチャージ要請の対象ではありません。対象になるのはグリッド内のラベル(ex. AA00)のついているポータルだけです');
        }
    };

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


