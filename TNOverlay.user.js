// ==UserScript==
// @author         lostling
// @name           Tainan 2024 Overlay
// @version        1.0.0
// @description    Tainan 2024 XMA
// @match          https://intel.ingress.com/*
// @match          https://intel-x.ingress.com/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {

	var localRESWUE = {
		"Alerts": [],
		"Agents": [],
		"Keys": [],
		"Portals": [
		{"guid":"477b919a55164cf880f8cf720c35cf2b.16","name":"牌樓-玄良亭","lat":22.982677,"lng":120.198253,"codename":"OA14"},
		{"guid":"5221960ded804ea6b999b97fb3de23a4.16","name":"變電箱","lat":22.983276,"lng":120.197749,"codename":"OA12"},
		{"guid":"0bab8311dfd13d92b21b48f2df52f84f.16","name":"我愛仙草里石頭","lat":22.983431,"lng":120.199263,"codename":"OA11"},
		{"guid":"63c46daa373a4125a46c15a5adf4e5ba.16","name":"浮雲","lat":22.983579,"lng":120.200007,"codename":"OA10"},
		{"guid":"88b71ada3c1647178318d5101649b3e3.16","name":"螺絲雕塑","lat":22.982027,"lng":120.198338,"codename":"OA18"},
		{"guid":"a28c5eb9d44348869c0307fbfcdbff60.16","name":"金典大樓","lat":22.982532,"lng":120.201848,"codename":"OA15"},
		{"guid":"91017071396931938c67bbb3df94d4af.16","name":"母子石像雕塑","lat":22.98236,"lng":120.199197,"codename":"OA16"},
		{"guid":"84af545513ad48a48cb3780bf0f548f6.16","name":"衛理堂","lat":22.9815,"lng":120.199377,"codename":"OA20"},
		{"guid":"48742bbb71144d2aab5f52255c68223e.16","name":"健康路浸信會","lat":22.981661,"lng":120.201801,"codename":"OA19"},
		{"guid":"8a5f17dc4890430c85edd1d63a701b0f.16","name":"西洋幽靈馬車壁畫","lat":22.982236,"lng":120.198239,"codename":"OA17"},
		{"guid":"3942d836ab6941c38a7b2666af37d35a.16","name":"花花變電箱","lat":22.982959,"lng":120.205624,"codename":"OB11"},
		{"guid":"6bd4ee1c79174068bf108d885fbc325e.16","name":"深吻","lat":22.982772,"lng":120.203926,"codename":"OB12"},
		{"guid":"ced9b3fefe4e335ba2d3254517d2de07.16","name":"五妃社區活動中心","lat":22.983064,"lng":120.204835,"codename":"OB10"},
		{"guid":"fd4aaa64f9e24603bd89393afd8959aa.16","name":"樹柵欄","lat":22.982742,"lng":120.205455,"codename":"OB13"},
		{"guid":"87204afa19cf4304bcc6b8ef753a3e34.16","name":"五妃廟 - 解說牌","lat":22.982075,"lng":120.205183,"codename":"OB24"},
		{"guid":"113bc5b665864c23a18f23c10c5a024e.16","name":"五妃廟 - 期許","lat":22.982272,"lng":120.205316,"codename":"OB21"},
		{"guid":"626770c3ca5843cc9520904a0be3df4f.16","name":"千香電箱","lat":22.982536,"lng":120.205733,"codename":"OB18"},
		{"guid":"1e99c97877df47f69ae74cb2418b6936.16","name":"五妃廟","lat":22.982534,"lng":120.205062,"codename":"OB19"},
		{"guid":"d90cf5c189784e02b5acc95a1a0394f3.16","name":"五妃廟-益靈君","lat":22.981787,"lng":120.205041,"codename":"OB27"},
		{"guid":"5d75f43b8007405084c05b7e288af52a.16","name":"變電箱三兄弟","lat":22.982645,"lng":120.206135,"codename":"OB16"},
		{"guid":"14398f6fcdb43296b738208d5454e66e.16","name":"尋訪古蹟","lat":22.982669,"lng":120.205873,"codename":"OB14"},
		{"guid":"c42b965a72d040b199fb62f6b0b973c9.16","name":"五妃廟墓門","lat":22.981848,"lng":120.204809,"codename":"OB26"},
		{"guid":"a8dd4b77bbe44258b0f27ffdcec7bce1.16","name":"五妃廟","lat":22.981973,"lng":120.204983,"codename":"OB25"},
		{"guid":"8f16d2ad650a4b4a8e3c39d25bc79cf3.16","name":"金龜樹","lat":22.982533,"lng":120.205435,"codename":"OB20"},
		{"guid":"a445e3059b8437edaddeb5329a6f98f6.16","name":"五妃廟旁圖根點","lat":22.982159,"lng":120.204569,"codename":"OB23"},
		{"guid":"c608e8be06e84849bae7b9cb802facf3.16","name":"五妃之碑","lat":22.982229,"lng":120.204939,"codename":"OB22"},
		{"guid":"45b63aa36769460abf008a075521abed.16","name":"綠林彩繪壁畫","lat":22.982547,"lng":120.204452,"codename":"OB17"},
		{"guid":"e519450c36e64019bb528aaf0c1c6e75.16","name":"五妃廟變電箱","lat":22.982667,"lng":120.205236,"codename":"OB15"},
		{"guid":"f72e504069104691ab86a6244b947bbf.16","name":"灌水叢生變電箱","lat":22.982581,"lng":120.209939,"codename":"OC14"},
		{"guid":"5b1b78e9434743dc914816426ec8b335.16","name":"變電箱","lat":22.982083,"lng":120.211527,"codename":"OC19"},
		{"guid":"60b7c53c078f4e1eaa193541cf274578.16","name":"山河變電箱","lat":22.981822,"lng":120.209716,"codename":"OC20"},
		{"guid":"96d21fa0f8363214881351d21d5cd2c0.16","name":"Don-Nita Garden","lat":22.982461,"lng":120.207484,"codename":"OC15"},
		{"guid":"36db52a4468c4fc4a910d01721485ca5.16","name":"白色花海變電箱","lat":22.982639,"lng":120.208447,"codename":"OC10"},
		{"guid":"fc19994042b148feaf3eccdd6c90587d.16","name":"還我河山","lat":22.981706,"lng":120.210383,"codename":"OC21"},
		{"guid":"620af41118f841019310a9687546bd48.16","name":"台南大學 - 校徽地磚","lat":22.982612,"lng":120.207591,"codename":"OC13"},
		{"guid":"3df613f5a4023367b53b492a0e163e84.16","name":"迦南教會","lat":22.982084,"lng":120.209568,"codename":"OC18"},
		{"guid":"88b219bdd835447eb58ed45df3cd8d64.16","name":"台南大學 - 鳳凰","lat":22.982631,"lng":120.207322,"codename":"OC11"},
		{"guid":"3499933be1f63e4682be200f4a24d3d9.16","name":"北極殿-玄天上帝","lat":22.982221,"lng":120.207474,"codename":"OC17"},
		{"guid":"7a872fef7f004bca8e0e1a3ca5685643.16","name":"棉花田變電箱","lat":22.982618,"lng":120.209112,"codename":"OC12"},
		{"guid":"ae9e9ebb107747bab5fc48412f24e330.16","name":"花花變電箱","lat":22.982359,"lng":120.209783,"codename":"OC16"},
		{"guid":"74f993e9b83a42feb784ed09a8f82530.16","name":"水交社眷村園區","lat":22.979365,"lng":120.200352,"codename":"OD13"},
		{"guid":"34f9a1ffb4eb44bebdaf8151515fc94d.16","name":"無極紫鑾殿","lat":22.979996,"lng":120.199553,"codename":"OD11"},
		{"guid":"5c433e64888c484597fadb04db4f6aac.16","name":"仙草寮玄光壇","lat":22.979865,"lng":120.20034,"codename":"OD12"},
		{"guid":"361cd08b6a493e40b8221718750cc49a.16","name":"紙船石雕","lat":22.977699,"lng":120.199623,"codename":"OD14"},
		{"guid":"8eedb3a29d17332faeb5e1a64f6ba251.16","name":"真耶穌教會南門教會","lat":22.980528,"lng":120.200165,"codename":"OD10"},
		{"guid":"e0b0427813af35d69b645db7e872e39e.16","name":"世界真美麗","lat":22.976846,"lng":120.198761,"codename":"OD17"},
		{"guid":"ca3d39f534933cdab0169069e08eb261.16","name":"真美麗溜滑梯","lat":22.976857,"lng":120.198197,"codename":"OD16"},
		{"guid":"686df020f80f363ead62828393f46f81.16","name":"小白鳥","lat":22.976902,"lng":120.197887,"codename":"OD15"},
		{"guid":"319a8ef313684166b1735374c5e9cb6d.16","name":"彩繪變電箱-台鹽生技","lat":22.981245,"lng":120.204399,"codename":"OE12"},
		{"guid":"537de0742ef0439ab5df0f534a44d1a7.16","name":"水仙花變電箱","lat":22.977656,"lng":120.203711,"codename":"OE20"},
		{"guid":"ce05e97504ec42d5aafd1f6665298645.16","name":"變電","lat":22.979047,"lng":120.20357,"codename":"OE15"},
		{"guid":"9b11c34f812045e5ab42016348252e06.16","name":"台南市體育處","lat":22.977971,"lng":120.204125,"codename":"OE19"},
		{"guid":"2f3ebd06d7634e89a2335f3f4de241f7.16","name":"山變電 P9684-HD16","lat":22.978804,"lng":120.203547,"codename":"OE16"},
		{"guid":"a147438a8f1e49f48ed18967227d05b8.16","name":"藍色變電箱","lat":22.98091,"lng":120.203815,"codename":"OE13"},
		{"guid":"72bbd26606cd402886ed529e1286f95a.16","name":"臺南市政府體育局","lat":22.97825,"lng":120.203812,"codename":"OE17"},
		{"guid":"db221790e1a14378baf7a89c05b2921d.16","name":"健康路郵局","lat":22.981478,"lng":120.204344,"codename":"OE10"},
		{"guid":"aa088190f4f3410b85d34dc0c4c46f7a.16","name":"台南水都","lat":22.981299,"lng":120.204844,"codename":"OE11"},
		{"guid":"818860e17c823babb50cfeb2c28246c9.16","name":"台南市立體育場前裝置藝術","lat":22.978205,"lng":120.204235,"codename":"OE18"},
		{"guid":"6ee34943451d4040a250071b1912e000.16","name":"勞工育樂中心","lat":22.97991,"lng":120.203873,"codename":"OE14"},
		{"guid":"9c841f5df0d934d3ac111232bc224cd0.16","name":"臺南市國軍英雄館","lat":22.981182,"lng":120.206883,"codename":"OF11"},
		{"guid":"a358a64116653d53958fb7a9aeeb21e0.16","name":"大章魚","lat":22.979414,"lng":120.210308,"codename":"OF17"},
		{"guid":"685a3d5d25a538108abf5a7d83542fee.16","name":"健康廣場","lat":22.980863,"lng":120.209783,"codename":"OF14"},
		{"guid":"8bb92ae408ad45adbe16f90e7ada11db.16","name":"鳶尾","lat":22.979846,"lng":120.206955,"codename":"OF16"},
		{"guid":"6ab2f62576c5453c8789526760110848.16","name":"竹溪里歡迎牌","lat":22.981094,"lng":120.209556,"codename":"OF12"},
		{"guid":"e1d14d3a055642b785b5f45abc0f0621.16","name":"天堂鳥","lat":22.980903,"lng":120.206974,"codename":"OF13"},
		{"guid":"cd2edd0767953610a720a5abc3893ae7.16","name":"車輪與葫蘆","lat":22.979385,"lng":120.20722,"codename":"OF18"},
		{"guid":"455581e15877475c94eab1c25b128b01.16","name":"台南市立體育場","lat":22.98119,"lng":120.207148,"codename":"OF10"},
		{"guid":"a3ae072a82f2349a9e4d4f91a6eda75b.16","name":"竹溪里活動中心涼亭","lat":22.979316,"lng":120.209523,"codename":"OF19"},
		{"guid":"42a38b290e65443bac79e4f72e538a35.16","name":"運動場綠底黃花變電箱","lat":22.980198,"lng":120.206955,"codename":"OF15"},
		{"guid":"2ba3c1065ff24c56b36988a9db1cbb79.16","name":"竹溪寺","lat":22.977534,"lng":120.208397,"codename":"OG17"},
		{"guid":"7492b28fa8b13376ba7cf3bf6154804e.16","name":"湖心輪月標示牌","lat":22.977653,"lng":120.209828,"codename":"OG15"},
		{"guid":"f581592bd7c5481aaab517f71e7a6c77.16","name":"台南市極限運動場","lat":22.978474,"lng":120.207449,"codename":"OG10"},
		{"guid":"9c53af61e7244d188ccc595dc137511c.16","name":"積健為雄石柱","lat":22.97817,"lng":120.20796,"codename":"OG12"},
		{"guid":"898505eb97ee4b7bb342a00f48496165.16","name":"竹溪古寺牌樓","lat":22.977621,"lng":120.207716,"codename":"OG16"},
		{"guid":"52f17a78c2f6451098048850e233b6af.16","name":"體育場聖火台","lat":22.978226,"lng":120.20712,"codename":"OG11"},
		{"guid":"15ddc6822abc445a8682088f152c99f3.16","name":"羅漢堂達摩祖師","lat":22.977848,"lng":120.208068,"codename":"OG14"},
		{"guid":"4986695d14994e50bbb8396ee76c179d.16","name":"太極拳早操會","lat":22.978095,"lng":120.20838,"codename":"OG13"},
		{"guid":"41764b8b46544972a983a16fd589b76c.11","name":"崇孝塔","lat":22.977288,"lng":120.209316,"codename":"OG18"},
		{"guid":"70a8ae913aff4f45ab3c55a3a516467d.16","name":"塔","lat":22.976864,"lng":120.209349,"codename":"OG19"},
		{"guid":"03bcfe9454ca362b86cd9d33178fe1f1.16","name":"南無地蔵王菩薩","lat":22.975192,"lng":120.204186,"codename":"OH12"},
		{"guid":"dd633c5aa78246eebb19aeaf69137903.16","name":"水交社","lat":22.975191,"lng":120.20053,"codename":"OH13"},
		{"guid":"7d321cb5650c35abbda116f41e7f9d18.16","name":"光禾社區公共景觀藝品","lat":22.975677,"lng":120.19975,"codename":"OH10"},
		{"guid":"3f8da49588984203abc863306ce2c1c7.16","name":"竹溪禪寺","lat":22.974055,"lng":120.205226,"codename":"OH18"},
		{"guid":"633f271f134244629dcb0d629eeaa47a.16","name":"竹溪觀音像","lat":22.97429,"lng":120.205829,"codename":"OH16"},
		{"guid":"3ea4d9f4805c34018625822d7c34045a.16","name":"三角公園","lat":22.975413,"lng":120.203555,"codename":"OH11"},
		{"guid":"7c5e87dbad5544e58c2df672232e0450.16","name":"笑容嫵媚","lat":22.974491,"lng":120.205983,"codename":"OH15"},
		{"guid":"d55c8061a1ac4c27ab0aaba92b47a8f9.16","name":"闔家歡","lat":22.975132,"lng":120.202616,"codename":"OH14"},
		{"guid":"17fdbbb061154e9fadb5ef6c8a580926.16","name":"竹溪寺","lat":22.974081,"lng":120.206579,"codename":"OH17"},
		{"guid":"4f0f9c8a3bc44cc8b10ccfa831d8a802.16","name":"臺南市立橄欖球場","lat":22.974656,"lng":120.207183,"codename":"OI14"},
		{"guid":"55be6032817e4cb0acae01404feac097.16","name":"台南市體育公園徒步園區","lat":22.975987,"lng":120.207162,"codename":"OI11"},
		{"guid":"d29aac5b26ff4d09a12e633d0a40ce9d.11","name":"羅公亭","lat":22.97436,"lng":120.207471,"codename":"OI15"},
		{"guid":"a915d4d879e84a7cb2043b3641eda18f.16","name":"竹溪斜張橋","lat":22.97635,"lng":120.20713,"codename":"OI10"},
		{"guid":"4b9e1e608d8c46fbac9ee686b907f5f4.11","name":"蒸氣火車頭","lat":22.975014,"lng":120.207215,"codename":"OI13"},
		{"guid":"5124e713f1243168a40a7963546be4a4.16","name":"台南市立專用足球場","lat":22.975017,"lng":120.209686,"codename":"OI12"},
		{"guid":"c0e0903ea0d843408a32fa4accc267c5.16","name":"光明王寺","lat":22.987873,"lng":120.20933,"codename":"SA20"},
		{"guid":"e9c2cfb016b443c4bbe8ca199c069d83.16","name":"阿花警察箱","lat":22.988841,"lng":120.209988,"codename":"SA13"},
		{"guid":"c53f54507ef74ee988674d60419bebe9.16","name":"府前一路119號變電箱","lat":22.988919,"lng":120.208431,"codename":"SA10"},
		{"guid":"0fd332625e223a0a9365f1e10e961c17.16","name":"忠義堂拱門","lat":22.987447,"lng":120.209923,"codename":"SA21"},
		{"guid":"e755fb22ed5d39bb9391a55b733323c4.16","name":"牛車來去","lat":22.988134,"lng":120.20972,"codename":"SA18"},
		{"guid":"4c1de3368db24df9b81c5fd6d1771942.16","name":"仁厚境福德祠","lat":22.988357,"lng":120.209333,"codename":"SA16"},
		{"guid":"730bbb1ef9384dc9b455eb86125987ba.16","name":"順天冰棒","lat":22.988424,"lng":120.208565,"codename":"SA15"},
		{"guid":"6f3c26f3bafc4ca2b77a0b114adf6e9f.16","name":"仁厚境","lat":22.988855,"lng":120.209429,"codename":"SA12"},
		{"guid":"dbb8a32682e23f979180daddb5ed8672.16","name":"請勿隨地吐痰","lat":22.987963,"lng":120.210185,"codename":"SA19"},
		{"guid":"c7314a7440c9448e82a5a9535c35a4cf.16","name":"變電箱","lat":22.988894,"lng":120.20967,"codename":"SA11"},
		{"guid":"c1bb564854ba4ca991ac7619a585a94c.16","name":"招牌肉串mix多啦A夢","lat":22.98851,"lng":120.21114,"codename":"SA14"},
		{"guid":"1b06b416859e4da588dbba29875f53be.16","name":"開山公園","lat":22.988149,"lng":120.209407,"codename":"SA17"},
		{"guid":"9cc577237a383a22aacbe03e14b0a55a.16","name":"天主教教友活動中心","lat":22.987402,"lng":120.209472,"codename":"SA22"},
		{"guid":"ce95013a592c4e7baed3efbf8e7230ee.11","name":"台南忠烈祠","lat":22.981071,"lng":120.210941,"codename":"SB12"},
		{"guid":"8189758ca81b41849d34ff07e471e7ce.16","name":"藝術轉角","lat":22.981028,"lng":120.211689,"codename":"SB13"},
		{"guid":"05e24f2aa76d39459ff80fa590ae0e83.16","name":"大林國宅商圈","lat":22.980512,"lng":120.212254,"codename":"SB16"},
		{"guid":"6a5bc253f32b4c6cbfb1a0730bac0406.11","name":"體育公園牌樓","lat":22.981089,"lng":120.210188,"codename":"SB11"},
		{"guid":"13b59db27f6945d8b3c628576b8dab36.16","name":"踏出健康的人生","lat":22.980143,"lng":120.210533,"codename":"SB18"},
		{"guid":"6c1950dc4c80328b97cace51856458e6.16","name":"皇宮舞台","lat":22.980825,"lng":120.211517,"codename":"SB15"},
		{"guid":"2d96011491e23f5a9039842c931c968a.16","name":"國民革命忠烈廟","lat":22.980263,"lng":120.210916,"codename":"SB17"},
		{"guid":"044698c2403e43f79846ecf34503f6df.16","name":"司馬光打破缸","lat":22.979772,"lng":120.211026,"codename":"SB20"},
		{"guid":"467d3360cb9748dfbfa7e52d1f2985d5.16","name":"府城藝術轉角","lat":22.980898,"lng":120.211947,"codename":"SB14"},
		{"guid":"5ae9e4751f9548e3a78948e5cc29fa62.16","name":"勝利之聲廣播公司","lat":22.981327,"lng":120.21111,"codename":"SB10"},
		{"guid":"397509d6a8314351876c39d610780db4.16","name":"歷史偉人蔣介石","lat":22.979741,"lng":120.21079,"codename":"SB21"},
		{"guid":"8d5ca7b6f0a34273ad0ecebdbf68deaf.16","name":"快樂石獅左","lat":22.980058,"lng":120.211045,"codename":"SB19"},
		{"guid":"0adc522888313a069e0970710610e40f.16","name":"台南市體育公園軟式網球場（北）","lat":22.97934,"lng":120.211343,"codename":"SC10"},
		{"guid":"a027657645dc3702890b5ff9fd092d00.16","name":"台南市體育公園軟式網球場（南）","lat":22.978322,"lng":120.211556,"codename":"SC12"},
		{"guid":"8b64eef0b64b3101b35bf480182283da.16","name":"新生里活動中心","lat":22.978629,"lng":120.211919,"codename":"SC11"},
		{"guid":"e86feaa5c00d490986be153c13ed7bd5.16","name":"跑步機","lat":22.978087,"lng":120.211879,"codename":"SC13"},
		{"guid":"bdecd2f6c4d04af5b6e2fe1b073b7565.16","name":"新生公園 涼亭","lat":22.976874,"lng":120.211941,"codename":"SC17"},
		{"guid":"9aff053863fa325ba1f986f7fcaced1d.16","name":"搖搖椅","lat":22.976683,"lng":120.21204,"codename":"SC18"},
		{"guid":"ed64677d770a3f0fb1a367fc027cfb80.16","name":"步道藝術小燈飾","lat":22.976531,"lng":120.212168,"codename":"SC19"},
		{"guid":"453d832b29d0324ba077f486a71cc044.16","name":"大林國宅後戶外兒童遊戲設施","lat":22.977415,"lng":120.211681,"codename":"SC14"},
		{"guid":"282976aaefc44e75977041bb56d54531.16","name":"新生公園","lat":22.976107,"lng":120.212352,"codename":"SC20"},
		{"guid":"e534c534f95e402eaff61d64132b4de6.11","name":"坦克","lat":22.977235,"lng":120.2117,"codename":"SC16"},
		{"guid":"09c6c328552c42aebb4e0ef127fe535a.16","name":"永懷領袖","lat":22.977387,"lng":120.212087,"codename":"SC15"},
		{"guid":"fba50eb601f33d05855e0866614427ef.16","name":"大林社區教會","lat":22.978727,"lng":120.212658,"codename":"SD11"},
		{"guid":"4e9802cf080e36f692533f17d166c5c6.16","name":"大林新城","lat":22.978554,"lng":120.213249,"codename":"SD12"},
		{"guid":"d033bdaae65938eb93870f154ac25547.16","name":"跳躍的鯉魚","lat":22.978542,"lng":120.212921,"codename":"SD13"},
		{"guid":"15dd5b7ab6a038cdb78d65209b185f6e.16","name":"祖孫釣魚","lat":22.978433,"lng":120.212761,"codename":"SD14"},
		{"guid":"e3c74d7e88c8368d8bdb69c7846fc2fd.16","name":"我♥️大林新城","lat":22.978736,"lng":120.213157,"codename":"SD10"},
		{"guid":"cd533d03761840938c03f734fe1c66a4.16","name":"變電箱","lat":22.978324,"lng":120.213385,"codename":"SD15"},
		{"guid":"af2662b9faf04355b930f70633780ab3.16","name":"老先覺","lat":22.977892,"lng":120.213186,"codename":"SD17"},
		{"guid":"2c85502ac04d435bb521e8f08fb71712.16","name":"變電箱","lat":22.978162,"lng":120.213391,"codename":"SD16"},
		{"guid":"dea0c675e27c3485ac89102849161f5e.16","name":"台南眷村美食","lat":22.977006,"lng":120.212981,"codename":"SD19"},
		{"guid":"c40a7da63bb63643a5b035ecd2e23e0f.16","name":"松村滷味","lat":22.977333,"lng":120.213417,"codename":"SD18"},
		{"guid":"59634b48e1b846c58bb0f80a937b5e79.16","name":"煙斗狗","lat":22.976409,"lng":120.213026,"codename":"SD20"},
		{"guid":"0ad383f1c1483033bd215870a8008f54.16","name":"台南煙波休息區藝術牆","lat":22.989131,"lng":120.199882,"codename":"TA18"},
		{"guid":"37fe94f31af3302fb80f65fee6dfcec8.16","name":"燕鴨","lat":22.989374,"lng":120.200088,"codename":"TA17"},
		{"guid":"70156b3239ed4eee8cae23ad0868c08b.16","name":"彩繪變電箱","lat":22.988533,"lng":120.198118,"codename":"TA25"},
		{"guid":"837e16fe608e47139d860bed16221a49.16","name":"小西門圓環","lat":22.990222,"lng":120.197941,"codename":"TA10"},
		{"guid":"ae5161574b5b43919ac357d5d87de32d.16","name":"昆沙宮牌樓","lat":22.989999,"lng":120.199197,"codename":"TA11"},
		{"guid":"409688e144ac37a7a69737b8ff614df0.16","name":"101忠狗","lat":22.989575,"lng":120.197941,"codename":"TA15"},
		{"guid":"1c7178c573463bdbabeb5510f454313f.16","name":"原台南刑務所地磚","lat":22.988697,"lng":120.200149,"codename":"TA22"},
		{"guid":"86c28c8e787045459144724733ad5f89.16","name":"小巷彩繪","lat":22.989657,"lng":120.197744,"codename":"TA14"},
		{"guid":"f288bfe1ff9146aca25c09082fb3938c.16","name":"變電箱","lat":22.989065,"lng":120.197608,"codename":"TA19"},
		{"guid":"62c1fbcd6dd04be38cd8f4fbe97027ca.16","name":"控制箱","lat":22.988702,"lng":120.19768,"codename":"TA21"},
		{"guid":"4ee08ad31ba73870acaef1ed20bc7727.16","name":"和意公園兒童遊戲場","lat":22.988597,"lng":120.199355,"codename":"TA23"},
		{"guid":"9c48346ab3e144b68fe600591ee56fa0.16","name":"永福路ㄧ段108巷變電箱","lat":22.98891,"lng":120.19944,"codename":"TA20"},
		{"guid":"ce0849a8e4e345cd969766f026d1f94f.16","name":"和意堂七祖仙師","lat":22.989798,"lng":120.198476,"codename":"TA12"},
		{"guid":"7a65f11229e135d5b645ecc5f40f1b35.16","name":"小西門  招牌","lat":22.988553,"lng":120.199078,"codename":"TA24"},
		{"guid":"1cc7a770b6883b6fbfd5b176eecb05ac.16","name":"三級古蹟昆沙宮","lat":22.989778,"lng":120.199076,"codename":"TA13"},
		{"guid":"5b374e8c9cae40abad977fa45f6ccf21.16","name":"小西門公園","lat":22.988502,"lng":120.199169,"codename":"TA26"},
		{"guid":"944a3ebb9d0e3f03bbc887b8a1bb9663.16","name":"煙波飯店","lat":22.989549,"lng":120.200182,"codename":"TA16"},
		{"guid":"43ef3fdcbe6042c4aec6366ac37ad664.16","name":"臺南浸信會","lat":22.988968,"lng":120.203347,"codename":"TB20"},
		{"guid":"1388295ffada4f4e92875d8a1789211a.16","name":"見證愛情","lat":22.989601,"lng":120.200568,"codename":"TB12"},
		{"guid":"8ff39b18a8c74cc9a416d1076a0cf52b.16","name":"地磚-第三代法院","lat":22.9898,"lng":120.200703,"codename":"TB10"},
		{"guid":"587e229a8c313de7874b85ee6702ebfb.16","name":"法務部行政執行署台南分署","lat":22.988993,"lng":120.201534,"codename":"TB18"},
		{"guid":"5e3107469191484787c8242a72470ea5.16","name":"彩繪變電箱 建興","lat":22.989251,"lng":120.203263,"codename":"TB15"},
		{"guid":"45a72878158b4217b97f8ceeba294b75.16","name":"大象家族彩繪電信箱","lat":22.989482,"lng":120.201955,"codename":"TB13"},
		{"guid":"372755bcdafd4cf69a066b240ff9c488.16","name":"司法之聲","lat":22.98913,"lng":120.200912,"codename":"TB16"},
		{"guid":"3018fefe10863ee1ab1ae068eb8621a8.16","name":"台南地方法院觀護人室","lat":22.988452,"lng":120.202018,"codename":"TB21"},
		{"guid":"39ceadf9f0034db1abd47b36bc4eecd4.16","name":"台南司法博物館","lat":22.989712,"lng":120.201148,"codename":"TB11"},
		{"guid":"5f0763e622e24b56bbc534bba2505057.16","name":"臺南市政府財政稅務局","lat":22.987963,"lng":120.201979,"codename":"TB24"},
		{"guid":"bbf3e52286ea46d59ac804213ef55798.16","name":"台灣電力公司台南區營業處","lat":22.988025,"lng":120.201792,"codename":"TB23"},
		{"guid":"8c57910730b33469b463f414744c0f79.16","name":"行政執行署 裝置藝術","lat":22.989012,"lng":120.201836,"codename":"TB17"},
		{"guid":"c463c58b17df4a8b84f1217c3ac53a5b.16","name":"見證愛情旁彩繪變電箱","lat":22.989315,"lng":120.200385,"codename":"TB14"},
		{"guid":"bb89ffa90e09466fb9be44d3dc7e5979.16","name":"永續發展年","lat":22.988427,"lng":120.200317,"codename":"TB22"},
		{"guid":"54791dd368c339a38e6a2dfac4a08576.16","name":"御路","lat":22.98899,"lng":120.200953,"codename":"TB19"},
		{"guid":"e18aa725ac5d31dfaba58792e35de40f.16","name":"簡愛大石","lat":22.987057,"lng":120.205143,"codename":"TC20"},
		{"guid":"11a8912a85bc48148f883577384b3b4c.16","name":"南門路郵局","lat":22.988605,"lng":120.204655,"codename":"TC15"},
		{"guid":"b10ac14061483323b8327295919ec666.16","name":"瑞慶珠寶","lat":22.988294,"lng":120.204663,"codename":"TC16"},
		{"guid":"895c2140a5284c79b731934ee251492b.16","name":"彩繪變電箱","lat":22.988927,"lng":120.20548,"codename":"TC10"},
		{"guid":"c57d1643bed24fc8ae9a9ce62ad32c72.16","name":"自來水公司第六區管理處","lat":22.988102,"lng":120.20434,"codename":"TC17"},
		{"guid":"44cb97ee83e24973923536eacdf06061.16","name":"變電箱","lat":22.98891,"lng":120.206216,"codename":"TC11"},
		{"guid":"aa42395bfc364e10b7867fff69fe56de.16","name":"台灣基督長老教會台南南門教會","lat":22.987235,"lng":120.20456,"codename":"TC19"},
		{"guid":"21b4659d11754b478fb20e8d07cda8aa.16","name":"台南市童軍會","lat":22.987749,"lng":120.204309,"codename":"TC18"},
		{"guid":"e2823d00abb442db9b83c923a7fe2dad.16","name":"台電變電箱","lat":22.988872,"lng":120.20699,"codename":"TC13"},
		{"guid":"27bb9b44087f3bb7b3a4f98d3cf4b1e7.16","name":"臺灣銀行臺南分行","lat":22.98886,"lng":120.206654,"codename":"TC14"},
		{"guid":"f76c521d680e4270adc47bc6ce6c4e00.16","name":"綠地之箱","lat":22.988909,"lng":120.207281,"codename":"TC12"},
		{"guid":"32868fc9e0f6300fb64400d46586a982.16","name":"貓狗剪影","lat":22.986452,"lng":120.204393,"codename":"TD20"},
		{"guid":"0946d10d831c4771b7a0fed22164eb1f.16","name":"府城南門公園-大南門城旁大榕樹","lat":22.98659,"lng":120.203537,"codename":"TD15"},
		{"guid":"0cfd3f4a1ebb3995972d204a0e99fe73.16","name":"西門砲台","lat":22.986976,"lng":120.203706,"codename":"TD10"},
		{"guid":"da59d4f39f1f43868560e5bbe3270279.16","name":"府城南門公園","lat":22.986508,"lng":120.204149,"codename":"TD18"},
		{"guid":"a4570fa3b653490e987d6ba5e3662cb8.16","name":"南門碑林","lat":22.986228,"lng":120.203054,"codename":"TD21"},
		{"guid":"9d9848ae6a6d489f8170a8c9c5555ceb.16","name":"萬壽宮圖","lat":22.986487,"lng":120.203273,"codename":"TD19"},
		{"guid":"e68a5f716fdb448197df0fff94c54272.16","name":"南門放送局灰作修復匠師圖像","lat":22.986221,"lng":120.203809,"codename":"TD22"},
		{"guid":"50e3acdc627045e2b9132b8db7691044.16","name":"大南門地磚","lat":22.986011,"lng":120.204028,"codename":"TD24"},
		{"guid":"097500b18ef64a5096bb15135aec63c5.16","name":"山水變電箱","lat":22.986154,"lng":120.203367,"codename":"TD23"},
		{"guid":"b2e824e3f96c454385d6f32597f234f8.16","name":"台南府城大南門與放送局沿革","lat":22.986758,"lng":120.204184,"codename":"TD12"},
		{"guid":"0dc0b41e701c4cedb5548babaa43a291.16","name":"重建安瀾橋碑記","lat":22.986618,"lng":120.202751,"codename":"TD14"},
		{"guid":"ede124bbb70b44cfb3d734137727b28c.16","name":"台南市郊外產業道路改修紀念碑","lat":22.986575,"lng":120.203051,"codename":"TD17"},
		{"guid":"17660b6b68893bb8b70a9c40f421f112.16","name":"報恩閣碑記","lat":22.986582,"lng":120.203085,"codename":"TD16"},
		{"guid":"6cd674adda5b4352b57e203908dbb8c4.16","name":"甘棠遺蔭","lat":22.986715,"lng":120.203178,"codename":"TD13"},
		{"guid":"78a629dd4e754012a3ee2c210126c943.16","name":"府城大南門","lat":22.986759,"lng":120.203666,"codename":"TD11"},
		{"guid":"d37f008b94994734bc55010ba608940e.16","name":"永續發展變電箱","lat":22.98728,"lng":120.207456,"codename":"TE16"},
		{"guid":"2f052239e4f831eabb187dc9e9e0b2a9.16","name":"星蛋","lat":22.987938,"lng":120.207093,"codename":"TE11"},
		{"guid":"27efd43458a2484cbf41b354639d8282.16","name":"人行道扶手","lat":22.985829,"lng":120.20809,"codename":"TE23"},
		{"guid":"310c7fd2602e4bd5a6b886953f2c052c.16","name":"樹林變電箱","lat":22.986146,"lng":120.209101,"codename":"TE22"},
		{"guid":"15b18d87fab348549132a8e23f854c05.16","name":"大樹下的瀑布電箱","lat":22.987579,"lng":120.206608,"codename":"TE14"},
		{"guid":"9388ddb303ea4bb0aff3c402a419eb12.16","name":"郡王花園","lat":22.98617,"lng":120.208192,"codename":"TE21"},
		{"guid":"d1bfada324644e76be5aee1489a08548.16","name":"臨水夫人媽廟","lat":22.98824,"lng":120.207323,"codename":"TE10"},
		{"guid":"4b45936ebd7a4d57a85399e725a44a8b.16","name":"台南市總工會","lat":22.987529,"lng":120.20713,"codename":"TE15"},
		{"guid":"e30c493d069b4c77aff3a2380a953ab6.16","name":"郡王里活動中心","lat":22.987903,"lng":120.2069,"codename":"TE12"},
		{"guid":"78b8ae666c294ecab26bfbd377217f35.16","name":"P9786 EC57 花瓶嶼變電箱","lat":22.987136,"lng":120.208801,"codename":"TE18"},
		{"guid":"c88fbe288ad24273a6674da525c6dc22.16","name":"蝴蝶人行道裝飾","lat":22.986176,"lng":120.208659,"codename":"TE20"},
		{"guid":"eddf5b791e6340f6a6b125c1afdae838.16","name":"彩繪變電箱-群山漫延","lat":22.987177,"lng":120.20698,"codename":"TE17"},
		{"guid":"49f35421d4db3c4b8449f4df7232f41b.16","name":"鹿角枝","lat":22.986368,"lng":120.20842,"codename":"TE19"},
		{"guid":"c267a020399648b7b554c1d9766e82ad.16","name":"有牌樓的彩繪變電箱","lat":22.987752,"lng":120.206817,"codename":"TE13"},
		{"guid":"29ce45d88866447481375e81c2d868c3.12","name":"中華聖母","lat":22.987199,"lng":120.20921,"codename":"TF11"},
		{"guid":"468c547b63354d91a74fe4edeb9a889e.16","name":"彩繪變電箱-崑明殿","lat":22.98579,"lng":120.210401,"codename":"TF17"},
		{"guid":"74bca3ea426132eba465e709544f4aff.16","name":"第一銀行-竹溪分行","lat":22.98631,"lng":120.211242,"codename":"TF14"},
		{"guid":"e2f955e604f44853aa58f60a02575e06.16","name":"六合境福德祠","lat":22.986922,"lng":120.209377,"codename":"TF12"},
		{"guid":"b73609b43c2d453c97be331598191101.16","name":"小南城隍廟","lat":22.985484,"lng":120.210721,"codename":"TF18"},
		{"guid":"52aade1dcd264af08f5d4118c65bf37d.16","name":"P9786 FC3201 樹變電箱","lat":22.986628,"lng":120.209654,"codename":"TF13"},
		{"guid":"b34fc27a17f7412ca469181725de3642.16","name":"開山聖教會","lat":22.98631,"lng":120.210172,"codename":"TF15"},
		{"guid":"90e33239851e40a89eb2a0014fa2bfcc.16","name":"臺灣府城小南門遺址","lat":22.98605,"lng":120.209849,"codename":"TF16"},
		{"guid":"ab032a2a3f874e9fb872b15d2cc05490.16","name":"天主堂","lat":22.987285,"lng":120.209128,"codename":"TF10"},
		{"guid":"175388939d33385c9e90f7c0aa779a65.16","name":"椅子四重奏","lat":22.986948,"lng":120.200618,"codename":"TG11"},
		{"guid":"1b40b616a41a4d1592eee6aae2a4954a.16","name":"泰國雕像","lat":22.986459,"lng":120.200239,"codename":"TG13"},
		{"guid":"6d6449c8d8ef4d64b54ec8056d09dcc4.16","name":"變電箱","lat":22.984707,"lng":120.19771,"codename":"TG18"},
		{"guid":"4c546abfb78842a9a8e65008e82e2ec5.16","name":"彩繪小電箱","lat":22.986808,"lng":120.200219,"codename":"TG12"},
		{"guid":"6277944aeee44da59a502b0968d660f6.16","name":"帶我去遠方","lat":22.987174,"lng":120.200689,"codename":"TG10"},
		{"guid":"6ccc3a802b144d0dab95c86c9cd2c0ca.16","name":"馬賽克壁畫","lat":22.98499,"lng":120.200458,"codename":"TG17"},
		{"guid":"489130fc027740cb8f0b82bd4c2e0925.16","name":"聖若瑟天主堂","lat":22.985005,"lng":120.199244,"codename":"TG16"},
		{"guid":"dd3a2e843a0e4fa69bffc10233a87986.16","name":"小西門里活動中心","lat":22.984305,"lng":120.199638,"codename":"TG19"},
		{"guid":"c3c3981e9e8c48e8917464b025831695.16","name":"安樂聖教會","lat":22.984203,"lng":120.200034,"codename":"TG20"},
		{"guid":"732512e6e79f351fbc25dac7eb7799cf.16","name":"新光三越","lat":22.986292,"lng":120.197686,"codename":"TG14"},
		{"guid":"2e1445a4716241e08575aaa9eb817719.16","name":"變電箱","lat":22.985854,"lng":120.197686,"codename":"TG15"},
		{"guid":"26010b3a5df847c8a1cecab066819db3.16","name":"無敵海景變電箱","lat":22.985021,"lng":120.204051,"codename":"TH19"},
		{"guid":"4648dcb0b123496cae4e304d90a0d141.16","name":"花草與電箱","lat":22.98511,"lng":120.202316,"codename":"TH18"},
		{"guid":"335a3c1ea0e437af8176be9c88944913.16","name":"塗鴉牆愛心門","lat":22.985322,"lng":120.202553,"codename":"TH17"},
		{"guid":"bd36543f5df9486583a1fb9899a6fc07.16","name":"跳跳跳","lat":22.984818,"lng":120.203422,"codename":"TH21"},
		{"guid":"52590ed8c37048fd8d6c74247ecd84e0.16","name":"山水畫變電箱","lat":22.985684,"lng":120.201742,"codename":"TH15"},
		{"guid":"346bd7b23b394af180013cd5014feaf7.16","name":"樹林變電箱","lat":22.98601,"lng":120.201836,"codename":"TH13"},
		{"guid":"4b0e4913545c454a96f7ef1414e0e14f.16","name":"舊城聯盟寧南館","lat":22.984958,"lng":120.201902,"codename":"TH20"},
		{"guid":"a08e452a6476404bbd8631ad6d1f72b7.16","name":"忠義樹林彩繪變電箱","lat":22.98638,"lng":120.201947,"codename":"TH10"},
		{"guid":"4bffc3b51e1643d3baf138aeb9cc0688.16","name":"彩繪變電箱","lat":22.986034,"lng":120.202683,"codename":"TH12"},
		{"guid":"e10d0a07bd15425cb0d5b46844e9d10a.16","name":"大南社區","lat":22.984692,"lng":120.203858,"codename":"TH22"},
		{"guid":"d60994631b9d4d689a8e822739ff4089.16","name":"變電箱 山河","lat":22.983929,"lng":120.201828,"codename":"TH25"},
		{"guid":"8cff25a314e442daa7913302b6530efa.16","name":"櫻花電箱","lat":22.985443,"lng":120.204119,"codename":"TH16"},
		{"guid":"c81fe4a27e4f4383b153ef284954d879.16","name":"變電箱 山水","lat":22.984603,"lng":120.20179,"codename":"TH23"},
		{"guid":"a922d4c18132409abecba905cc81c34b.16","name":"向日葵變電箱","lat":22.985895,"lng":120.202921,"codename":"TH14"},
		{"guid":"5f27b6bb1544441285604e6a14ab12bd.16","name":"樹的變電箱","lat":22.984246,"lng":120.204141,"codename":"TH24"},
		{"guid":"0361f1be9b934c3fbe7495768787c513.16","name":"變電箱nearby中山國中","lat":22.98381,"lng":120.204045,"codename":"TH26"},
		{"guid":"6bc6664f4f144a64a85fac09e4f96787.16","name":"愛夏子","lat":22.986316,"lng":120.202514,"codename":"TH11"},
		{"guid":"c3a333e2c3ed45a7abdc205bcd0664e2.16","name":"鐵門上的森林","lat":22.985875,"lng":120.204645,"codename":"TI12"},
		{"guid":"4bda1b33403e4168adbe41ac6d0e38f9.16","name":"城池演變","lat":22.985882,"lng":120.205817,"codename":"TI11"},
		{"guid":"810b3c0adc20474282fcd8837e457b01.16","name":"柏楊文物館","lat":22.985401,"lng":120.205857,"codename":"TI19"},
		{"guid":"f28c7cc099a14af19c3b4ef34b0cfed4.16","name":"樹林街河流電箱","lat":22.985962,"lng":120.204408,"codename":"TI10"},
		{"guid":"57234d150c5430c08332014350020892.16","name":"City Sport 城市運動工作室","lat":22.985366,"lng":120.205185,"codename":"TI22"},
		{"guid":"9d97f6eef07d4a46998c9501d30e9414.16","name":"P9785 BE1375湖邊變電箱","lat":22.983972,"lng":120.205503,"codename":"TI28"},
		{"guid":"98a465d390ca3acd8096b0027f1cc7b3.16","name":"老宅·南","lat":22.984874,"lng":120.204963,"codename":"TI27"},
		{"guid":"22759ce85dd832aa84317f3f0a2bab07.16","name":"斑馬電箱","lat":22.985497,"lng":120.205431,"codename":"TI17"},
		{"guid":"71c129afe42d4d6bacfa501a8b775c87.16","name":"台南大學-國家研究中心","lat":22.985297,"lng":120.206229,"codename":"TI24"},
		{"guid":"515b85c8171c466abc7976deb2ebbc4b.16","name":"南大南風廣場花圃","lat":22.984978,"lng":120.206012,"codename":"TI26"},
		{"guid":"daaa564b42ee49169231801be5a5c91e.16","name":"南大南風廣場彩繪牆壁","lat":22.9851,"lng":120.205841,"codename":"TI25"},
		{"guid":"6eedb0383bc24f0aa554e3d83064bc4a.16","name":"樹林街的樹林電箱","lat":22.985761,"lng":120.205347,"codename":"TI13"},
		{"guid":"db38dd8822903727823a982a0afe58e6.16","name":"自行車路線圖","lat":22.985673,"lng":120.205918,"codename":"TI14"},
		{"guid":"f92c12b8b77e4cc296e46a7fe7ebf1d6.16","name":"古城牆歷史解說牌","lat":22.985503,"lng":120.206733,"codename":"TI16"},
		{"guid":"c4fce470837c428188cc281746542cbb.16","name":"郡王里地標","lat":22.985381,"lng":120.207954,"codename":"TI21"},
		{"guid":"7dddc22cf6f6444c9d168454f1c095f2.16","name":"台南大學 - 天鵝情侶","lat":22.985319,"lng":120.208297,"codename":"TI23"},
		{"guid":"4c24f75cb4a64016a6e392944a149313.16","name":"南門城殘垣","lat":22.985602,"lng":120.20633,"codename":"TI15"},
		{"guid":"aebc800936f942bab3eb68c8999f54b2.16","name":"人魚娃娃","lat":22.985397,"lng":120.206274,"codename":"TI20"},
		{"guid":"5d0076282d014653a9227ea7bfcb57fb.16","name":"NUTN ART D.C.","lat":22.985452,"lng":120.206403,"codename":"TI18"},
		{"guid":"e64e7670b5f543e2acd84ab7fdc0265e.16","name":"芒草電箱","lat":22.983995,"lng":120.210855,"codename":"TJ13"},
		{"guid":"e4dc8743bbae4d7db48f23fe0e9c0410.16","name":"菩提路","lat":22.983181,"lng":120.210032,"codename":"TJ19"},
		{"guid":"2c61675269d04e1896e88bd4d29cc08f.16","name":"路燈下孤獨的彩繪變電箱","lat":22.983099,"lng":120.210226,"codename":"TJ20"},
		{"guid":"bb479e1f27be31bc874c27d99304827c.16","name":"法華寺重建紀念碑","lat":22.984209,"lng":120.209775,"codename":"TJ11"},
		{"guid":"d552dbab959644bebb3d94958cbd9a54.16","name":"親愛的佛祖","lat":22.983992,"lng":120.209951,"codename":"TJ14"},
		{"guid":"ce8e2aae8e6549b4ba63d18c9448636d.16","name":"法華寺","lat":22.98377,"lng":120.209599,"codename":"TJ15"},
		{"guid":"4857294f948e417e8f264789c52d9a4a.16","name":"法華寺","lat":22.983476,"lng":120.210218,"codename":"TJ16"},
		{"guid":"9f6aefe466074e5dab33328b41c5cae1.16","name":"府連三叉路口變電箱","lat":22.984716,"lng":120.210999,"codename":"TJ10"},
		{"guid":"9c6b11b166374ed48b20537686ce0327.16","name":"P9785ED95彩繪變電箱","lat":22.983228,"lng":120.209199,"codename":"TJ18"},
		{"guid":"ca382c99434c454ca0131c31dcf5084c.16","name":"C2-3040 全台首學電信箱","lat":22.984064,"lng":120.211071,"codename":"TJ12"},
		{"guid":"57c2e960d9653a62906cae36c58f2a10.16","name":"明倫堂","lat":22.983366,"lng":120.20973,"codename":"TJ17"},
		{"guid":"3bd06f140153495cae8098716b7f8fdc.16","name":"樹蘭解說牌","lat":22.987871,"lng":120.207325,"codename":"WA16"},
		{"guid":"8160c748c6363ce98d9a1bf874d080a6.16","name":"仁厚境福徳祠金爐","lat":22.987695,"lng":120.208502,"codename":"WA20"},
		{"guid":"169722eccff44685aaa808f90e92d4c8.11","name":"沈葆楨遺像","lat":22.987529,"lng":120.207721,"codename":"WA22"},
		{"guid":"930d6c04037c4e67af25451225dee236.11","name":"延平郡王祠大砲","lat":22.987624,"lng":120.208039,"codename":"WA21"},
		{"guid":"e8ef7741d4f044b9aa74e732f24d761a.16","name":"祭器所","lat":22.987724,"lng":120.207603,"codename":"WA19"},
		{"guid":"06b721eeaf284b71a997afc7b16efa70.16","name":"延平郡王祠小橋","lat":22.98808,"lng":120.207509,"codename":"WA14"},
		{"guid":"8c9f3589dff344179ac28c2004ae45d9.11","name":"延平郡王祠 岩泉","lat":22.988246,"lng":120.207717,"codename":"WA11"},
		{"guid":"adadc4c9db3e4b4e98195233f35054df.11","name":"延平郡王祠 龍雕","lat":22.988172,"lng":120.207954,"codename":"WA12"},
		{"guid":"17e1aecc36b7474f8097a12d7fc8ec2f.11","name":"鄭成功騎馬像","lat":22.988549,"lng":120.20769,"codename":"WA10"},
		{"guid":"5ef5ff53b14a4f01ad2838ada27015f6.11","name":"延平郡王祠修建紀念","lat":22.987774,"lng":120.208319,"codename":"WA18"},
		{"guid":"fe62cc1041e8497b9664ced41979f048.16","name":"延平郡王祠擎天門","lat":22.987907,"lng":120.207777,"codename":"WA15"},
		{"guid":"4051c379bd124a55b3c5743afdbabc7b.11","name":"忠肝義膽牌坊","lat":22.987822,"lng":120.208115,"codename":"WA17"},
		{"guid":"56be10bb660c4d3e9a0633bbc230bce9.11","name":"明延平郡王祠","lat":22.988132,"lng":120.208239,"codename":"WA13"},
		{"guid":"3217d7c15df14f4faebb8603ae5fa38a.11","name":"台南市立博物館","lat":22.987496,"lng":120.208186,"codename":"WA23"},
		{"guid":"4d8a1ca056cd4e14a44063fe87e42245.16","name":"台南大學 - 校徽燈柱","lat":22.984327,"lng":120.206571,"codename":"WB34"},
		{"guid":"3751fb80d2754d34a38e8cf6253691cc.16","name":"台南大學 - 學生宿舍樓長室","lat":22.982861,"lng":120.206379,"codename":"WB49"},
		{"guid":"22c82fed0b8e4d758e6e69d0494d1380.16","name":"中山館雕像","lat":22.984159,"lng":120.207774,"codename":"WB38"},
		{"guid":"20592652df6a4d41a99ab1100de76f1e.16","name":"台南大學 - 進修推廣組","lat":22.983781,"lng":120.206937,"codename":"WB40"},
		{"guid":"adaa866a26554634b724b7a12d4f1bc2.16","name":"台南大學 - 親愛精誠","lat":22.983766,"lng":120.207158,"codename":"WB41"},
		{"guid":"65d558d999353137a5484c1e2980f91d.16","name":"誠正樓史","lat":22.984254,"lng":120.207991,"codename":"WB35"},
		{"guid":"94b1cc7d01914d9b986d5c09b0ac003c.16","name":"台南大學 - 涼亭","lat":22.984471,"lng":120.206782,"codename":"WB32"},
		{"guid":"231bae7583ab4a4eafc67681552ec3bb.16","name":"化雨亭","lat":22.983124,"lng":120.206699,"codename":"WB47"},
		{"guid":"957e4eb53a7a4c2fa5e394765d06a309.16","name":"台南大學 - 中山體育館","lat":22.984156,"lng":120.207308,"codename":"WB39"},
		{"guid":"643a10bae0094b96a9ad5c71dd04327c.16","name":"國立臺南大學-體操廣場","lat":22.984486,"lng":120.207183,"codename":"WB31"},
		{"guid":"7304a8c8de2d4e908512c374d4b09262.16","name":"台南大學 - 懷遠齋","lat":22.983166,"lng":120.206363,"codename":"WB46"},
		{"guid":"4af20186e04e47cb9aa6dcfd28a5492d.16","name":"台南大學 - 孩童戲水","lat":22.983247,"lng":120.20709,"codename":"WB45"},
		{"guid":"7c05e08a3cae4dc2928ecf15a006b6ff.16","name":"司令台","lat":22.983413,"lng":120.207419,"codename":"WB44"},
		{"guid":"b777a49d3f61348d9562b361dbeccd32.16","name":"台南大學夜景介紹牌","lat":22.984855,"lng":120.208112,"codename":"WB23"},
		{"guid":"8e0c76cb84b33e508c6a57e42135a8a6.16","name":"台南師範1899","lat":22.985149,"lng":120.206916,"codename":"WB12"},
		{"guid":"75c3db4fa2953ccfa1c0a4be7b3a69b0.16","name":"承先啟後繼往開來","lat":22.985032,"lng":120.208011,"codename":"WB17"},
		{"guid":"26dd195cb73b4d9f9dacdc32e24983f2.16","name":"台南大學 - 格致樓","lat":22.984199,"lng":120.208526,"codename":"WB36"},
		{"guid":"b40ad90204624ec085363b76efb7f789.16","name":"台南大學 - 行政大樓魚池","lat":22.984188,"lng":120.208204,"codename":"WB37"},
		{"guid":"81667ff38ac0477c9b19b90e6aff379c.16","name":"台南大學 - 薪傳","lat":22.984529,"lng":120.207949,"codename":"WB30"},
		{"guid":"999b3ab32c8f48e58c3d788165e02d9d.16","name":"親情","lat":22.984471,"lng":120.208413,"codename":"WB33"},
		{"guid":"a3e9d75c292445b1a82fb6f3897992d6.16","name":"台南大學 - 入口圓環","lat":22.985082,"lng":120.208182,"codename":"WB15"},
		{"guid":"daa9de4411ae3c978b9a60e4b859bf98.16","name":"重建啟明苑記","lat":22.985048,"lng":120.207445,"codename":"WB16"},
		{"guid":"69e04fd914c945cd9abb91e29b614977.16","name":"台南大學 - 雅音樓","lat":22.984964,"lng":120.208538,"codename":"WB19"},
		{"guid":"c113dbe8e29848698348ca539c6cd823.16","name":"雅音樓 音樂廳","lat":22.98476,"lng":120.208594,"codename":"WB26"},
		{"guid":"93bd2a8986c547be9dea81501fd3b2bd.16","name":"台南大學 - 雅風庭園","lat":22.98493,"lng":120.208862,"codename":"WB20"},
		{"guid":"46df6de32a794801b5612d3cd35c85df.16","name":"台南大學 - 美術系所","lat":22.983634,"lng":120.208334,"codename":"WB42"},
		{"guid":"f4a5de71e30a43069739da39c87938c1.16","name":"臺南大學思誠樓","lat":22.984895,"lng":120.207341,"codename":"WB22"},
		{"guid":"50764a1b2a824ef7a7d6ac6fe656fd8e.16","name":"台南大學 - 見證歷史","lat":22.984828,"lng":120.20838,"codename":"WB24"},
		{"guid":"8148bbebaf3e4eb9a6353b4534aca1b9.16","name":"台南大學 - 成就","lat":22.984682,"lng":120.207809,"codename":"WB28"},
		{"guid":"1ec3f4d0cb614585be6f87f8c5e9f4fa.16","name":"台南大學 - 校門","lat":22.985168,"lng":120.207971,"codename":"WB10"},
		{"guid":"cdc3ae8b10c54ad09913ce1f4f389a0f.16","name":"台南大學 - 狂勁","lat":22.985137,"lng":120.207244,"codename":"WB14"},
		{"guid":"97dc3de1096d4a279ff170ac5969faad.16","name":"台南大學 - 鐘樓","lat":22.985167,"lng":120.207734,"codename":"WB11"},
		{"guid":"2ce830a977df304d9e41249d2ed4cf94.16","name":"青蛙石像","lat":22.984928,"lng":120.207903,"codename":"WB21"},
		{"guid":"82e1fb07ff324050b92eb73e1f70c0ee.16","name":"台南大學 - 美術館","lat":22.983093,"lng":120.208141,"codename":"WB48"},
		{"guid":"ccdd59f37cff4e299af0e1c3b8a45f32.16","name":"文薈樓史簡介","lat":22.984721,"lng":120.206497,"codename":"WB27"},
		{"guid":"978f48fa451f4b66983a278d58d6f90c.16","name":"5F造景","lat":22.985148,"lng":120.206626,"codename":"WB13"},
		{"guid":"56a47b6c00804550954d6e72a28801ca.16","name":"思誠樓匾額","lat":22.984788,"lng":120.206982,"codename":"WB25"},
		{"guid":"39870b0beeef494c8c6643548c4e4dc3.16","name":"台南大學 - 校訓","lat":22.984624,"lng":120.208127,"codename":"WB29"},
		{"guid":"8f458361eb4d4022b76793ddd91b0976.16","name":"台南大學 - 彩風廣場","lat":22.983515,"lng":120.208522,"codename":"WB43"},
		{"guid":"0bc9c3b562ee428d8a43f3ff92efd97d.16","name":"台南大學 - 有教無類","lat":22.984982,"lng":120.207783,"codename":"WB18"},
		{"guid":"333e07b2bd4b4a6c805860ea217673bc.16","name":"動物雕像","lat":22.977818,"lng":120.205944,"codename":"WC26"},
		{"guid":"3c4d34c1b9d04e2382dd1af33fd8fca5.16","name":"田徑運動場壁畫","lat":22.979087,"lng":120.205928,"codename":"WC22"},
		{"guid":"b4a66c6d3d8e4a93a66104bb8ef5e28f.16","name":"大猩猩吼吼","lat":22.977632,"lng":120.205443,"codename":"WC27"},
		{"guid":"9f0ed65e28d74b7482e666e464aaa255.16","name":"松柏娛樂中心","lat":22.979398,"lng":120.205222,"codename":"WC21"},
		{"guid":"a646a573feda418d9f8a3efb2337843c.16","name":"臺南體育場司令台","lat":22.97819,"lng":120.20581,"codename":"WC25"},
		{"guid":"42b2f61d03f346c092b640d74518120f.16","name":"獅子會的火炬","lat":22.979038,"lng":120.205693,"codename":"WC23"},
		{"guid":"545130357c1b4116974f445c1511da00.16","name":"歡迎您","lat":22.978412,"lng":120.205406,"codename":"WC24"},
		{"guid":"90de92688daf3a91a0f9c877d54fb92c.16","name":"這不是鋼蛋","lat":22.980981,"lng":120.205871,"codename":"WC15"},
		{"guid":"1c57b058bb6745729aa970a827851d74.16","name":"獅","lat":22.980984,"lng":120.205531,"codename":"WC14"},
		{"guid":"b6578b5397b44da0a46293bf33809d63.16","name":"Tainan Baseball Stadium","lat":22.981238,"lng":120.205366,"codename":"WC10"},
		{"guid":"0220192b66f747c5be9f332fc5972749.16","name":"台南市立游泳池","lat":22.980133,"lng":120.204952,"codename":"WC20"},
		{"guid":"238cc1306ea94599ab06b227f9bad242.16","name":"台南棒球場壁畫","lat":22.980703,"lng":120.205637,"codename":"WC17"},
		{"guid":"c9f0e35c498b473bb36114ad8047f0ce.16","name":"臺南市立棒球場 Tainan Municipal Baseball Stadium","lat":22.981126,"lng":120.20552,"codename":"WC13"},
		{"guid":"e5bc3fb3c1a33969824340b05c679399.16","name":"有抓痕的棒球","lat":22.981173,"lng":120.206192,"codename":"WC12"},
		{"guid":"4008b5f2271441e5950ed4779b8571f4.16","name":"粉紅森林","lat":22.980473,"lng":120.205271,"codename":"WC18"},
		{"guid":"e9cfbf96ee27358cba118f336ac9b3f0.16","name":"統一獅坐騎","lat":22.980214,"lng":120.205306,"codename":"WC19"},
		{"guid":"ac2abe5578ff3f5e9c8b5193b3b4a1e0.16","name":"統一獅機器人","lat":22.98089,"lng":120.205345,"codename":"WC16"},
		{"guid":"f6d8770432c04ac8babb0d625b0d8ab0.16","name":"彩繪變電箱-向日葵","lat":22.981215,"lng":120.206384,"codename":"WC11"},
		{"guid":"893b17e5133e44a697566dcdcd2b12fe.16","name":"水交社公園","lat":22.978109,"lng":120.203381,"codename":"WD13"},
		{"guid":"07af1a5fbf2d4a9aa216e3bda50ca552.16","name":"隊形飛機","lat":22.978141,"lng":120.202094,"codename":"WD12"},
		{"guid":"2cd0bfb6f1474265b9e7d0789c1c0527.16","name":"諾亞方舟","lat":22.977483,"lng":120.202755,"codename":"WD15"},
		{"guid":"1674626445e6416a966906056048efe1.16","name":"水交社","lat":22.978168,"lng":120.202508,"codename":"WD11"},
		{"guid":"767e98424c203ca0b90db7833f1c1c22.16","name":"水交社園區遊客中心","lat":22.977792,"lng":120.201518,"codename":"WD14"},
		{"guid":"ea0fc2235a9e400ca9d245bf8e417398.16","name":"雷虎L","lat":22.978237,"lng":120.20186,"codename":"WD10"},
		{"guid":"7f3bcf8bec993b35b2cad141b3156b9f.16","name":"水交社佈告欄","lat":22.97651,"lng":120.200476,"codename":"WD31"},
		{"guid":"1938f3097a3b38ac9cecef89dcf4ba4c.16","name":"水交社展演館","lat":22.976815,"lng":120.201115,"codename":"WD29"},
		{"guid":"ed63224cc79f4173b06b06b72b14cd17.16","name":"公園運動設施","lat":22.977354,"lng":120.202929,"codename":"WD20"},
		{"guid":"4f2898f8909f39ddb719a2208f38151a.16","name":"冰塊","lat":22.977058,"lng":120.200851,"codename":"WD24"},
		{"guid":"a69f98401f2e3cc08107745876abedee.16","name":"地板花花時鐘","lat":22.977268,"lng":120.203255,"codename":"WD21"},
		{"guid":"bd2286b3265f42d5a0e5905cf63ef762.16","name":"藝術造型圍牆","lat":22.976911,"lng":120.202303,"codename":"WD26"},
		{"guid":"9b88abf6517434599a4ad8bd1f992184.16","name":"水交社文化園區","lat":22.976862,"lng":120.200714,"codename":"WD28"},
		{"guid":"85ad55f77b9e3a50894df27dea670a69.16","name":"光合作用小屋","lat":22.977387,"lng":120.203153,"codename":"WD17"},
		{"guid":"fc084aeabb6b40afad6a33eb660656a1.16","name":"大象溜滑梯","lat":22.977092,"lng":120.203199,"codename":"WD23"},
		{"guid":"d4d4c8b1bc3d371fa60327ac5e0beb7a.16","name":"好美麗的五個圈圈","lat":22.976144,"lng":120.200754,"codename":"WD32"},
		{"guid":"98c638b9a00f30d58a42127fbe14c3b7.16","name":"眷村主題館","lat":22.976908,"lng":120.201688,"codename":"WD27"},
		{"guid":"34d2438d7f473d9ab6c89ab11d40b512.16","name":"飛行環景器","lat":22.977189,"lng":120.202695,"codename":"WD22"},
		{"guid":"944ac9630cd53e62810c059ae12af0f3.16","name":"F5E 5229","lat":22.977362,"lng":120.200794,"codename":"WD18"},
		{"guid":"b709b44f544a497f98fb19608107ffc7.16","name":"桂子山公園遺跡","lat":22.97736,"lng":120.203481,"codename":"WD19"},
		{"guid":"9c822116d8f93c21ad72fac8da973f97.16","name":"中華 梅花","lat":22.97662,"lng":120.201337,"codename":"WD30"},
		{"guid":"a823cca8c3664b768677549630a2a9a6.16","name":"變電679","lat":22.977025,"lng":120.203493,"codename":"WD25"},
		{"guid":"7ff56c4ac0d4476fa03b8a484d990cc2.16","name":"桂子山旁公園指標","lat":22.977439,"lng":120.202554,"codename":"WD16"},
		{"guid":"0898d701127946c897a26c3f51d0f938.16","name":"永懷領袖","lat":22.979469,"lng":120.210703,"codename":"WE10"},
		{"guid":"1b31fba568124932927e32f0876ca894.16","name":"府城亭","lat":22.97767,"lng":120.210907,"codename":"WE14"},
		{"guid":"681955ab9f62422e8028cd74b11c6d4a.16","name":"網球","lat":22.978316,"lng":120.210981,"codename":"WE13"},
		{"guid":"0fc19825736e404e8f181f807d495246.16","name":"台南市體育公園徒步園區","lat":22.979353,"lng":120.210625,"codename":"WE11"},
		{"guid":"2398a6ce845f437884663a085c0f37c7.16","name":"溜滑梯","lat":22.977502,"lng":120.211251,"codename":"WE16"},
		{"guid":"fde6d355f21c4489a57a4b41f1670e12.16","name":"網球看台緣起碑","lat":22.978856,"lng":120.210822,"codename":"WE12"},
		{"guid":"951c9b523e543cf690671bc88d6c9ad2.16","name":"竹溪景觀橋","lat":22.977554,"lng":120.210546,"codename":"WE15"},
		{"guid":"3c6234a9dc4149b4898593e29eccb52e.16","name":"台南市立羽球館","lat":22.975695,"lng":120.208706,"codename":"WE22"},
		{"guid":"a3a08b298a7a455c88bea9770cd59a1e.11","name":"鼎","lat":22.977351,"lng":120.210731,"codename":"WE17"},
		{"guid":"a521ddf084d84ea1957b93eb369dea82.16","name":"動物塑像","lat":22.976345,"lng":120.209663,"codename":"WE20"},
		{"guid":"eefae240d50136d19fe0dd57486c0203.16","name":"打羽球白人雕像","lat":22.975888,"lng":120.208897,"codename":"WE21"},
		{"guid":"a7988221ec8e4c12b2b4c94e9bde4097.11","name":"鐘塔","lat":22.976633,"lng":120.210037,"codename":"WE19"},
		{"guid":"21f8e3deed4440fe8d1be88a290dbbdf.16","name":"鳳凰橋","lat":22.976641,"lng":120.209519,"codename":"WE18"}
		],
        "Teams": []
    };



    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function () { };

    // use own namespace for plugin
    window.plugin.tainan2024 = function () { };

    window.plugin.tainan2024.mode = (localStorage && localStorage.getItem('xma-tainan-mode')) || 0; // 012

    window.plugin.tainan2024.NAME_WIDTH = 60;
    window.plugin.tainan2024.NAME_HEIGHT = 28;

    window.plugin.tainan2024.labelLayers = {};
    window.plugin.tainan2024.labelLayerGroup = null;

    window.plugin.tainan2024.setupCSS = function () {
        $("<style>").prop("type", "text/css").html(''
                                                   + '.plugin-tainan-xma {'
                                                   + 'pointer-events: none;'
                                                   + 'color:#FFFFBB;'
                                                   + 'font-size:12px;line-height:13px;'
                                                   + 'text-align:center;padding:1px;'
                                                   + 'overflow:visible;'
                                                   + 'white-space:nowrap;'
                                                   + 'display: -webkit-box;'
                                                   + '-webkit-line-clamp: 2;'
                                                   + '-webkit-box-orient: vertical;'
                                                   + 'text-shadow: '
                                                   + '  1px  1px 0px black,'
                                                   + '  1px -1px 0px black,'
                                                   + ' -1px -1px 0px black,'
                                                   + ' -1px  1px 0px black,'
                                                   + '  0    0   1em black,'
                                                   + '  0    0 0.5em black;'
                                                   + 'pointer-events:none;'
                                                   + '}'
                                                   + '.plugin-tainan-xma-polygon {'
                                                   + 'pointer-events: none;'
                                                   + 'font-size:18px;line-height:20px;'
                                                   + 'font-weight: 700;'
                                                   + 'text-align:center;padding:0;'
                                                   + 'overflow: hidden;'
                                                   + 'white-space:nowrap;'
                                                   + 'display: -webkit-box;'
                                                   + '-webkit-line-clamp: 2;'
                                                   + '-webkit-box-orient: vertical;'
                                                   + 'opacity: 0.8;'
                                                   + 'text-shadow: '
                                                   + '  1px  1px 0px black,'
                                                   + '  1px -1px 0px black,'
                                                   + ' -1px -1px 0px black,'
                                                   + ' -1px  1px 0px black;'
                                                   + 'pointer-events:none;'
                                                   + '}'
                                                   + '.plugin-tengu-immunity-label {'
                                                   + 'pointer-events:none !important;'
                                                   + '}'
                                                  ).appendTo("head");
    }

    window.plugin.tainan2024.removeAll = function () {
        $.each(window.plugin.tainan2024.labelLayers, (guid) => window.plugin.tainan2024.removeLabel(guid));
    }


    window.plugin.tainan2024.removeLabel = function (guid) {
        var previousLayer = window.plugin.tainan2024.labelLayers[guid];
        if (previousLayer) {
            window.plugin.tainan2024.labelLayerGroup.removeLayer(previousLayer);
            delete window.plugin.tainan2024.labelLayers[guid];
        }
    }

    window.plugin.tainan2024.addLabel = function (portalNickname, guid, latLng) {
        var previousLayer = window.plugin.tainan2024.labelLayers[guid];
        if (!previousLayer) {
            var label = L.marker(latLng, {
                icon: L.divIcon({
                    className: 'plugin-tainan-xma',
                    iconAnchor: [window.plugin.tainan2024.NAME_WIDTH / 2, window.plugin.tainan2024.NAME_HEIGHT / 4],
                    iconSize: [window.plugin.tainan2024.NAME_WIDTH, window.plugin.tainan2024.NAME_HEIGHT],
                    html: portalNickname
                }),
                guid: guid,
                bubblingMouseEvents: true,
                interactive: false
            });
            window.plugin.tainan2024.labelLayers[guid] = label;
            label.addTo(window.plugin.tainan2024.labelLayerGroup);
        }
    }

    window.plugin.tainan2024.updatePortalLabels = function () {
        if (window.plugin.tainan2024.mode == 0) {
        } else if (window.plugin.tainan2024.mode == 1) {
            // portal
            $.each(localRESWUE.Portals, function (id, p) {
                if (p.codename) {
                    window.plugin.tainan2024.addLabel(p.codename, p.guid, { lat: p.lat, lng: p.lng });
                }
            });
        }
    }

    window.plugin.tainan2024.copyLink = function (link){if (typeof android !== "undefined") {androidCopy(link);}else{navigator.clipboard.writeText(link);}}


    var setup = function () {
        window.plugin.tainan2024.setupCSS();

        window.plugin.tainan2024.labelLayerGroup = new L.LayerGroup();
        window.layerChooser.addOverlay(window.plugin.tainan2024.labelLayerGroup, 'tainan 2024 XMA');

        window.addHook('iitcLoaded', function () {
            setTimeout(window.plugin.tainan2024.updatePortalLabels, 100);
        });

        window.addHook('iitcLoaded', function () {
            var codename = window.getURLParam("codename");
            var zoom = window.getURLParam("z");
            var open = window.getURLParam("open");
            if (codename) {
                $.each(localRESWUE.Portals, function (id, p) {
                    if (p.codename && p.codename == codename) {
                        setTimeout( function () {
                            console.log("ZOOM TO CODENAME", p);

                            const oldZoom = window.DEFAULT_ZOOM;
                            window.DEFAULT_ZOOM = parseInt(zoom) || 19;

                            var urlPortalLL = new L.LatLng(p.lat, p.lng);
                            window.zoomToAndShowPortal(p.guid, urlPortalLL);
                            window.DEFAULT_ZOOM = oldZoom;

                            if (open) {
                                let portalLink = `https://link.ingress.com/?link=https%3a%2f%2fintel.ingress.com%2fportal%2f${p.guid}&apn=com.nianticproject.ingress&isi=576505181&ibi=com.google.ingress&ifl=https%3a%2f%2fapps.apple.com%2fapp%2fingress%2fid576505181&ofl=https%3a%2f%2fintel.ingress.com%2fintel%3fpll%3d${p.lat}%2c${p.lng}%26codename=${p.codename}`;
                                let newWin = window.open(portalLink, '_blank');
                                if(!newWin || newWin.closed || typeof newWin.closed=='undefined')  {
                                    window.open(portalLink, '_self');
                                }
                            }
                        }, 5);
                    }
                });
            }
        });


        // search for portals
        window.addHook('search', function (query) {
            var term = query.term.toLowerCase().trim();
            if (term.length == 3) {
                term = term.substr(0,2) + "0" + term.substr(2);
            }
            $.each(localRESWUE.Portals, function (id, p) {
                if(p.codename) {
                    if (p.codename.toLowerCase() == term) {
                        let latLng = new L.LatLng(p.lat, p.lng);
                        let portalLink = `https://link.ingress.com/?link=https%3a%2f%2fintel.ingress.com%2fportal%2f${p.guid}&apn=com.nianticproject.ingress&isi=576505181&ibi=com.google.ingress&ifl=https%3a%2f%2fapps.apple.com%2fapp%2fingress%2fid576505181&ofl=https%3a%2f%2fintel.ingress.com%2fintel%3fpll%3d${p.lat}%2c${p.lng}%26codename=${p.codename}&z=19`;
                        let portalLinks = `<span class="ui-dialog-buttonset"><table><tr><th>XMA</th><td><a target="_blank" href="${portalLink}"><button class="ui-button button" style="width: 100%;border-radius:5px;" id="agentLinkOpen">ᅠOPENᅠ</button></a></td><td> ⧸ </td><td><button class="ui-button button" style="width: 100%;border-radius:5px;" id="agentLinkCopy" target="_blank" onclick="window.plugin.tainan2024.copyLink('${portalLink}')">ᅠCOPYᅠ</button></td></tr></table></span>`

                        query.addResult({
                            title: "[" + p.codename + "] " + p.name,
                            description: portalLinks,
                            position: { lat: p.lat, lng: p.lng } ,
                            onSelected: function(result, event) {
                                const oldZoom = window.DEFAULT_ZOOM;
                                if (window.DEFAULT_ZOOM < map.getZoom()) {
                                    window.DEFAULT_ZOOM = map.getZoom();
                                }
                                if (window.DEFAULT_ZOOM < 19) {
                                    window.DEFAULT_ZOOM = 19;
                                }

                                if (event.type === 'dblclick') {
                                    window.zoomToAndShowPortal(p.guid, latLng);
                                } else if (window.portals[p.guid]) {
                                    if (!window.map.getBounds().contains(result.position)) {
                                        window.map.setView(result.position);
                                    }
                                    window.renderPortalDetails(p.guid);
                                } else {
                                    window.selectPortalByLatLng(latLng);
                                }

                                if(window.isSmartphone()) window.show('map');

                                window.DEFAULT_ZOOM = oldZoom;
                                return true; // prevent default behavior
                            },
                        });
                    }
                }
            });
        });



        window.addHook('publicChatDataAvailable', function (data) {
            data.result.forEach((json) => {
                const guid = json[0];
                const ts = json[1];
                const plext = json[2];

                if (plext && plext['plext'] && plext['plext']["plextType"]== "SYSTEM_BROADCAST") {

                    if (plext['plext']['markup'] &&
                        plext['plext']['markup'][1][0] == 'TEXT' &&
                        plext['plext']['markup'][1][1]['plain']== ' deployed a Beacon on ')
                    {
                        const portal =  plext['plext']['markup'][2][1];

                        $.each(localRESWUE.Portals, function (id, p) {
                            if (Math.round(p.lat *1e6) == portal.latE6  &&
                                Math.round(p.lng *1e6) == portal.lngE6 ) {
                                debugger;
                                console.log('BEACON ', p.codename, ts, guid, p.lat, p.lng);

                                $.ajax({
                                    'url' : 'https://hkresistance.com/pp.php',
                                    'type' : 'GET',
                                    'data' : {
                                        'guid' : guid,
                                        'ts': ts,
                                        'lat': p.lat,
                                        'lng': p.lng,
                                        'code': p.codename
                                    },
                                    'success' : function(data) {
                                        console.log("A");
                                    },
                                    'error' : function(request,error)
                                    {
                                        console.log("B");
                                    }
                                });

                            }
                        });
                    }
                }

            });
        });

        // setup toolbar

        var tainanLayers = L.Control.extend({
            options: {
                position: 'topleft'
            },

            onAdd: function (map) {
                var button = document.createElement('a');
                button.className = 'leaflet-bar-part';
                button.addEventListener('click', function toggle () {
                    window.plugin.tainan2024.mode = (window.plugin.tainan2024.mode + 1) % 2;
                    if(localStorage) localStorage.setItem('xma-tainan-mode',  window.plugin.tainan2024.mode);
                    console.log("Mode Switch", window.plugin.tainan2024.mode);
                    window.plugin.tainan2024.removeAll();
                    console.log("tainan Removed");
                    window.plugin.tainan2024.updatePortalLabels();
                    console.log("tainan Updated");
                }, false);
                button.title = 'tainan Layer Mode';

                var tooltip = document.createElement('div');
                tooltip.className = 'leaflet-control-tainan-tooltip';
                var text = document.createTextNode("TN");
                tooltip.appendChild(text);
                button.appendChild(tooltip);

                var container = document.createElement('div');
                container.className = 'leaflet-control-tainan leaflet-bar';
                container.appendChild(button);
                return container;
            }
        });
        var ctrl = new tainanLayers;
        ctrl.addTo(window.map);

        // portal detail
        var origRenderPortalDetails =  window.renderPortalDetails ;
        window.renderPortalDetails = function (guid) {
            origRenderPortalDetails(guid);

            $.each(localRESWUE.Portals, function (id, p) {
                if (p.codename && p.guid == guid) {
                    $(`<small>[${p.codename}] </small>`).insertAfter('#portaldetails .title svg');
                }
            });
        }
    }

    setup.info = plugin_info; //add the script info data to the function as a property
    if (typeof changelog !== 'undefined') setup.info.changelog = changelog;
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end


// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.ersion, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);