// ==UserScript==
// @id             iitc-plugin-ornamennt-portal-export-machap
// @name           IITC plugin: Ornament Portal Export
// @category       Info
// @version        1.3.0.20220331.000001
// @description    Export Ornament Portal List
// @include        https://*.ingress.com/*
// @include        http://*.ingress.com/*
// @updateURL      https://gitlab.com/Machap/iitc-plugin/-/raw/master/ornamennt-portal-export.meta.js
// @downloadURL    https://gitlab.com/Machap/iitc-plugin/-/raw/master/ornamennt-portal-export.user.js
// @match          https://*.ingress.com/*
// @match          http://*.ingress.com/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
  // ensure plugin framework is there, even if iitc is not yet loaded
  if (typeof window.plugin !== 'function') {
    window.plugin = function() {};
  }

  // PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
  // (leaving them in place might break the 'About IITC' page or break update checks)
  plugin_info.buildName = 'machap';
  plugin_info.dateTimeVersion = '20220331.000001';
  plugin_info.pluginId = 'ornament_portallist';
  // END PLUGIN AUTHORS NOTE

  // PLUGIN START ////////////////////////////////////////////////////////

  // use own namespace for plugin
  const self = window.plugin.portals_list_exporter = function() {};

  const showPortalsListDialog = portals => {
    const width = 800, height = $(window).height() - 150;

    let LIST_OPTIONS = {
      showAllTag: 'All Ornament',
      showOnlyKythera: 'Kythera(ap1)',
      showOnlyKureze: 'Kureze(ap2)',
      onlyEastSide: 'EasternHemisphereOnly',
      insideSearch: 'InsideSearchResult',
      insidePolygons: 'InsidePolygons',
      withImageURL:  'Image',
      withParmalink: 'Intel',
      withCellID: 'CellID',
      showHeader: 'HeaderOutput',
    };
    if (!window.S2) delete LIST_OPTIONS['withCellID'];

    const classnameFor = name => `portals-list-${name}`;

    const getBorderInfo = coords => {
      const polygon = coords[0].map(c => L.GeoJSON.coordsToLatLng(c));

      const min = {
        lat: Math.min.apply(this, polygon.map(p => p.lat)),
        lng: Math.min.apply(this, polygon.map(p => p.lng))
      };
      const max = {
        lat: Math.max.apply(this, polygon.map(p => p.lat)),
        lng: Math.max.apply(this, polygon.map(p => p.lng))
      };

      const origin = {
        lat: min.lat - 0.01,
        lng: min.lng - 0.01
      };

      const getBorders = polygon => {
        let result = [];
        for (let i = 0, limit = polygon.length - 1; i < limit; i++) {
          result.push([polygon[i], polygon[(i + 1)]]);
        }
        result.push([polygon[polygon.length - 1], polygon[0]]);
        return result;
      };

      let borderList = [];
      coords.forEach(c => {
        const polygon = c.map(c => L.GeoJSON.coordsToLatLng(c));
        borderList.push(getBorders(polygon));
      });

      return {
        min: min,
        max: max,
        origin: origin,
        borderList: borderList,
      };
    };

    const lastSearch = window.search.lastSearch;
    const lastResult = lastSearch && lastSearch.selectedResult;
    const lastLayer = lastResult && lastResult.layer;
    let borders = [];
    if (lastLayer instanceof L.GeoJSON) {
      lastLayer.eachLayer(layer => {
        if (!layer.feature || !layer.feature.geometry) return;
        const geometry = layer.feature.geometry;
        if (geometry.type == 'Polygon') {
          borders.push(getBorderInfo(geometry.coordinates));
        } else if (geometry.type == 'MultiPolygon') {
          geometry.coordinates.forEach(c => { borders.push(getBorderInfo(c)); });
        }
      });
    }
    window.plugin.portals_list_exporter.borders = borders;

    const getPolygonInfo = polygon => {
      const min = {
        lat: Math.min.apply(this, polygon.map(p => p.lat)),
        lng: Math.min.apply(this, polygon.map(p => p.lng))
      };
      const max = {
        lat: Math.max.apply(this, polygon.map(p => p.lat)),
        lng: Math.max.apply(this, polygon.map(p => p.lng))
      };

      const origin = {
        lat: min.lat - 0.01,
        lng: min.lng - 0.01
      };

      const borders = (function() {
        let result = [];
        for (let i = 0, limit = polygon.length - 1; i < limit; i++) {
          result.push([polygon[i], polygon[(i + 1)]]);
        }
        result.push([polygon[polygon.length - 1], polygon[0]]);
        return result;
      })();

      return {
        min: min,
        max: max,
        origin: origin,
        borderList: [borders],
      };
    };

    let polygons = [];
    try {
      const drawdata_key = window.plugin.drawTools && window.plugin.drawTools.KEY_STORAGE || 'plugin-draw-tools-layer';
      const drawdata = localStorage[drawdata_key];
      const drawitems = drawdata && JSON.parse(drawdata);
      if (drawitems) {
        drawitems.forEach(item => {
          if (item.type == 'polygon') {
            polygons.push(getPolygonInfo(item.latLngs));
          }
        });
      }
    } catch (e) {
      polygons = [];
    }
    window.plugin.portals_list_exporter.polygons = polygons;

    const download = () => {
      const content = $('.portals-list-csv', dialog).val() + '\r\n';
      if (typeof window.android !== 'undefined' && window.android && window.android.shareString) {
        window.android.shareString(content);
      } else {
        const blob = new Blob([content], { type: "text/csv;charset=UTF-8" });
        const now = new Date;
        const pad0 = n => n < 10 ? '0' + n : n;
        let a = document.createElement("a");
        a.href = URL.createObjectURL(blob);
        a.download = 'g01-'
          + PLAYER.nickname + '-'
          + now.getFullYear()
          + pad0(now.getMonth() + 1)
          + pad0(now.getDate())
          + pad0(now.getHours())
          + pad0(now.getMinutes())
          + '.csv';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(a.href);
      }
    }

    const dialog = window.dialog({
      width: width + 40,
      html: '<div>'
        + $.map(LIST_OPTIONS, (value, key) => `<span style="display:inline-block;"><input type=checkbox class=${classnameFor(key)}>${value}</span>`).join()
        + `<textarea readonly class=portals-list-csv style="width: ${width}px; height: ${height}px;">`
        + `</textarea></div>`,
      buttons: [{
        text: 'Download',
        class: 'portal-list-dialog-button',
        click: () => { download(); }
      },
      {
        text: 'OK',
        class: 'portal-list-dialog-button',
        click: () => { dialog.dialog('close'); },
      }],
    });

    try {
      const options = JSON.parse(localStorage['plugin-portalList-options']);
      if (options) {
        $.each(LIST_OPTIONS, key => {
          if (options[key]) $(`.${classnameFor(key)}`, dialog).prop('checked', true);
        });
      }
    } catch (e) {}

    $(':checkbox', dialog).on('change', function() {
      let options = {};
      $.each(LIST_OPTIONS, (key) => {
        options[key] = $(`.${classnameFor(key)}`, dialog).prop('checked')
      });

      // const selected = options.showOnlyAp ? portals.filter(isKurezePhase1) : portals;
      let selected =
        options.showOnlyKythera ? portals.filter(isKythera) :
          options.showOnlyKureze ? portals.filter(isKureze) :
            options.showAllTag ? portals.filter(isAllOrnament) : portals;
      if (options.onlyEastSide) {
        selected = selected.filter(isEastSide);
      }
      if (options.insideSearch) {
        selected = selected.filter(insideSearch);
      }
      if (options.insidePolygons) {
        selected = selected.filter(insidePolygons);
      }

      $('.ui-dialog-title').text(titleForDialog(selected));
      $('.portals-list-csv', dialog).val(headerLine(options) + toCSV(selected, options));

      localStorage['plugin-portalList-options'] = JSON.stringify(options);
    }).trigger('change');
  };

  const isKythera = p => (p.options.data.ornaments || []).join(',').match(/ap1/);
  const isKureze = p => (p.options.data.ornaments || []).join(',').match(/ap2/);
  const isAllOrnament = p => (p.options.data.ornaments || []).join(',').match(/.+/);
  const isEastSide = p => (p.getLatLng().lng >= 0);

  const isInside = (b, p) => {
    p = p.getLatLng();
    if (p.lat < b.min.lat || p.lat > b.max.lat || p.lng < b.min.lng || p.lng > b.max.lng) {
      return false;
    }

    let countCrossing = 0;
    const line_a = [p, b.origin];
    b.borderList.forEach(function(borders) {
      borders.forEach(function (line_b) {
        const a0 = line_a[0], a1 = line_a[1];
        const b0 = line_b[0], b1 = line_b[1];

        const ta = (b0.lng - b1.lng) * (a0.lat - b0.lat) + (b0.lat - b1.lat) * (b0.lng - a0.lng);
        const tb = (b0.lng - b1.lng) * (a1.lat - b0.lat) + (b0.lat - b1.lat) * (b0.lng - a1.lng);
        const tc = (a0.lng - a1.lng) * (b0.lat - a0.lat) + (a0.lat - a1.lat) * (a0.lng - b0.lng);
        const td = (a0.lng - a1.lng) * (b1.lat - a0.lat) + (a0.lat - a1.lat) * (a0.lng - b1.lng);

        if ((ta * tb < 0) && (tc * td < 0)) {
          countCrossing++;
        }
      });
    });
    return countCrossing % 2;
  };

  const insideSearch = p => window.plugin.portals_list_exporter.borders.some(b => isInside(b, p));

  const insidePolygons = p => {
    let c = 0;
    window.plugin.portals_list_exporter.polygons.forEach(b => {
      if (isInside(b, p)) c++;
    });
    return c % 2;
  }

  const titleForDialog = portals =>
    'PortalList: ' + portals.length + (portals.length === 1 ? ' portal' : ' portals');

  const headerLine = (options) => {
    let header = ''
    if (options.showHeader) {
      header += 'name,lat,lng';
      header += ',guid,ornaments'
      if (options.withImageURL) header += ',image';
      if (options.withParmalink) header += ',link';
      if (options.withCellID) header += ',cell';
      header += '\r\n';
    }
    return header;
  };

  const toCSV = (portals, options) => portals.map(p => {
    const data = p.options.data;
    const name = data.title || '???';
    const ornaments = (data.ornaments || []).join(',');
    let row = [quote(name), p._latlng.lat, p._latlng.lng];
    row.push(p.options.guid);
    row.push(quote(ornaments));
    if (options.withImageURL) row.push(quote(data.image || ''));
    if (options.withParmalink) row.push(quote(getParmalink(p)));
    if (options.withCellID && window.S2) row.push(quote(getCellID(p)));
    return row.join(',');
  }).join('\r\n');

  const getParmalink = p => {
    const latlng = p._latlng.lat + ',' + p._latlng.lng;
    return 'https://intel.ingress.com/?ll=' + latlng + '&z=17&pll=' + latlng;
  };

  const getCellID = p => {
    const FACE_NAMES = ['AF', 'AS', 'NR', 'PA', 'AM', 'ST'];
    const CODE_WORDS = ['ALPHA', 'BRAVO', 'CHARLIE', 'DELTA', 'ECHO', 'FOXTROT', 'GOLF', 'HOTEL',
      'JULIET', 'KILO', 'LIMA', 'MIKE', 'NOVEMBER', 'PAPA', 'ROMEO', 'SIERRA',];

    let name = "-";
    if (window.S2) {
      var cell = S2.S2Cell.FromLatLng(L.latLng(p._latlng.lat, p._latlng.lng), 6);
      if (cell.face % 2) {
        cell = S2.S2Cell.FromFaceIJ(cell.face, [cell.ij[1], cell.ij[0]], cell.level);
      }
      name = FACE_NAMES[cell.face];
      var regionI = cell.ij[0] >> (cell.level - 4);
      var regionJ = cell.ij[1] >> (cell.level - 4);
      name += zeroPad(regionI + 1, 2) + '-' + CODE_WORDS[regionJ];
      var facequads = cell.getFaceAndQuads();
      var number = facequads[1][4] * 4 + facequads[1][5];
      name += '-' + zeroPad(number, 2);
    }
    return name;
  };

  const quote = str => '"' + str.replace(/"/g, '""') + '"';

  const getPortals = filter => {
    let selected = [];
    $.each(window.portals, function() {
      if (filter(this)) {
        selected.push(this);
      }
    });
    return selected;
  };

  self.showPortalsOnScreen = function() {
    const displayBounds = window.map.getBounds();
    showPortalsListDialog(getPortals(p => displayBounds.contains(p.getLatLng())));
  };

  self.showPortalsInPolygon = function(layer) {
    const polygon = layer.getLatLngs();

    const min = {
      lat: Math.min.apply(this, polygon.map(p => p.lat)),
      lng: Math.min.apply(this, polygon.map(p => p.lng))
    };
    const max = {
      lat: Math.max.apply(this, polygon.map(p => p.lat)),
      lng: Math.max.apply(this, polygon.map(p => p.lng))
    };

    const origin = {
      lat: min.lat - 0.01,
      lng: min.lng - 0.01
    };

    const borders = (function() {
      let result = [];
      for (let i = 0, limit = polygon.length - 1; i < limit; i++) {
        result.push([polygon[i], polygon[(i + 1)]]);
      }
      result.push([polygon[polygon.length - 1], polygon[0]]);
      return result;
    })();

    showPortalsListDialog(getPortals(p => {
      p = p.getLatLng();
      if (p.lat < min.lat || p.lat > max.lat || p.lng < min.lng || p.lng > max.lng) {
        return false;
      }

      let countCrossing = 0;
      const line_a = [p, origin];
      borders.forEach(function(line_b) {
        const a0 = line_a[0], a1 = line_a[1];
        const b0 = line_b[0], b1 = line_b[1];

        const ta = (b0.lng - b1.lng) * (a0.lat - b0.lat) + (b0.lat - b1.lat) * (b0.lng - a0.lng);
        const tb = (b0.lng - b1.lng) * (a1.lat - b0.lat) + (b0.lat - b1.lat) * (b0.lng - a1.lng);
        const tc = (a0.lng - a1.lng) * (b0.lat - a0.lat) + (a0.lat - a1.lat) * (a0.lng - b0.lng);
        const td = (a0.lng - a1.lng) * (b1.lat - a0.lat) + (a0.lat - a1.lat) * (a0.lng - b1.lng);

        if ((ta * tb < 0) && (tc * td < 0)) {
          countCrossing++;
        }
      });
      return countCrossing % 2;
    }));
  };

  const addTooltip = function(layer) {
    if (layer instanceof L.GeodesicPolygon || layer instanceof L.Polygon) {
      const $popup = $('<div class="ui-dialog-buttonset">');
      $('<button>')
        .text('List Portals')
        .on('click', function() {
          self.showPortalsInPolygon(layer);
        })
        .appendTo($popup);
      layer.bindPopup($popup[0]);
    }
  };

  self.searchResult = function(type) {
    const lastSearch = window.search.lastSearch;
    const lastResult = lastSearch && lastSearch.selectedResult;
    const layer = lastResult && lastResult.layer;
    if (!layer || !layer instanceof L.GeoJSON) return;

    let feature;
    layer.eachLayer(l => {
      if (!feature && l.feature) feature = l.feature;
    });
    if (!feature) return;

    let text;
    if (type == 0) {
      if (!feature.properties) feature.properties = {};
      feature.properties.title = lastResult.title;
      feature.properties.description = lastResult.description;
      text = JSON.stringify(feature);
    } else {
      const addPolygon = (coords) => {
        coords.forEach(polygon => {
          const latLngs = polygon.map(coords => L.GeoJSON.coordsToLatLng(coords))
          drawdata.push({ type: 'polygon', latLngs: latLngs });
        });
      }
      const addPolyline = (coords) => {
        const latLngs = coords.map(coords => L.GeoJSON.coordsToLatLng(coords))
        drawdata.push({ type: 'polyline', latLngs: latLngs });
      }
      const addMarker = (coords) => {
        const latLng = L.GeoJSON.coordsToLatLng(coords);
        drawdata.push({ type: 'marker', latLng: latLng });
      }

      let drawdata = [];
      const geometry = feature.geometry;
      if (geometry.type == 'Polygon') {
        addPolygon(geometry.coordinates);
      } else if (geometry.type == 'MultiPolygon') {
        geometry.coordinates.forEach(coords => { addPolygon(coords); });
      } else if (geometry.type == 'LineString') {
        addPolyline(geometry.coordinates);
      } else if (geometry.type == 'MultiLineString') {
        geometry.coordinates.forEach(coords => { addPolyline(coords); });
      } else if (geometry.type == 'Point') {
        addMarker(geometry.coordinates);
      } else if (geometry.type == 'MultiPoint') {
        geometry.coordinates.forEach(coords => { addMarker(coords); });
      }
      text = JSON.stringify(drawdata);
    }

    $('.portals-list-export textarea').text(text);
    return text;
  }

  self.exportSearchResult = function () {
    var html = '<p>' +
      '<span><input type="radio" name="x" onclick="window.plugin.portals_list_exporter.searchResult(0);" checked="checked">GeoJSON</span>' +
      '<span><input type="radio" name="x" onclick="window.plugin.portals_list_exporter.searchResult(1);">draw-tools</span></p>' +
      '<div><textarea readonly style="width: 570px; height: 300px; resize: vertical;">' +
      self.searchResult(0) + '</textarea></div>';
    window.dialog({
      html: html,
      width: 600,
      dialogClass: 'portals-list-export',
      title: 'Export Search Result',
    });
  };

  self.setup = function() {
    $('#toolbox').append('<a onclick="window.plugin.portals_list_exporter.showPortalsOnScreen()">OrnamentCSVOutput</a>');
    $('#toolbox').append('<a onclick="window.plugin.portals_list_exporter.exportSearchResult()">ExportSearchResult</a>');
    delete self.setup;

    $('<style>').prop('type', 'text/css').html('.portal-list-dialog-button {margin: 0 6px;}').appendTo('head');

    if (window.plugin.drawTools) {
      window.plugin.drawTools.drawnItems.eachLayer(layer => addTooltip(layer));
    }

    addHook('pluginDrawTools', e => {
      if (e.event === 'import') {
        window.plugin.drawTools.drawnItems.eachLayer(layer => addTooltip(layer));
      }
    });

    window.map.on('draw:created', e => addTooltip(e.layer));
  };

  // PLUGIN END //////////////////////////////////////////////////////////

  self.setup.info = plugin_info; // add the script info data to the function as a property

  if (!window.bootPlugins) {
    window.bootPlugins = [];
  }
  window.bootPlugins.push(self.setup);

  // if IITC has already booted, immediately run the 'setup' function
  if (window.iitcLoaded && typeof self.setup === 'function') {
    self.setup();
  }
}

// inject code into site contetx
const script = document.createElement('script');
const info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
