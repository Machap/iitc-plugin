// ==UserScript==
// @name            IITC plugin: Portal-heatmap
// @id              iitc_plugin_portalheatmap
// @category        Layer
// @version         1.0.1
// @namespace       https://github.com/IITC-CE/ingress-intel-total-conversion
// @updateURL       https://gitlab.com/Machap/iitc-plugin/-/raw/master/portal_heatmap.meta.js
// @downloadURL     https://gitlab.com/Machap/iitc-plugin/-/raw/master/portal_heatmap.user.js.
// @description     IITC plugin: Portal Heatmap
// @match           https://*.ingress.com/*
// @author          Machap
// ==/UserScript==
