// ==UserScript==
// @author         tyrrellp34
// @name           IITC plugin: Live Location
// @category       Layer
// @version        0.8.3.20241027.175524
// @description    [0.8.3.20241027.175524] Show Telegram live locations on the map
// @id             live-location
// @namespace      https://toolbox.hgen.cf/iitc/
// @updateURL      https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.meta.js
// @downloadURL    https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.user.js
// @match          https://intel.ingress.com/*
// @match          https://intel-x.ingress.com/*
// @match          https://intel.bluecf.mydns.jp/*
// @match          https://intel-x.bluecf.mydns.jp/*
// @grant          none
// ==/UserScript==
