// ==UserScript==
// @id             iitc-plugin-splitOrnaments-EpiphanyDawn
// @name           IITC plugin: Split Anomaly ornaments and beacons into their own layers for EpiphanyDawn
// @author         Machap
// @category       Layer
// @version        0.2.0.20221201.0000001
// @downloadURL    https://ingress.love/iitc-ja/nya/split_ornaments_epd.user.js
// @updateURL      https://ingress.love/iitc-ja/nya/split_ornaments_epd.meta.js
// @description    [2022-12-01] Split Anomaly ornaments and beacons into their own layers for EpiphanyDawn
// @match          *://intel.ingress.com/*
// @match          *://intel-x.ingress.com/*
// @match          *://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==
