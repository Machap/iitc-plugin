// ==UserScript==
// @id             iitc-plugin-ornamennt-portal-export-machap
// @name           IITC plugin: Ornament Portal Export
// @category       Info
// @version        1.3.0.20220331.000001
// @description    Export Ornament Portal List
// @include        https://*.ingress.com/*
// @include        http://*.ingress.com/*
// @updateURL      https://gitlab.com/Machap/iitc-plugin/-/raw/master/ornamennt-portal-export.meta.js
// @downloadURL    https://gitlab.com/Machap/iitc-plugin/-/raw/master/ornamennt-portal-export.user.js
// @match          https://*.ingress.com/*
// @match          http://*.ingress.com/*
// @grant          none
// ==/UserScript==
