
# 概要

- Telegramで共有された位置情報をマーカ・リストとして表示します。

# 準備

- IITCのプラグインです。IITCについては
  [CE版IITC(iitc.app)](https://iitc.app)、
  [元祖IITC(iitc.me)](https://iitc.me) などを参照してください。
- 本プラグインを下記URLからIITCに登録してください。<br>
  [https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.user.js](https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.user.js)

# 使い方／動作

## タグの設定

- 本プラグインを使用する際はタグを設定する必要があります。
- タグは表示対象とするチャットグループを指定するためのものです。
- ポータル詳細画面下の[LiveLoc SetTag]をクリックするか<br>
  位置情報リスト(後述)の下の[SetTag]をクリックするとタグを入力する画面が表示されます。
- タグはコンマ(,)を区切り文字として複数指定することができます。

## 位置情報マーカ表示

- 共有されている位置情報を元にマップ上にマーカが表示されます。
- マーカには位置情報を共有しているTelegramのユーザ名が表示されます。
- 役割(TL/STL/その他)によってマーカの色が変わります。<br>
  TL:赤／STL:オレンジ／その他:青
- 位置情報が最後に更新されてから3分経過するとマーカの色がグレーになります。
- マーカアイコンのボタンをクリックすると一時的に(5秒間)マーカが非表示になり<br>
  マーカに隠れたポータルを選択しやすくします。
- 位置情報が共有されているチャットグループ毎にレイヤーが作成されるので、<br>
  チャットグループ毎に表示／非表示を切り替えることができます。

## 位置情報リスト表示

- リストアイコンのボタンかポータル詳細画面下の[Location list]をクリックすると位置情報のリストが表示されます。
- スマホ・タブレットの場合はパネルリストの[Location list]を選択してもリストを表示できます。
- AG名をクリックするとAGが画面の中央に来るようにマップをスクロールします。
- リストは各列の見出しをクリックすることでソートできます。

## 設定メニュー

- 歯車マークアイコンのボタンをクリックすると下記を設定する画面が表示されます。
  * 各Roleの表示/非表示を切り替えるチェックボックス
  * Mobile modeの有効/無効を切り替えるチェックボックス
  * 接続するサーバを切り替えるラジオボタン
- Mobile modeを有効にすると、サーバとの通信量を制限します。

# 今後の予定(未定)

- リスト表示の内容をCSVファイルに保存
- シャード情報と連携して各AGに最寄りのシャード位置を通知？

# 変更履歴

## 0.8.3
 - FIX: v0.8.0-v0.8.1でAndroidで正常に動作しない不具合を修正
## 0.8.2
 - FIX: v0.8.0-v0.8.1がAndroidで正常に動作しないため一旦v0.7.6ベースにロールバック
## 0.8.1
 - FIX: 設定画面でOKクリック後すぐに設定が反映されない不具合を修正
## 0.8.0
 - ADD: 設定画面にサーバを切り替える項目を追加

## 0.7.6
 - CHG: マーカ非表示はもう一度ボタンを押すまで継続
## 0.7.5
 - FIX: 無関係のウインドウのタイトルを書き換えてしまう不具合を修正
## 0.7.4
 - CHG: サーバからバージョン番号を受け取ったらすぐに表示に反映
## 0.7.3
 - CHG: タグを変更したらすぐにタイトルバーのタグ文字列を更新
## 0.7.2
 - CHG: サーバ接続エラーはalertに出さず、位置情報リストのみに表示
## 0.7.1
 - FIX: 複数タグ指定時に1つのタグのレイヤーしか表示できない問題に対応
 - CHG: ロール未設定時はMEMBERと表示
## 0.7.0
 - ADD: ロール種別をTL/STL/BIKE/CAR/MEMBERに変更(追加)
 - FIX: ズーム時に一時的にマーカの位置がずれる問題を修正
 - CHG: マーカの移動アニメーションを微修整

## 0.6.1
 - CHG: OMSのパラメータを変更しないように変更
## 0.6.0
 - CHG: タグをコンマ(,)区切りで複数指定できるように変更

## 0.5.1
 - ADD: 位置情報リストにプラグインとサーバのバージョンを表示
## 0.5.0
 - ADD: タグ指定処理追加(復活)
 - DEL: パスワード関連処理削除
 - ADD: APIKEY対応
 - CHG: リストの色の変更タイミングを30秒から70秒に変更
 - CHG: レイヤー名の LiveLoc: を LL: に変更
 - FIX: 起動直後にRoleフィルタがマーカ表示に反映されない問題に対応

## 0.4.0
 - FIX: ユーザ名が空白の場合にリストが表示されない問題を修正
 - ADD: パスワードの前後に空白などが付いていたら削除する
 - CHG: マーカを手前に表示(z-indexを調整)
 - CHG: 難読化

## 0.3.4
 - FIX: Androidで起動しない問題に対応(再修正)
## 0.3.3
 - FIX: パスワード設定直後の接続に失敗する問題を修正
## 0.3.2
 - FIX: Androidで起動しない問題に対応(再修正)
## 0.3.1
 - FIX: Androidで起動しない問題に対応(setup()前のisSmartphone()呼び出し)
 - CHG: マーカの色を更新
 - CHG: タグの処理を一旦隠す
## 0.3.0
 - ADD: フィルタなどを指定するチェックボックスメニューを追加

## 0.2.4
 - ADD: マーカアイコンのボタンで一時的に(5秒間)マーカを非表示に
 - CHG: リストアイコンのボタンで位置情報リストを表示するように変更
## 0.2.3
 - FIX: リストのフィルタが正しく表示されない不具合を修正
 - FIX: パネル表示が正しく表示されない不具合を修正
 - CHG: TL、STL以外のメンバのマーカの色を青から紫に変更
## 0.2.2
 - ADD: リスト表示をスマホのパネル表示に対応
## 0.2.1
 - ADD: タグを設定する機能を追加
 - ADD: リストのAG名をクリックするとAGをマップの中心に表示
 - ADD: ツールバーボタンでリスト表示
 - ADD: リストにパスワード・タグ設定のボタンを追加
## 0.2.0
 - CHG: マップ表示範囲外の位置情報もリストに載せる
 - CHG: マーカをクリックした時にリストは表示しない
 - FIX: 役割が変更されたらすぐマーカの色を更新

## 0.1.2
 - CHG: マーカの移動をアニメーション表示
## 0.1.1
 - ADD: パスワード入力画面に入力済みパスワードを表示
 - ADD: プラグインヘッダのバージョンにタイムスタンプを追加
 - CHG: BOTとの個別チャットで共有した位置情報は表示しない
 - CHG: レイヤー選択メニューの並び順をソート
## 0.1.0
 - 公開版

## 0.0.x
 - お試し版

# 作者

tyrrellp34
