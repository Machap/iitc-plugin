// ==UserScript==
// @id             iitc-plugin-splitOrnaments-EpiphanyDawn
// @name           IITC plugin: Split Anomaly ornaments and beacons into their own layers for EpiphanyDawn
// @author         Machap
// @category       Layer
// @version        0.2.0.20221201.0000001
// @downloadURL    https://ingress.love/iitc-ja/nya/split_ornaments_epd.user.js
// @updateURL      https://ingress.love/iitc-ja/nya/split_ornaments_epd.meta.js
// @description    [2022-12-01] Split Anomaly ornaments and beacons into their own layers for EpiphanyDawn
// @match          *://intel.ingress.com/*
// @match          *://intel-x.ingress.com/*
// @match          *://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {

    // Make sure that window.plugin exists. IITC defines it as a no-op function,
    // and other plugins assume the same.
    if (typeof window.plugin !== "function") window.plugin = function () { };

    window.plugin.ornamentLayers = function () { };

    const thisPlugin = window.plugin.ornamentLayers
    // Name of the IITC build for first-party plugins
    plugin_info.buildName = "ornamentLayers-epd";

    // Datetime-derived version of the plugin
    plugin_info.dateTimeVersion = "202212010000001";

    // ID/name of the plugin
    plugin_info.pluginId = "ornamentLayers-epd";


    function setup() {
        var layerGroup = L.layerGroup;
        if (window.map.options.preferCanvas && L.Browser.canvas && !window.DISABLE_CANVASICONLAYER) {
            layerGroup = L.canvasIconLayer;
            L.CanvasIconLayer.mergeOptions({ padding: L.Canvas.prototype.options.padding });
        }
        window.ornaments._anomalyportals = layerGroup();
        window.ornaments._anomaly_volatile_portals = layerGroup();
        window.ornaments._battlebeacons = layerGroup();
        window.ornaments._battleresults = layerGroup();
        window.ornaments._scoutcontroller = layerGroup();
        if (!window.ornaments._layer) {
            window.ornaments._layer = window.ornaments.layers['Ornaments'];
        }
        if (!window.ornaments._beacons) {
            window.ornaments._beacons = layerGroup();
            window.addLayerGroup('Beacons', window.ornaments._beacons, true);
        }
        if (!window.ornaments._frackers) {
            window.ornaments._frackers = layerGroup();
            window.addLayerGroup('Frackers', window.ornaments._frackers, true);
        }
        window.addLayerGroup('Ornmt: Anomaly Portals', window.ornaments._anomalyportals, true);
        window.addLayerGroup('Ornmt: Volatile', window.ornaments._anomaly_volatile_portals, true);
        window.addLayerGroup('Ornmt: Battle Beacons', window.ornaments._battlebeacons, true);
        window.addLayerGroup('Ornmt: Battle Results', window.ornaments._battleresults, true);
        window.addLayerGroup('Ornmt: Scout Controller', window.ornaments._scoutcontroller, true);

        window.ornaments.addPortal = function (portal) {
            this.removePortal(portal);

            var ornaments = portal.options.data.ornaments;
            if (ornaments && ornaments.length) {
                this._portals[portal.options.guid] = ornaments.map(function (ornament) {
                    var layer = this._layer;
                    var opacity = this.OVERLAY_OPACITY;
                    var size = this.OVERLAY_SIZE * window.portalMarkerScale();
                    var anchor = [size / 2, size / 2];
                    var iconUrl = '//commondatastorage.googleapis.com/ingress.com/img/map_icons/marker_images/' + ornament + '.png';

                    if (this.knownOrnaments && !this.knownOrnaments[ornament]) {
                        this.knownOrnaments[ornament] = false;
                    }

                    if (this.icon && ornament in this.icon) {
                        if (this.icon[ornament].url) {
                            iconUrl = this.icon[ornament].url;
                            if (this.icon[ornament].offset) {
                                var offset = this.icon[ornament].offset;
                                anchor = [size * offset[0] + anchor[0], size * offset[1] + anchor[1]];
                            }
                            if (this.icon[ornament].opacity) {
                                opacity = this.icon[ornament].opacity;
                            }
                        }
                    }

                    var exclude = false;
                    if (this.excludedOrnaments && this.excludedOrnaments !== ['']) {
                        exclude = this.excludedOrnaments.some(function (pattern) {
                            return ornament.startsWith(pattern);
                        });
                    }
                    exclude = exclude || this.knownOrnaments && this.knownOrnaments[ornament];
                    if (exclude) {
                        layer = this.layers['Excluded ornaments'];
                    }

                    if (ornament.startsWith('pe')) {
                        layer = ornament === 'peFRACK'
                            ? this._frackers
                            : this._beacons;
                    }
                    if (ornament.startsWith("ap")) layer = window.ornaments._anomalyportals;
                    if (ornament.endsWith("_v")) layer = window.ornaments._anomaly_volatile_portals;
                    if (ornament.startsWith("sc")) layer = window.ornaments._scoutcontroller;
                    if (ornament.startsWith("bb_s")) layer = window.ornaments._scoutcontroller;
                    if (ornament.startsWith("peBB")) layer = window.ornaments._battlebeacons;
                    if (ornament.startsWith("peBR")) layer = window.ornaments._battleresults;

                    return L.marker(portal.getLatLng(), {
                        icon: L.icon({
                            iconUrl: iconUrl,
                            iconSize: [size, size],
                            iconAnchor: anchor, // https://github.com/IITC-CE/Leaflet.Canvas-Markers/issues/4
                            className: 'no-pointer-events'
                        }),
                        interactive: false,
                        keyboard: false,
                        opacity: opacity,
                        layer: layer
                    }).addTo(layer);
                }, this);
            }
        };
    };

    function delaySetup() {
        setTimeout(setup, 1000); // delay setup and thus requesting data, or we might encounter a server error
    }
    delaySetup.info = plugin_info; //add the script info data to the function as a property

    if (window.iitcLoaded) {
        delaySetup();
    } else {
        if (!window.bootPlugins) {
            window.bootPlugins = [];
        }
        window.bootPlugins.push(delaySetup);
    }
};


(function () {
    const plugin_info = {};
    if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
        plugin_info.script = {
            version: GM_info.script.version,
            name: GM_info.script.name,
            description: GM_info.script.description
        };
    }
    // Greasemonkey. It will be quite hard to debug
    if (typeof unsafeWindow != 'undefined' || typeof GM_info == 'undefined' || GM_info.scriptHandler != 'Tampermonkey') {
        // inject code into site context
        const script = document.createElement('script');
        script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(plugin_info) + ');'));
        (document.body || document.head || document.documentElement).appendChild(script);
    } else {
        // Tampermonkey, run code directly
        wrapper(plugin_info);
    }
})();

