    // ==UserScript==
    // @id             iitc-plugin-CustomGrid   
    // @name           IITC plugin: Custrom Grid Layer
    // @author         Machap
    // @category       Layer
    // @version        0.1.0.20190512.0000000
    // @downloadURL    https://ingress.love/iitc-ja/nya/custom_grid.user.js
    // @updateURL      https://ingress.love/iitc-ja/nya/custom_grid.meta.js
    // @description    [2019-05-12] Split Anomaly ornaments and beacons into their own layers for Abbadon Prime
    // @include        https://ingress.com/intel*
    // @include        http://ingress.com/intel*
    // @include        https://*.ingress.com/intel*
    // @include        http://*.ingress.com/intel*
    // @match          https://*.ingress.com/intel*
    // @match          https://intel.ingress.com/*
    // @include        https://intel.ingress.com/*
    // @match          http://*.ingress.com/intel*
    // @grant          none
    // ==/UserScript==

    function wrapper(plugin_info) {
        if (typeof window.plugin !== 'function') window.plugin = function() {};


        // PLUGIN START ////////////////////////////////////////////////////////
        window.plugin.customgird_layer = function() {};
        window.plugin.customgird_layer.start_lng = 139.700858;
        window.plugin.customgird_layer.start_lat = 35.68869;

        //

        //
        var STORAGE_KEY = 'plugin_custom_grid';

        //ローカルストレージからデータ読み出し
        window.plugin.customgird_layer.loadSettings = function() {
            if (!localStorage[STORAGE_KEY]) {
                window.plugin.customgird_layer.settings.latlngkey = "";
                window.plugin.customgird_layer.saveSettings();
            } else {
                window.plugin.customgird_layer.settings = JSON.parse(localStorage[STORAGE_KEY]);
            }
        };

        //ローカルストレージ保存
        window.plugin.customgird_layer.saveSettings = function() {
            localStorage[STORAGE_KEY] = JSON.stringify(window.plugin.customgird_layer.settings);
        };

        //ダイアログの表示
        window.plugin.customgird_layer.openDialog = function() {
            var contents = [
                '<table>',
                '<tr>',
                '<td>',
                '</td></tr>',

                '</table>'
            ].join('');

            var dialogBox = '<div id="customgrid-settings">' + contents + '</div>';


            dialog({
                html: dialogBox,
                title: 'Custom Grid Settings',
                closeCallback: closeCallback
            });
        }

        window.plugin.customgird_layer.drawgrid= function() {
            window.plugin.customgird_layer.gridLayerGroup1.clearLayers();
            window.plugin.customgird_layer.gridLayerGroup2.clearLayers();
            window.plugin.customgird_layer.gridLayerGroup3.clearLayers();
            window.plugin.customgird_layer.gridLayerGroup4.clearLayers();
            var polyLines, polyLines1, polyLines2, polyLines3, polyLines4 = [];
            var lines = [];
            var lines1 = [];
            var lines2 = [];
            var lines3 = [];
            var lines4 = [];
            start_lng = window.plugin.customgird_layer.start_lng;
            start_lat = window.plugin.customgird_layer.start_lat;
            var distance = 8000;
            var dis1 = 500;
            var dis2 = dis1 / 2;
            var dis3 = dis2 / 2;
            var dis4 = dis3 / 2;
            var dis_lat, dis_lng;

            start_lat = start_lat - window.plugin.customgird_layer.meter2lat() * distance;
            var end_lat = start_lat + window.plugin.customgird_layer.meter2lat() * distance;
            var end_lng = start_lng + window.plugin.customgird_layer.meter2lng(start_lat) * distance;

            lines.push([
                [start_lat, start_lng],
                [start_lat, end_lng]
            ], [
                [start_lat, start_lng],
                [end_lat, start_lng]
            ], [
                [end_lat, start_lng],
                [end_lat, end_lng]
            ], [
                [start_lat, end_lng],
                [end_lat, end_lng]
            ]);
            for (var i = dis4; distance > i; i += dis4) {
                dis_lat = start_lat + window.plugin.customgird_layer.meter2lat() * i;
                dis_lng = start_lng + window.plugin.customgird_layer.meter2lng(start_lat) * i;

                if (i % dis1) {
                    if (i % dis2) {
                        if (i % dis3) {
                            lines1.push([
                                [dis_lat, start_lng],
                                [dis_lat, end_lng]
                            ], [
                                [start_lat, dis_lng],
                                [end_lat, dis_lng]
                            ]);
                        } else {
                            lines2.push([
                                [dis_lat, start_lng],
                                [dis_lat, end_lng]
                            ], [
                                [start_lat, dis_lng],
                                [end_lat, dis_lng]
                            ]);

                        }
                    } else {
                        lines3.push([
                            [dis_lat, start_lng],
                            [dis_lat, end_lng]
                        ], [
                            [start_lat, dis_lng],
                            [end_lat, dis_lng]
                        ]);
                    }
                } else {
                    lines4.push([
                        [dis_lat, start_lng],
                        [dis_lat, end_lng]
                    ], [
                        [start_lat, dis_lng],
                        [end_lat, dis_lng]
                    ]);
                }
            }
            polyLines = window.L.multiPolyline(lines, { color: 'black', weight: 2.0, opacity: 1.0 });
            polyLines1 = window.L.multiPolyline(lines1, { color: 'black', weight: 1, opacity: 0.4 });
            polyLines2 = window.L.multiPolyline(lines2, { color: 'purple', weight: 1.5, opacity: 0.8 });
            polyLines3 = window.L.multiPolyline(lines3, { color: 'blue', weight: 1.5, opacity: 0.8 });
            polyLines4 = window.L.multiPolyline(lines4, { color: 'red', weight: 1.5, opacity: 1.0 });
            polyLines.addTo(window.plugin.customgird_layer.gridLayerGroup4);
            polyLines1.addTo(window.plugin.customgird_layer.gridLayerGroup1);
            polyLines2.addTo(window.plugin.customgird_layer.gridLayerGroup2);
            polyLines3.addTo(window.plugin.customgird_layer.gridLayerGroup3);
            polyLines4.addTo(window.plugin.customgird_layer.gridLayerGroup4);
            console.log("draw test data");

            var toInte = Math.round(dis_lng * 1000000);
            var stInte = toInte.toString(36);
            console.log("toInte:" + toInte + " stInte:" + stInte);

        }
        window.plugin.customgird_layer.meter2lng = function(lat) {
            return 360 / (2 * Math.PI * (6378137.000) * Math.cos(lat * Math.PI / 180.0));
        }
        window.plugin.customgird_layer.meter2lat = function() {
            return 360 / (2 * Math.PI * 6356752.314140356);
        }
        window.plugin.customgird_layer.selectpoint = function(ev){
            var latlng = ev.latlng;
            var fields = window.fields;
            console.log("clickerd:"+latlng + "type"+typeof(latlng));
            window.plugin.customgird_layer.start_lng = latlng.lng;
            window.plugin.customgird_layer.start_lat = latlng.lat;
            window.plugin.customgird_layer.drawgrid();
        }
        window.plugin.customgird_layer.setpoint = function(ev) {
            var btn = plugin.customgird_layer.button,
            tooltip = plugin.customgird_layer.tooltip,
            layer = plugin.customgird_layer.layer;

            if(btn.classList.contains("active")) {
                console.log("active");
                if(window.plugin.drawTools !== undefined) {
                    window.plugin.drawTools.drawnItems.eachLayer(function(layer) {
                        if (layer instanceof L.GeodesicPolygon) {
                            L.DomUtil.addClass(layer._path, "leaflet-clickable");
                            layer._path.setAttribute("pointer-events", layer.options.pointerEventsBackup);
                            layer.options.pointerEvents = layer.options.pointerEventsBackup;
                            layer.options.clickable = true;
                        }
                    });
                }
                map.off("click",  window.plugin.customgird_layer.selectpoint);
                btn.classList.remove("active");
            } else {
                console.log("inactive");
                if(window.plugin.drawTools !== undefined) {
                    window.plugin.drawTools.drawnItems.eachLayer(function(layer) {
                        if (layer instanceof L.GeodesicPolygon) {
                            layer.options.pointerEventsBackup = layer.options.pointerEvents;
                            layer.options.pointerEvents = null;
                            layer.options.clickable = false;
                            L.DomUtil.removeClass(layer._path, "leaflet-clickable");
                            layer._path.setAttribute("pointer-events", "none");
                        }
                    });
                }
                map.on("click",  window.plugin.customgird_layer.selectpoint);
                btn.classList.add("active");
                // setTimeout(function(){
                //     tooltip.textContent = "Click on map";
                // }, 10);
            } 
        }
        // 初期設定\
        window.plugin.customgird_layer.setup = function() {


        $('<style>').prop('type', 'text/css').html('.leaflet-control-custom-gridmap a\n{\n	background-image: url("data:image/svg+xml;base64,PHN2ZyBpZD0iYjE0YjcwNWEtNjc0My00MDhiLWIxNGYtZjMwODFlYmI0OWY4IiBkYXRhLW5hbWU9IuODrOOCpOODpOODvCAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNy42OSAxOC41MSI+PGRlZnM+PHN0eWxlPi5hMWVkNTA3Ny02YzYwLTRlNDMtOTlkYi1lZjk1MzgzZTcxZjgsLmIxMjdjMjViLTdhM2EtNDM2Yi1iMmMxLTJiNjQ5ZjAxZjgwYywuYjM3OWZlMjYtNzRmYi00ZTkxLTkyZGMtMTc2Mjk5M2E2YWJjLC5lN2MxMzYwYS1mNWNhLTRmODEtYTVlYS03ZGE2YmQ2MmQ3NTJ7ZmlsbDpub25lO3N0cm9rZS1taXRlcmxpbWl0OjEwO30uYjM3OWZlMjYtNzRmYi00ZTkxLTkyZGMtMTc2Mjk5M2E2YWJje3N0cm9rZTojYTBhMGE0O30uZTdjMTM2MGEtZjVjYS00ZjgxLWE1ZWEtN2RhNmJkNjJkNzUye3N0cm9rZTpncmF5O30uYTFlZDUwNzctNmM2MC00ZTQzLTk5ZGItZWY5NTM4M2U3MWY4e3N0cm9rZTojNTk1OTU5O30uYjEyN2MyNWItN2EzYS00MzZiLWIyYzEtMmI2NDlmMDFmODBje3N0cm9rZTojMDAwO308L3N0eWxlPjwvZGVmcz48dGl0bGU+Z2lyZHRvcDwvdGl0bGU+PHJlY3QgY2xhc3M9ImIzNzlmZTI2LTc0ZmItNGU5MS05MmRjLTE3NjI5OTNhNmFiYyIgeD0iMi43MyIgeT0iMTQuMTUiIHdpZHRoPSI0LjgyIiBoZWlnaHQ9IjMuODYiLz48cmVjdCBjbGFzcz0iYjM3OWZlMjYtNzRmYi00ZTkxLTkyZGMtMTc2Mjk5M2E2YWJjIiB4PSI3LjU1IiB5PSIxNC4xNSIgd2lkdGg9IjQuODIiIGhlaWdodD0iMy44NiIvPjxyZWN0IGNsYXNzPSJiMzc5ZmUyNi03NGZiLTRlOTEtOTJkYy0xNzYyOTkzYTZhYmMiIHg9IjEyLjM3IiB5PSIxNC4xNSIgd2lkdGg9IjQuODIiIGhlaWdodD0iMy44NiIvPjxyZWN0IGNsYXNzPSJlN2MxMzYwYS1mNWNhLTRmODEtYTVlYS03ZGE2YmQ2MmQ3NTIiIHg9IjIuNzMiIHk9IjEwLjI5IiB3aWR0aD0iNC44MiIgaGVpZ2h0PSIzLjg2Ii8+PHJlY3QgY2xhc3M9ImU3YzEzNjBhLWY1Y2EtNGY4MS1hNWVhLTdkYTZiZDYyZDc1MiIgeD0iNy41NSIgeT0iMTAuMjkiIHdpZHRoPSI0LjgyIiBoZWlnaHQ9IjMuODYiLz48cmVjdCBjbGFzcz0iZTdjMTM2MGEtZjVjYS00ZjgxLWE1ZWEtN2RhNmJkNjJkNzUyIiB4PSIxMi4zNyIgeT0iMTAuMjkiIHdpZHRoPSI0LjgyIiBoZWlnaHQ9IjMuODYiLz48cmVjdCBjbGFzcz0iYTFlZDUwNzctNmM2MC00ZTQzLTk5ZGItZWY5NTM4M2U3MWY4IiB4PSIyLjczIiB5PSI2LjQ0IiB3aWR0aD0iNC44MiIgaGVpZ2h0PSIzLjg2Ii8+PHJlY3QgY2xhc3M9ImExZWQ1MDc3LTZjNjAtNGU0My05OWRiLWVmOTUzODNlNzFmOCIgeD0iNy41NSIgeT0iNi40NCIgd2lkdGg9IjQuODIiIGhlaWdodD0iMy44NiIvPjxyZWN0IGNsYXNzPSJhMWVkNTA3Ny02YzYwLTRlNDMtOTlkYi1lZjk1MzgzZTcxZjgiIHg9IjEyLjM3IiB5PSI2LjQ0IiB3aWR0aD0iNC44MiIgaGVpZ2h0PSIzLjg2Ii8+PHJlY3QgY2xhc3M9ImIxMjdjMjViLTdhM2EtNDM2Yi1iMmMxLTJiNjQ5ZjAxZjgwYyIgeD0iMi43MyIgeT0iMi41OCIgd2lkdGg9IjQuODIiIGhlaWdodD0iMy44NiIvPjxyZWN0IGNsYXNzPSJiMTI3YzI1Yi03YTNhLTQzNmItYjJjMS0yYjY0OWYwMWY4MGMiIHg9IjcuNTUiIHk9IjIuNTgiIHdpZHRoPSI0LjgyIiBoZWlnaHQ9IjMuODYiLz48cmVjdCBjbGFzcz0iYjEyN2MyNWItN2EzYS00MzZiLWIyYzEtMmI2NDlmMDFmODBjIiB4PSIxMi4zNyIgeT0iMi41OCIgd2lkdGg9IjQuODIiIGhlaWdodD0iMy44NiIvPjxjaXJjbGUgY2xhc3M9ImIxMjdjMjViLTdhM2EtNDM2Yi1iMmMxLTJiNjQ5ZjAxZjgwYyIgY3g9IjIuNTgiIGN5PSIyLjU4IiByPSIyLjA4Ii8+PC9zdmc+");\n}\n.leaflet-control-custom-gridmap a.active\n{\n	background-color: #BBB;\n}\n.leaflet-control-custom-gridmap-tooltip\n{\n	background-color: rgba(255, 255, 255, 0.6);\n	display: none;\n	height: 24px;\n	left: 30px;\n	line-height: 24px;\n	margin-left: 15px;\n	margin-top: -12px;\n	padding: 0 10px;\n	position: absolute;\n	top: 50%;\n	white-space: nowrap;\n	width: auto;\n}\n.leaflet-control-custom-gridmap a.active .leaflet-control-custom-gridmap-tooltip\n{\n	display: block;\n}\n.leaflet-control-custom-gridmap-tooltip:before\n{\n	border-color: transparent rgba(255, 255, 255, 0.6);\n	border-style: solid;\n	border-width: 12px 12px 12px 0;\n	content: "";\n	display: block;\n	height: 0;\n	left: -12px;\n	position: absolute;\n	width: 0;\n}\n').appendTo('head');

        var parent = $(".leaflet-top.leaflet-left", window.map.getContainer());

        var button1 = document.createElement("a");
        button1.className = "leaflet-bar-part";
        button1.id = "button-options";
        button1.addEventListener("click", function(e) {
            console.debug('options click: e=', e);
            window.plugin.customgird_layer.setpoint(e); e.preventDefault(); e.stopPropagation();}, false);
        button1.title = 'Select Grid Start Point..';
        // setpoint.options_button = button1;

        var container = document.createElement("div");
        container.className = "leaflet-control-custom-gridmap leaflet-bar leaflet-control";
        container.appendChild(button1);
        parent.append(container);
        plugin.customgird_layer.button = button1;
            window.plugin.customgird_layer.loadSettings();
            //設定ファイアログのバインド
            // var openDialog = '<a onclick="window.plugin.customgird_layer.openDialog();return false;">Ornament Split opt</a>';
            // $('#toolbox').append(openDialog);

            // レイヤー追加
            window.plugin.customgird_layer.gridLayerGroup1 = new L.LayerGroup();
            window.plugin.customgird_layer.gridLayerGroup2 = new L.LayerGroup();
            window.plugin.customgird_layer.gridLayerGroup3 = new L.LayerGroup();
            window.plugin.customgird_layer.gridLayerGroup4 = new L.LayerGroup();
            window.addLayerGroup('Custom Huge Grid', window.plugin.customgird_layer.gridLayerGroup4, true);
            window.addLayerGroup('Custom Large Grid', window.plugin.customgird_layer.gridLayerGroup3, true);
            window.addLayerGroup('Custom Middle Grid', window.plugin.customgird_layer.gridLayerGroup2, true);
            window.addLayerGroup('Custom Small Grid', window.plugin.customgird_layer.gridLayerGroup1, true);

        };

        window.addHook('iitcLoaded', function() {
            window.plugin.customgird_layer.drawgrid();
        });

        window.plugin.customgird_layer.settings = {};
        var setup = window.plugin.customgird_layer.setup;
        setup.info = plugin_info; //add the script info data to the function as a property
        if (!window.bootPlugins) window.bootPlugins = [];
        window.bootPlugins.push(setup);
        // if IITC has already booted, immediately run the 'setup' function
        if (window.iitcLoaded && typeof setup === 'function') {
            setup();
        }

        // PLUGIN END ////////////////////////////////////////////////////////
    } // WRAPPER END ////////////////////////////////////////////////////////

    var script = document.createElement('script');
    var info = {};
    if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
    script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
    (document.body || document.head || document.documentElement).appendChild(script);