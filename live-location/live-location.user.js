// ==UserScript==
// @author         tyrrellp34
// @name           IITC plugin: Live Location
// @category       Layer
// @version        0.8.3
// @description    [0.8.3] Show Telegram live locations on the map
// @id             live-location
// @namespace      https://toolbox.hgen.cf/iitc/
// @updateURL      https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.meta.js
// @downloadURL    https://gitlab.com/Machap/iitc-plugin/-/raw/master/live-location.user.js
// @match          https://intel.ingress.com/*
// @match          https://intel-x.ingress.com/*
// @match          https://intel.bluecf.mydns.jp/*
// @match          https://intel-x.bluecf.mydns.jp/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
  // PLUGIN START ////////////////////////////////////////////////////////

  // ensure plugin framework is there, even if iitc is not yet loaded
  if (typeof window.plugin !== 'function') window.plugin = function () { };

  window.plugin.liveLocation = function () { };
  const self = window.plugin.liveLocation;
  self.pluginVersion = '0.8.3';
  self.serverVersion = '';

  //// Manage location data ///////////////////////////////////////////////////

  self.setupSocket = function () {
    // inject socket.io.js
    script = document.createElement('script');
    script.src = `https://${self.options.llServer}/socket.io/socket.io.min.js`;
    (document.body || document.head || document.documentElement).appendChild(script);
  }

  self.connectSocket = function () {
    self.error = '';
    if (self.socket) {
      try {
        self.socket.close();
      } catch (err) {
        console.warn(err);
      }
      self.socket = null;
    }
    if (typeof io === 'undefined') {
      console.error('socket.io not ready!');
      // alert('サーバに接続できません / Server error (E2)');
      self.error = 'E2';
      return;
    }
    try {
      const socket = io(`https://${self.options.llServer}/livelocation`, { reconnectionDelayMax: 1800000 });
      if (!socket) {
        console.error('socket.io connect failed!');
        // alert('サーバに接続できません / Server error (E4)');
        self.error = 'E4';
        return;
      }
      self.socket = socket;
      socket.on('connect', function () {
        let options = {
          apikey: self.APIKEY,
          saverMode: self.options.saverMode ? "on" : "off",
        };
        if (self.apitag) {
          options.tags = self.apitag.split(',').filter(x => !!x);
        } else {
          options.tags = [];
        }
        console.info('connect:', options);
        socket.emit('subscribe', options);
      });
      socket.on('chat', function (chat) {
        console.info('chat:', chat);
        self.chatUpdated(chat);
      });
      socket.on('user', function (user) {
        console.info('user:', user);
        self.userUpdated(user);
      });
      socket.on('location', function (data) {
        if (!window.isSmartphone()) console.info('location:', data);
        self.locationUpdated(data);
      });
      socket.on('updates', function (data) {
        console.info('updates:', data);
        self.updatesReceived(data);
      });
    } catch (err) {
      console.error('socket.io open failed:', err);
      // alert('サーバに接続できません / Server error (E3)');
      self.error = 'E3';
      return;
    }
  }

  self.updatesReceived = function (updates) {
    if (updates.version) {
      self.serverVersion = updates.version;
    }
    if (updates.chats) {
      updates.chats
        .sort((a, b) => self.chatTitle(a).toLowerCase() > self.chatTitle(b).toLowerCase() ? 1 : -1)
        .forEach(chat => self.chatUpdated(chat));
      self.setupLayerMenu();
    }
    if (updates.users) {
      updates.users.forEach(user => self.userUpdated(user));
    }
    if (updates.locations) {
      updates.locations.forEach(location => self.locationUpdated(location));
    }
  }

  self.chats = {};
  self.chatUpdated = function (data) {
    if (!data.chatid) return;
    if (!data.title) return;  // 個別チャットを除外
    let chat = self.chats[data.chatid + ''];
    if (chat) {
      if (data.title && data.title != chat.title) {
        chat.title = data.title;
        window.removeLayerGroup(chat.layer);
        window.addLayerGroup('LL: ' + self.chatTitle(chat), chat.layer, true);
        self.filterChat[chat.chatid] = !map.hasLayer(chat.layer);
      }
    } else {
      const layer = L.layerGroup();
      chat = self.chats[data.chatid + ''] = {
        chatid: data.chatid,
        title: data.title,
        layer: layer,
      };
      window.addLayerGroup('LL: ' + self.chatTitle(chat), chat.layer, true);
    }
    return chat;
  };

  self.chatTitle = function (chat) {
    return chat ? (chat.title || '[' + chat.chatid + ']') : '-';
  };

  self.setupLayerMenu = function () {
    Object.values(self.chats)
      .filter(chat => !!chat.title)  // 個別チャットを除外
      .sort((a, b) => {
        if (a.title < b.title) return -1;
        if (a.title > b.title) return 1;
        return 0;
      })
      .forEach(chat => {
        window.removeLayerGroup(chat.layer);
        window.addLayerGroup('LL: ' + self.chatTitle(chat), chat.layer, true);
        self.filterChat[chat.chatid] = !map.hasLayer(chat.layer);
      });
  }

  self.users = {};
  self.userUpdated = function (data) {
    if (!data.id) return;
    let user = self.users[data.id + ''];
    if (user) {
      if (data.name && data.name != user.name) {
        user.name = data.name;
      }
      if (data.fullname && data.fullname != user.fullname) {
        user.fullname = data.fullname;
      }
      if (typeof data.role !== 'undefined' && data.role != user.role) {
        user.role = data.role;
      }
      if (data.iconUrl && data.iconUrl != user.iconUrl) {
        user.iconUrl = data.iconUrl;
      }
    } else {
      user = self.users[data.id + ''] = {
        id: data.id,
        name: data.name,
        fullname: data.fullname,
        role: data.role,
        iconUrl: data.iconUrl,
      }
    }
    return user;
  };

  self.userName = function (user) {
    return user ? (user.name || '[' + user.id + ']') : '-';
  }

  self.userFullName = function (user) {
    return user ? user.fullname : '-';
  }

  self.locations = {};
  self.locationUpdated = function (data) {
    if (data.event == 'user' && data.message) {
      self.userUpdated(data.message)
      return;
    }

    self.userUpdated({ id: data.id, name: data.name, fullname: data.fullname });
    self.chatUpdated({ chatid: data.chatid, title: data.title });
    let location = self.locations[data.id + ':' + data.chatid];
    if (location) {
      location.chatid = data.chatid;
      location.timestamp = new Date(data.timestamp).getTime();
      location.latLng.lat = data.latitude;
      location.latLng.lng = data.longitude;
    } else {
      location = {
        id: data.id,
        chatid: data.chatid,
        timestamp: new Date(data.timestamp).getTime(),
        latLng: L.latLng(data.latitude, data.longitude),
      };
      self.locations[data.id + ':' + data.chatid] = location;
    }
    self.updateMarker(location);
    return location;
  }

  //// Manage location markers ////////////////////////////////////////////////

  self.updateMarker = function (location) {
    const icon = location.icon = self.createIcon(location);
    let marker = location.marker;
    if (marker) {
      if (icon) {
        marker.setIcon(icon);
      }
      if (location.prevLat != location.latLng.lat || location.prevLng != location.latLng.lng) {
        if (marker._icon) {
          const pos = map.latLngToLayerPoint(location.latLng);
          marker._icon.style.transition = 'transform 2s ease';
          marker._icon.style.transform = `translate(${pos.x}px, ${pos.y}px)`;
          window.clearTimeout(location.timer);
          location.timer = window.setTimeout(() => {
            marker._icon.style.transition = '';
            marker._icon.style.transform = '';
            location.marker.setLatLng(location.latLng);
          }, 2000);
        } else {
          location.marker.setLatLng(location.latLng);
        }
        location.prevLat = location.latLng.lat;
        location.prevLng = location.latLng.lng;
      }
    } else {
      location.marker = self.createMarker(location);
      location.prevLat = location.latLng.lat;
      location.prevLng = location.latLng.lng;
    }
  }

  self.createIcon = function (location) {
    const user = self.users[location.id + ''];
    let iconUrl = self.iconOther;
    const recentness = self.recentness(location.timestamp);
    if (recentness < 2) {
      if (user.role == 'TL') {
        iconUrl = self.iconLeader;
      } else if (user.role == 'STL') {
        iconUrl = self.iconSubLeader;
      } else if (user.role == 'BIKE') {
        iconUrl = self.iconBike;
      } else if (user.role == 'CAR') {
        iconUrl = self.iconCar;
      } else {
        iconUrl = self.iconMember;
      }
    }
    if (iconUrl == location.prevIcon) return null;
    location.prevIcon = iconUrl;

    let iconAnchor = [42, 41];
    let iconSize = [80, 41];
    let iconClass = '';
    if (user.iconUrl) {
      iconUrl = user.iconUrl;
      iconAnchor = [30, 60];
      iconSize = [60, 60];
      iconClass = 'photo';
    }
    const name = user && user.name || '';
    const icon = L.divIcon({
      className: 'plugin-live-location-marker-label',
      iconAnchor: iconAnchor,
      iconSize: iconSize,
      html: '<img class="iconimg ' + iconClass + '" src="' + iconUrl + '" />' +
        '<div class="agname">' + name + '</div>',
    });
    return icon;
  }

  self.createMarker = function (location) {
    const user = self.users[location.id + ''];
    const role = self.roles.find(role => role.match(role, user.role));
    const fullname = user && (user.fullname || user.name) || '-';
    const marker = L.marker(location.latLng, {
      // title: fullname,
      icon: location.icon,
      riseOnHover: true,
      zIndexOffset: role ? role.zIndexOffset : 1000,
    });
    const isFiltered = self.options[`filterRole_${role.id}`];
    if (isFiltered) {
      const chat = self.chats[location.chatid + ''];
      location.layer = chat ? chat.layer : map;
      location.layer.addLayer(marker);
    }
    window.registerMarkerForOMS(marker);
    return marker;
  }

  self.hideMarker = function (temporary = false) {
    if (self._button1.classList.contains('active')) {
      self._button1.classList.remove("active");
    } else {
      self._button1.classList.add("active");
      if (temporary) {
        window.clearTimeout(self.hideTimer);
        self.hideTimer = window.setTimeout(() => {
          self._button1.classList.remove("active");
          self.redrawAllMarkers();
        }, 5000);
      }
    }
    self.redrawAllMarkers();
  }

  self.updateAllMarkers = function () {
    Object.values(self.locations).forEach(location => self.updateMarker(location));
  }

  self.redrawMarker = function (location) {
    const isHidden = self._button1.classList.contains('active');
    const user = self.users[location.id + ''];
    const role = self.roles.find(role => role.match(role, user.role));
    const isFiltered = self.options[`filterRole_${role.id}`];
    if (!isHidden && isFiltered) {
      if (!location.layer) {
        const chat = self.chats[location.chatid + ''];
        location.layer = chat ? chat.layer : map;
        location.layer.addLayer(location.marker);
      }
    } else {
      if (location.layer) {
        location.layer.removeLayer(location.marker);
        location.layer = null;
      }
    }
  }

  self.redrawAllMarkers = function () {
    Object.values(self.locations).forEach(location => self.redrawMarker(location));
  }

  //// Manage location list ///////////////////////////////////////////////////

  self.listLocations = [];
  self.sortBy = 3;
  self.sortOrder = -1;
  self.reversed = false;
  self.counts = {};
  self.filter = '';

  self.fields = [
    {
      title: "Agent Name",
      value: function (location) {
        const user = self.users[location.id + ''];
        return user && user.name || location.name;
      },
      sortValue: function (value, location) { return value ? value.toLowerCase() : '-'; },
      format: function (cell, location, value) {
        $(cell)
          // .text(value)
          .append(self.agentLink(location, value))
          .addClass("agent");
      },
    },
    {
      title: "Role",
      value: function (location) {
        const user = self.users[location.id + ''];
        return user && user.role || 'MEMBER';
      },
      sortValue: function (value, location) {
        const user = self.users[location.id + ''];
        const role = self.roles.find(role => role.match(role, user.role));
        return role.sortValue;
      },
      format: function (cell, location, value) {
        $(cell)
          .text(value)
          .addClass("role");
      },
    },
    {
      title: "Chat Name",
      value: function (location) {
        const chat = self.chats[location.chatid + ''];
        return self.chatTitle(chat);
      },
      format: function (cell, location, value) {
        $(cell)
          .text(value)
          .addClass("chat");
      },
    },
    {
      title: "Updated at",
      value: function (location) { return location.timestamp ? self.dateStr(location.timestamp) : '-'; },
      sortValue: function (value, location) { return location.timestamp; },
      format: function (cell, location, value) {
        $(cell)
          .text(value)
          .addClass("timestamp");
        const recent = self.recentness(location.timestamp);
        if (recent > 2) {
          $(cell).addClass('oldest')
        } else if (recent > 1) {
          $(cell).addClass('older')
        } else if (recent > 0) {
          $(cell).addClass('old')
        } else {
          $(cell).addClass('new')
        }
      },
      defaultOrder: -1,
    },
  ];

  self.roles = [
    {
      title: 'Leader',
      id: 'TL',
      match: function (role, roleName) { return roleName == role.id; },
      sortValue: 1,
      zIndexOffset: 1005,
    },
    {
      title: 'SubLeader',
      id: 'STL',
      match: function (role, roleName) { return roleName == role.id; },
      sortValue: 2,
      zIndexOffset: 1004,
    },
    {
      title: 'Bike',
      id: 'BIKE',
      match: function (role, roleName) { return roleName == role.id; },
      sortValue: 3,
      zIndexOffset: 1003,
    },
    {
      title: 'Car',
      id: 'CAR',
      match: function (role, roleName) { return roleName == role.id; },
      sortValue: 4,
      zIndexOffset: 1002,
    },
    {
      title: 'Member',
      id: 'MEMBER',
      match: function (role, roleName) { return !roleName || roleName == role.id; },
      sortValue: 5,
      zIndexOffset: 1001,
    },
  ];

  self.agentLink = function (location, name) {
    var latLng = location.latLng;
    var href = window.makePermalink(latLng);
    var link = document.createElement("a");
    link.textContent = name;
    link.href = href;
    link.addEventListener("click", function (ev) {
      if (!self.dialog) {
        window.show('map');
      }
      self.panTo(latLng);
      ev.preventDefault();
      return false;
    }, false);
    return link;
  };

  self.panTo = function (latLng) {
    let zoom = Math.max(map.getZoom(), 15);
    map.setView(latLng, zoom, { animate: true, duration: 1 });
  };

  self.getLocations = function () {
    var retval = false;
    self.listLocations = [];
    self.counts = {};
    Object.values(self.locations)
      //// 個別チャットを除外
      .filter(location => {
        const chat = self.chats[location.chatid + ''];
        if (chat && chat.title) return true;
        return false;
      })
      .forEach(location => {
        retval = true;

        const user = self.users[location.id + ''];
        const role = self.roles.find(role => role.match(role, user.role));
        if (role) {
          self.counts[role.id] = (self.counts[role.id] || 0) + 1;
        }

        const chat = self.chats[location.chatid + ''];
        self.counts[chat.chatid] = (self.counts[chat.chatid] || 0) + 1;

        var obj = { location: location, values: [], sortValues: [] };
        var row = document.createElement('tr');
        obj.row = row;

        var cell = row.insertCell(-1);
        cell.className = 'alignR';

        self.fields.forEach(function (field, i) {
          cell = row.insertCell(-1);
          var value = field.value(location);
          obj.values.push(value);
          obj.sortValues.push(field.sortValue ? field.sortValue(value, location) : value);
          if (field.format) {
            field.format(cell, location, value);
          } else {
            cell.textContent = value;
          }
        });
        self.listLocations.push(obj);
      });

    return retval;
  }

  self.showList = function (onPane = false) {
    if (self.dialog) {
      self.dialog.dialog('close');
      if (!onPane) return;
    }
    var list;
    if (self.error) {
      list = $('<table class="noLocations"><tr><td>サーバへの接続に失敗しました / Server error (' + self.error + ')</td></tr></table>');
    } else if (self.getLocations()) {
      list = self.locationTable(self.sortBy, self.sortOrder, self.filter, self.reversed);
    } else {
      list = $('<table class="noLocations"><tr><td>表示する情報がありません / Nothing to show</td></tr></table>');
    }
    if (onPane) {
      $('<div id="locationList" class="mobile">').append(list).appendTo(document.body);
    } else {
      self.dialog = window.dialog({
        html: $('<div id="locationList">').append(list),
        dialogClass: 'ui-dialog-locationList' + (window.isSmartphone() ? ' mobile' : ''),
        title: 'Live Location list' + (self.apitag ? ' [' + self.apitag + ']' : ''),
        id: 'location-list',
        width: 640,
        closeCallback: () => {
          self._button2.classList.remove("active");
          self.dialog = undefined;
        },
        buttons: [
          {
            text: 'SetTag',
            click: () => { self.setTag(); }
          },
          {
            text: 'Close',
            click: () => { self.dialog && self.dialog.dialog('close'); },
          },
        ],
      });
      let versionText = 'plugin ver. ' + self.pluginVersion;
      if (self.serverVersion) versionText += ' / server ver. ' + self.serverVersion;
      const versionLabel = $('<label id="liveloc-version-text">').text(versionText).addClass("ui-widget");
      $('.ui-dialog-locationList .ui-dialog-buttonpane .ui-dialog-buttonset').prepend(versionLabel);
      console.debug('buttonpane:', $('.ui-dialog-locationList.ui-dialog-buttonpane'));
      self._button2.classList.add("active");
    }
  }

  self.updateList = function () {
    const tagText = 'Live Location list' + (self.apitag ? ' [' + self.apitag + ']' : '');
    $('.ui-dialog-locationList .ui-dialog-titlebar span.ui-dialog-title').text(tagText);
    let versionText = 'plugin ver. ' + self.pluginVersion;
    if (self.serverVersion) versionText += ' / server ver. ' + self.serverVersion;
    $('.ui-widget#liveloc-version-text').text(versionText);
    if (self.getLocations()) {
      const e1 = document.getElementById('table-location');
      const scrollLeft = e1 && e1.scrollLeft || 0;
      const list = self.locationTable(self.sortBy, self.sortOrder, self.filter, self.reversed);
      $('#locationList').empty().append(list);
      const e2 = document.getElementById('table-location');
      if (e2) e2.scrollLeft = scrollLeft;
    } else {
      list = $('<table class="noLocations"><tr><td>表示する情報がありません / Nothing to show</td></tr></table>');
      $('#locationList').empty().append(list);
    }
  }

  self.locationTable = function (sortBy, sortOrder, filter, reversed) {
    self.sortBy = sortBy;
    self.sortOrder = sortOrder;
    self.filter = filter;
    self.reversed = reversed;

    var locations = self.listLocations;
    var sortField = self.fields[sortBy];

    locations.sort(function (a, b) {
      var valueA = a.sortValues[sortBy];
      var valueB = b.sortValues[sortBy];
      if (sortField.sort) {
        return sortOrder * sortField.sort(valueA, valueB, a.location, b.location);
      }
      return sortOrder * (valueA < valueB ? -1 : valueA > valueB ? 1 : 0);
    });

    locations = locations.filter(function (obj) {
      let isHidden = false;
      const user = self.users[obj.location.id + ''];
      const role = self.roles.find(role => role.match(role, user.role));
      if (role && !self.options[`filterRole_${role.id}`]) {
        isHidden = true;
      }
      const chat = self.chats[obj.location.chatid + ''];
      if (chat && self.filterChat[chat.chatid]) {
        isHidden = true;
      }
      return !isHidden;
    });

    var container = $('<div>');

    var tableDiv = document.createElement('div');
    tableDiv.className = 'table-container';
    tableDiv.id = 'table-location';
    container.append(tableDiv);

    var table = document.createElement('table');
    table.className = 'locations';
    tableDiv.appendChild(table);

    var thead = table.appendChild(document.createElement('thead'));
    var row = thead.insertRow(-1);

    var cell = row.appendChild(document.createElement('th'));
    cell.textContent = '#';

    self.fields.forEach(function (field, i) {
      cell = row.appendChild(document.createElement('th'));
      cell.textContent = field.title;
      if (field.sort !== null) {
        cell.classList.add("sortable");
        if (i === self.sortBy) {
          cell.classList.add("sorted");
        }
        $(cell).click(function () {
          var order;
          if (i === sortBy) {
            order = -sortOrder;
          } else {
            order = field.defaultOrder < 0 ? -1 : 1;
          }
          $('#locationList').empty().append(self.locationTable(i, order, filter, reversed));
        });
      }
    });

    locations.forEach(function (obj, i) {
      var row = obj.row
      if (row.parentNode) row.parentNode.removeChild(row);
      row.cells[0].textContent = i + 1;
      table.appendChild(row);
    });
    // container.append(`<div class="disclaimer">disclaimer</div>`);
    return container;
  }

  self.dateStr = function (time) {
    if (time == 0) return "---";
    const pad2 = n => n < 10 ? '0' + String(n) : String(n);
    const pad3 = n => n < 10 ? '00' + String(n) : n < 100 ? '0' + String(n) : String(n);
    const d = new Date(time);
    const now = Date.now();
    if (Math.abs(now - d.getTime()) > 6 * 30 * 24 * 60 * 60 * 1000) { // 6か月以上前
      return pad2(d.getMonth() + 1) + "/" + pad2(d.getDate()) + " " + d.getFullYear();
    }
    if (Math.abs(now - d.getTime()) > 12 * 60 * 60 * 1000) { // 12時間以上前
      return pad2(d.getMonth() + 1) + "/" + pad2(d.getDate()) + " " + pad2(d.getHours()) + ":" + pad2(d.getMinutes());
    }
    return pad2(d.getHours()) + ":" + pad2(d.getMinutes()) + ":" + pad2(d.getSeconds()); // + "." + pad3(d.getMilliseconds());
  }

  self.recentness = function (time) {
    const diff = Date.now() - time;
    if (diff > 1800e3) { // 30 minutes
      return 3;
    } else if (diff > 180e3) { // 3 minutes
      return 2;
    } else if (diff > 70e3) { // 70 seconds
      return 1;
    } else {
      return 0;
    }
  }

  //// Option dialog //////////////////////////////////////////////////////////

  self.optionDialog = null;
  self.openOptionDialog = () => {
    if (self.optionDialog) {
      self.optionDialog.dialog('close');
      self.optionDialog = null;
      return;
    }

    let div = document.createElement('div');
    div.className = "plugin-livelocation-option";
    self.optionList.forEach(group => {
      let div1 = div.appendChild(document.createElement("div"));
      div1.className = "leaflet-bar-part plugin-livelocation-option-head";
      div1.textContent = group.desc;
      let div2 = div1.appendChild(document.createElement("div"));
      div2.className = "leaflet-bar-part plugin-livelocation-option-body";
      group.options.forEach(option => {
        let label = div2.appendChild(document.createElement('label'));
        if (group.type === 'func') {
          var div3 = div2.appendChild(document.createElement('div'));
          var btn = div3.appendChild(document.createElement("a"));
          btn.className = "leaflet-bar-part";
          btn.addEventListener("click", () => { option.func(option.args) }, false);
          btn.textContent = ' ' + option.desc;
        } else {
          let input = label.appendChild(document.createElement('input'));
          input.type = group.type;
          input.name = 'plugin-livelocation-' + group.slug;
          if (group.type === 'radio') {
            input.value = option.slug;
            if (option.slug === self.options[group.slug]) {
              input.checked = true;
            }
            input.addEventListener('click', () => {
              self.options[group.slug] = option.slug;
              localStorage[self.STORAGE_KEY + "-" + group.slug] = option.slug;
              console.debug(group.slug + ": " + option.slug);
            }, false);
          } else if (group.type === 'checkbox') {
            input.checked = self.options[option.slug];
            input.addEventListener('click', () => {
              localStorage[self.STORAGE_KEY + "-" + option.slug] = input.checked.toString();
              self.options[option.slug] = input.checked;
              console.debug(option.slug + ":", !!input.checked);
              self.redrawAllMarkers();
              if (self.dialog) {
                self.updateList();
              }
            }, false);
          }
          label.appendChild(document.createTextNode(' ' + option.desc));
          div2.appendChild(document.createElement('br'));
        }
      });
    });

    const prevServer = self.options.llServer;
    const prevSaverMode = self.options.saverMode;
    self.optionDialog = window.dialog({
      id: 'plugin-livelocation',
      html: div,
      title: 'Live Location Options',
      dialogClass: 'ui-dialog-locationList-option' + (window.isSmartphone() ? ' mobile' : ''),
      width: 240,
      closeCallback: () => {
        if (self.options.llServer != prevServer || self.options.saverMode != prevSaverMode) {
          self.setupSocket();
          if (!self.initTimer) {
            self.connectSocket();
          }
        }
        self.redrawAllMarkers();
        if (self.dialog) {
          self.updateList();
        }
        self.optionDialog = null;
        self._button3.classList.remove("active");
      },
    });
    self._button3.classList.add("active");

    let versionText = 'ver. ' + self.pluginVersion;
    if (self.serverVersion) versionText += ' / ' + self.serverVersion;
    const versionLabel = $('<label id="liveloc-option-version-text">').text(versionText).addClass("ui-widget");
    $('.ui-dialog-locationList-option .ui-dialog-buttonpane .ui-dialog-buttonset').prepend(versionLabel);
  };

  self.loadOptions = () => {
    self.optionList = [
      {
        desc: 'Show Role',
        type: 'checkbox',
        options: [
          { slug: 'filterRole_TL', desc: 'TL', default: true },
          { slug: 'filterRole_STL', desc: 'STL', default: true },
          { slug: 'filterRole_BIKE', desc: 'BIKE', default: true },
          { slug: 'filterRole_CAR', desc: 'CAR', default: true },
          { slug: 'filterRole_MEMBER', desc: 'MEMBER', default: true },
        ]
      },
      {
        desc: 'Display mode',
        type: 'checkbox',
        options: [
          { slug: 'saverMode', desc: 'Mobile mode', default: window.isSmartphone() },
        ]
      },
      {
        desc: 'Server',
        type: 'radio',
        slug: 'llServer',
        default: 'll.enjoy-with.me',
        options: [
          { slug: 'iitc.igres.app', desc: 'V1 server' },
          { slug: 'll.enjoy-with.me', desc: 'V2 server' },
          // { slug: 'x.bluecf.mydns.jp', desc: 'Test server #1' },
          // { slug: 'livelocation.ebisu-craft.net', desc: 'Test server #2' },
        ]
      }
    ];

    self.options = {};
    self.optionList.forEach(group => {
      if (group.type === 'checkbox') {
        group.options.forEach(option => {
          try {
            let opt = localStorage[self.STORAGE_KEY + "-" + option.slug];
            self.options[option.slug] = option.default ? opt !== 'false' : opt === 'true';
          } catch (e) {
            console.error(e);
          }
        });
      } else if (group.type === 'radio') {
        try {
          let opt = localStorage[self.STORAGE_KEY + "-" + group.slug];
          self.options[group.slug] = opt || group.default;
        } catch (e) {
          console.error(e);
        }
      }
    });
  };

  //// Initialize /////////////////////////////////////////////////////////////

  self.setTag = function () {
    var apitag;
    do {
      apitag = prompt('タグを入力してください / Enter tag name.', self.apitag || '');
      if (apitag) {
        apitag = apitag.split(',').map(x => x.trim()).filter(x => !!x).join(',');
      }
      if (apitag !== null && apitag !== '' && apitag != self.apitag) {
        if (!apitag) continue;
        self.apitag = apitag;
        window.localStorage[self.STORAGE_KEY + '-apitag'] = apitag;
        if (!self.initTimer) {
          self.connectSocket();
        }
      } else if (apitag === '') {
        self.apitag = '';
        delete window.localStorage[self.STORAGE_KEY + '-apitag'];
        if (!self.initTimer) {
          self.connectSocket();
        }
      }
      break;
    } while (true);
  }

  self.setupToolbar = function () {
    var button1 = document.createElement("a");
    button1.className = "leaflet-bar-part";
    button1.id = "marker";
    button1.addEventListener("click", function (e) {
      self.hideMarker()
      e.preventDefault();
      e.stopPropagation();
    }, false);
    self._button1 = button1;

    var button2 = document.createElement("a");
    button2.className = "leaflet-bar-part";
    button2.id = "list";
    button2.addEventListener("click", function (e) {
      self.showList()
      e.preventDefault();
      e.stopPropagation();
    }, false);
    self._button2 = button2;

    var button3 = document.createElement("a");
    button3.className = "leaflet-bar-part";
    button3.id = "filter";
    button3.addEventListener("click", function (e) {
      self.openOptionDialog();
      e.preventDefault();
      e.stopPropagation();
    }, false);
    self._button3 = button3;

    var container = document.createElement("div");
    container.className = "leaflet-control-liveLocation leaflet-bar leaflet-control";
    container.appendChild(button1);
    container.appendChild(button2);
    container.appendChild(button3);
    var parent = $(".leaflet-top.leaflet-left", map.getContainer());
    parent.append(container);
  }

  self.setupCSS = function () {
    $("<style>").prop("type", "text/css").html([
      '.plugin-live-location-marker-label{',
      '  color:#FFF;',
      '  font-size:13px;line-height:14px;',
      '  text-align:center;padding: 2px;',
      '  overflow:hidden;',
      '  text-shadow:1px 1px #000,1px -1px #000,-1px 1px #000,-1px -1px #000,0 0 5px #000;',
      '  pointer-events:none;',
      '}',
      'img.iconimg{height:41px!important;}',
      'img.iconimg.photo{height:60px!important;border-radius: 50%;}',
      '.agname{',
      '  width:100%;margin:0;padding:2;',
      '  position:absolute;top:8px;',
      '  word-break:break-all;',
      '}',

      '#locationList.mobile {',
      '  background: transparent;',
      '  border: 0 none;',
      '  height: 100%;',
      '  width: 100%;',
      '  left: 0;',
      '  top: 0;',
      '  position: absolute;',
      '  overflow: auto;',
      '}',
      '#locationList table {',
      '  margin-top: 5px;',
      '  border-collapse: collapse;',
      '  empty-cells: show;',
      '  width: 100%;',
      '  clear: both;',
      '}',
      '#locationList table td, #locationList table th {',
      '  background-color: #1b415e;',
      '  border-bottom: 1px solid #0b314e;',
      '  color: white;',
      '  padding: 3px;',
      '  white-space: nowrap;',
      '  vertical-align: middle;',
      '}',
      '#locationList table th, #locationList table td {',
      '  text-align: center;',
      '}',
      '#locationList table .alignR {',
      '  text-align: right;',
      '}',
      '#locationList table th.sortable {',
      '  cursor: pointer;',
      '}',
      '#locationList table .agent,',
      '#locationList table .role,',
      '#locationList table .chat {',
      // '  min-width: 120px;',
      // '  max-width: 240px;',
      '  overflow: hidden;',
      '  white-space: nowrap;',
      '  text-overflow: ellipsis;',
      '}',
      '#locationList table .agent {',
      '  color: #FFCE00;',
      '}',
      '#locationList .new {',
      '  background-color: #63F;',
      '}',
      '#locationList .old {',
      '  background-color: #900;',
      '}',
      '#locationList .older {',
      '  background-color: #960;',
      '}',
      '#locationList .oldest {',
      '  background-color: #666;',
      '}',
      '#locationList .sorted {',
      '  color: #FFCE00;',
      '}',
      '#locationList .filters {',
      '  display: grid;',
      '  grid-template-columns: 1fr auto 1fr auto 1fr auto 1fr auto;',
      '  grid-gap: 1px',
      '}',
      '#locationList .filters div {',
      '  padding: 0.2em 0.3em;',
      '  overflow: hidden;',
      '  text-overflow: ellipsis;',
      '  background-color: #0005;',
      '  white-space: nowrap;',
      '}',
      '#locationList .filters .count {',
      '  text-align: right;',
      '  min-width: 20px;',
      '}',
      '#locationList .filters .active {',
      '  font-weight: bolder;',
      '  color: #FFCE00;',
      '}',
      '#locationList .filters .filterAll {',
      '  display: none;',
      '}',
      '#locationList.mobile .filters .filterAll {',
      '  display: block;',
      '}',
      '#locationList.mobile .filters .name {',
      '  float: left;',
      '}',
      '#locationList .filters .filterRole {',
      '  background-color: #650;',
      '}',
      '#locationList .filters .filterChat {',
      '  background-color: #600;',
      '}',
      '/* 2 columns */',
      '@media (orientation: portrait) {',
      '  #locationList .filters {',
      '    grid-template-columns: 1fr auto 1fr auto;',
      '  }',
      '}',
      '#locationList .filterName.name:before {',
      '  content: "";',
      '  display: inline-block;',
      '  width: 11px;',
      '  height: 11px;',
      '  border-radius: 6px;',
      '  margin: auto;',
      '  margin-right: 0.2em;',
      '  vertical-align: -8%;',
      '}',
      '#locationList .filterChat:before {',
      '  background-color: yellow;',
      '}',
      '#locationList .filterRole:before {',
      '  background-color: red;',
      '}',
      '#locationList .table-container {',
      '  overflow-y: hidden;',
      '}',
      '#locationList .disclaimer {',
      '  margin-top: 10px;',
      '}',
      '.ui-dialog-locationList {',
      '  max-width: calc(100vw - 2px);',
      '}',
      '.ui-dialog-locationList.mobile {',
      '  top:0!important;',
      '}',
      '.ui-dialog-locationList button,',
      '.ui-dialog-locationList-option button {',
      '  margin-left: 8px;',
      '}',
      '.plugin-livelocation-option-head {',
      '  margin-bottom: 10px;',
      '}',
      '.plugin-livelocation-option-head:last-child {',
      '  margin-bottom: 0;',
      '}',
      '.plugin-livelocation-option-body {',
      '  margin-left: 6px;',
      '}',
      '.leaflet-container,',
      '.leaflet-control-liveLocation {',
      '  -webkit-user-select: none;',
      '  -moz-user-select: none;',
      '  -ms-user-select: none;',
      '  user-select: none;',
      '}',
      '.leaflet-control-liveLocation a#marker {',
      '  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAPlJREFUeNqUUzEOgkAQPNQH8AUbaks7oDOh0R/wA8NLxBdIa0dDLbxAegr1Bd4TnE32yGWzENxksrd7N3PL7RIYYVmWJXBn4Ci2auDaNE3rJwNBzuFuZt4KiJQuWP9JJjtEUfQZhqEfKwA5hHsBoVlmFtiiErviRK6Q6YYToxd7IXOME4jFgTeQ4oaaQGvO+Rb7AvJ2IloX8LpWqhgFpCULc2bDvhMHdnhY6kjB8YVygtv5FVSKOD3Sl5Er+9U4B+ipRW/pm/YL21jiXe7aJD6VUqW1IKcukI+YKj3XZkP/F7ypfCiV9DwbdlZgQkQlTwoIETNFJvsJMABzb1PdHuPURgAAAABJRU5ErkJggg==);',
      '}',
      '.leaflet-control-liveLocation a#list {',
      '  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAOJJREFUOE/Nk7EOgjAQhlsSFuJL6KrPYNhMgA7uPoBPIeLrsJg01w4mauLqqIOTT8BgXOHOlAQDCYTi5K3tff3/6/08CIIt5zxmPxQRJdwATK/jOOchDET0zf0vQClVgmyrergBEEKMbQBSymcrIAzDhDG26YHsACDuAsxsFADA7U8tCCGmNhaklPeuGUzaAJ7nZWmavupnQ39hDgCXXkCXhaIosjzP367rroloVANx602MosgnolPVbHJgtvcnACIutdb7RhZswkREB0RcaK2PlZJSgU2ciejBGFsppa71YX4AYVeiebatl14AAAAASUVORK5CYII=);',
      '}',
      '.leaflet-control-liveLocation a#filter {',
      '  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAStJREFUeNqkU7FxwkAQfAkKUAkmUQwZGVasBCpQCYgKPFSAKMEVmESxcAX+XAGEhCqB3Zl9z/HIM/Z4Z1a6P939792fEvcDyrJ8xavTsmjb9jwWl5qEF3Buvo3ajGFsWCchGa8vMAMb8BN8M4ke3IMrsAYHcAFV16kCMtEpoI6UcqMPs/6On/DR9/0tz3M6lu53aHD6+0MPJNtikOy9bDcWm6jblFiZmpkwwymDekR1F1Mme0IFPtVVHaKuH0MyIfsY9YQ5Xer+CW5QgDvJCqgk25kSKvPdK6dITNA6uiore2vqJzYo60RjapyrSF2mYRoDY092EueaxL+Ak+hTIzd0naO8iXri5WvieNsD/g8Zd9W61lURO/gbo3bgf/DQg+CITn2ywwEBdwEGAIoGZfrYoEPaAAAAAElFTkSuQmCC);',
      '}',
      '.leaflet-control-liveLocation a.active {',
      '  background-color: #BBB;',
      '}',
      '.leaflet-control-liveLocation-tooltip {',
      '  background-color: rgba(255, 255, 255, 0.6);',
      '  display: none;',
      '  z-index: 9800;',
      '  height: 24px;',
      '  left: 27px;',
      '  line-height: 20px;',
      '  margin-left: 15px;',
      '  margin-top: -12px;',
      '  padding: 0;',
      '  position:absolute;',
      '  margin-top: 0;',
      '  white-space: nowrap;',
      '  width: auto;',
      '}',
      '.leaflet-control-liveLocation-tooltip:before {',
      '  border-color: transparent white; border-style: solid;',
      '  border-width: 12px 12px 12px 0; content: ""; display: block; height: 0; left: -12px;',
      '  position: absolute; width: 0;',
      '}',
      '.leaflet-control-liveLocation a.active .leaflet-control-liveLocation-tooltip {',
      '  display: block;',
      '}',
      '.leaflet-control-liveLocation-tooltip.active {',
      '  display: block;',
      '}',
      '#filterMenu .active {',
      '  display: block;',
      '}',
      '#toolbarFilterRole table,',
      '#toolbarFilterChat table {',
      '  border-collapse: collapse;',
      '  empty-cells: show;',
      '  width: 100%;',
      '  clear: both;',
      '}',
      '#toolbarFilterRole table td, #toolbarFilterChat table td {',
      '  background-color: white;',
      '  border: 1px solid #0b314e;',
      '  color: black;',
      '  padding: 0;',
      '  text-align: left;',
      '  white-space: nowrap;',
      '  vertical-align: middle;',
      '}',
      '#toolbarFilterRole table label, #toolbarFilterChat table label {',
      '  padding-right: 5px;',
      '  width: fill;',
      '}',
    ].join('\n')).appendTo("head");
  }

  self.setupImages = function () {
    // 51px x 82px for Retina
    self.iconLeader = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAYAAAAWy4frAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJgklEQVR4nL3bW2wc1RkH8P/c1+t449gucQK5QAMJJRCqVKCiPgSpVYtUIdTHglKQolUAKar6lLwZVIm3tslDI7lVhRDJAxFVaFFDJCh+KAQlQOIQO3h9yXpt7817Ge/szsw5czl98O56vfau9zKT82Dv7sye8/3mO2cuZ2Y5xhi6LaMK1w/gWG8gcMwF+znHMKQTshMA+oJBaLpeXTeoKCnGIcOD+7RkmmMAxsKEqd3GwHUKGVW4/qCi/FaSxBMrxdKPh3fssHf/YEgcDPVBFiUMDw6srsgYGAdwDADHIZHLwbIsJHI5xJezTk7ThO3bem+uFEvnwoS9e98gowrX3xsIjBDben2oL8Qf3LtH3L9zJyRJArBaV7XGMmKtsVUMAFQ+praN+WQKU7EFO6MVXEWUzpdMc6TdLLUFeS+knLEdZ+SR4V3y4X37Mdgfqgm8GYJVQ98MA3DgACSyWXwTmUZaVYkkiW8dL5B3PIWMKtx+RZY+71UCe547/ISwa8dANYpWEIxVYt8aAwCJTBZfTk46JqXf64T8OkxYtGvIxf7gy6ZF/3Hk4UfkowcfA3NZ2whwqyu0gwEDrt29i+8XYtS2nV+GCRvrGPJ+KPCG5Trnnn/qiLB/13DniJrg2sVMx5fwxcSEI/HCqVcK5l/bhlQQLz77U2GwP9Q5ggOYy8Bxa19uF5MravjXtWtNMZtCRhXumCSJn3qFWOta3WE++uJL23acX2zWzfhNEPtFUbj6/FNHvEOUI6xuNA6r61Xq4lD5YK1esLXXHDCwrQ8/e/KwKPD8J+UDcHNIUFE+PrRnr9zVmKhDMI8wj+5+EI/v26cEFeV/TSHvhZQzAVk+9NwTP/IW4TLPMEcPHAAv8I+/Hwq8URt7dYyMKly/wPPJF559RunkONEQUQmaAeC51fFQft/pmElksrhy4wZxXHe4cgZQzUhvIDDywMAObxFuzZb2MDO7hgbxQH+/Ikvi6crSKsSk9OTRHx7wFsHVdS0PMUcfexQcx51cBxlVuFe39/aKu4YGfUHAB8yuwUH0KoFtowr3UhXSF+x5/clHHhb8QDCuXI8PmIN7HhL6gj1nqhBNN57Zv3OnP92p+k3vMXuHh2FQ+jQA8KMKd2zHtm106+uJLhC1QXiI6esJQJEkflThjvG9gcBLDw4Nyb4jfMLsHhgUZUn8Fc84/GRge2jdQq8RlX2jH5jB7SEokvQ0L/L8Q309Pb4iGPMPU07CAb5Q0vcNhPr8Q9TW5QNGFkRQ2x7iAUCWJP8yUb1M8AczGOoDodZ23rdM1DaIurq9zkylSt8RjPmKAdadxvuHwH3A8ALPk6Kh+4qoru8DJpHNIagoKT6gyFFNN3xH+IZZrTvDS6J4J5HN+Yfgapb5gElkcxB4fpxXteLHOU27D93JH0wik7WLunGJBzAWS6b9QdRcCPmBobaNVF4VAYzxYcKiAVlaiCXTviGqG8ljzHwyjVBv8GaYMJUvr3h+ejHuG6LhjEyXmOnYom05zt9RWZ1Q63x8OUOLhuE9gjFfMJphILtScA2TXKxCwoSpiixdvTUz530mKo17jBmfmoUkiRc2TAfpJjk1uxCHZhjeIeoBHmE03cDsUgImoW9X1qxCwoRFtwV7rt6OzPmC8BJze2YOiix9WHsDaN2UaVE3Ts4uJZBbKXiK2GpZO5hULo/oUooSap2ojX0dJExYNBhQzt6YjNiedqcWoK1ixqfnbF7g/1x/s3TDbLxukhFVK5ZiieXuEVjbGF5g5hbiUAvF0mtFqzpV2hASJkyVRPHNryembGrb3SFq/naLIZaNrycjLrXsV+tj3hQCAC+rxgXHdSduR+55gljXPTrEfDsZgSiK18KEXW4ZAgCEWi/NLsStVE71BNENJpVXMR9PU8MkrzSKtyEkTFiU47g/fTMRcbpFdJMZSi1cuznpiqIw0ux+e0MIALxWtE4bhMa/m77XXXeq3/u1gZmaX4TL2N3fabTpUxBNIQBAqPXinUgUJcPsfExwm+zKW8DktSLuRKIwCW3YpVqGhAm7FexRzn51667dWSbKn3aA+erbu06wRzkbJuxW1xAA0A0yomolLRJdbA/B1YLaw0SiiyCWlXpFNX/fSowtQcKEqZZl/+a7SNShxGofUQtoAVMyTHw3FbVNQl9uJb6WIWXMmCQI/7l+e6ptBNAe5vr4FBRZ+mCrB2k6ggCAQejxZCZP0lm1dQSHzSfVGmCWkhnkVjRS0s0324mtLUiYMFUQhLeuj0+5LSGwNp5awVDbxs07M7Ykin9o9wm6tiAA8KpG33Ecd2lien5rBGN1Xas5ZvreEhgwdbxAGj7O5BkEAAi1jkfmlmzLslpCbDl3ywEl3URkbslu5ZjhGSRM2JgiS59NRGKtIVqYiJ6MxKDI0metHDM8gwBASTdPzkTj0A2zOaLyvgmmZBAsJjO0pJsn0WHpGBImLCpL4oeTU7Gmmai+boIZn5iDLInnW3kIs1HpGAIA1LJPLCYzVDfI5giurmttglnOqEhnVGqYdKSbWLqChAlTZUk8PxmJNUWgCWY+vlzJhtpNLF1BAMAw6V9ii2noJmmIqJwB12NKBsFSPNN1NgAPIGHCogFF/vfdyEJTxGaY7yMxiIJwodtsAB5AAMAk9NRSPGNbtt0wE/XvqWUjtrgMQq23m1TdcvEEUt6DzcbT+Y1BM7YpLraQRrBHud7Nnqq2eAIBAMOkp2dmlugGRPl1PWbmXsLWjdYf4t+qeAYJE3a5pBOmrpQ2IFD+X8FkcgU4rltqNLXTSfEMAgCSKFxcWFzegFh3SsIBC7E0eHD/9LJtTyGEWufiyRzdDLHu3ng673g1yCvFU0iYsFuO7ZQK2upvquqfu2eMIZnMQ+D5lFeDvFI8hQCAJItXFhaW19/Qr3mdyRUA4KrX7XoO0XXyt3giZ1fe13etRDxHCbHOed2u55AwYWPUsp3qiWRNK7phglo26/Sao1nxHAIAAUUaz2br7nrxQDKlQpGlcT/a9AVCqf15NqtteBSwUNDhOM5lP9r0BWLbzifJRM5hDJVrWQAMy+kVk1L7ih9t+gIJEzbmMmalUnkAq1kpFHRYPo0PwCdIufw3lcxXj/DZrAaO5y771ZhvENdllxKJvFU5bY/eS+Uc2/3Ar/b8zMhlx3GlfE6DblKYphXw8iSxvvgGKV/1fZROrdhzs0mNMXbJr7YAfzMCAO+qaklMJvIAcNnPhjr+PXur5Y+HdieH55PaCcN91M92/g/o/vLs9Ziu8gAAAABJRU5ErkJggg==';
    self.iconSubLeader = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAYAAAAWy4frAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJr0lEQVR4nL3bW2wc1RkH8P/MzmW9jp3EDrFDuKSQcGlBoQqiFPUh9KK2UoVQHxu0BSkaBZBQ1Sd4M6gSb23JQ4MMVGgEPICoQosKUaH4oZA0kOsmMbZje7PrvXq9O/Z6dmbOzOzpw96dXXt3Z06/h/V6d3zO9/M5Z+acs7McpRReYzLM7QBweHAgeLgM+lOOYlfJtMYAYGgwhKJeqh8bCsoZyiHHg/tMN8wpAFOKSjWvOXD9QibD3I5QUP6NKAlHV9f074/fstO5dXyXMLpzCJIoYnz3SPVICgqAq1SHVDYP27aRyuaRTK+4ea0Y2D48eGF1TT+uqPTt/xtkMsztGBwITli2/eyuncP8vftvF/btHYMoigBXKatRZAVRr6z5sfIDhDi4kchg5nrcyRXWyrIontANc6LXVuoJoiryS47jTtx1+x7pgXv2YXR0uJH4ZgiOArSF0YIB5cBxQCqzgnOROWRzmiVKwsvhSetVXyGTYW6fLItfDA4Eb3/s4e8F9twyUsulKwQtAxyPrjBABfTVuWuuaZJvS6b1K0WlUc+Q946FjpiE/PXgfXdJhw7eA1qmPSPAAaC9YUCB0+en8e18jDiO+3NFpVN9Q95Rgs/Zrnv88UcPBvbdMd4/ovF2z5i5Gwl8+fVVVwwEXnhq0vxLz5Aa4omf/DAwOjrcP4IHqEvB1bLrA5NfLeLv/zq9KaYtZDLMHRZF4TO/EOABlOEZ89GprxzHdX/WrpvxbRD7BCFw6vFHD/qHqNZU/6dxqB5Ha3k2HmlTWfXnwMj2IfzoBw8IAZ7/tHoB3hwSCsof33f3HZKnMbEBUT/GI+bAnXtx/4E75VBQ/s+mEFWRXwoGpfsee/i7/iIo9Q1z6MH94AP8/e8oweeac6+PkckwtyPA8+lf/vgRuZ/rxGaIRm1c4yTmYcykMiv45IuvLbdcHq/NAOotMjgQnNh9y04mCFpPxp+W2TM2it27dsiSJLzYVFwlTIscO/TAfjaIMvUdc+jBA+A47lgLZDLMPb19eFDYMzbKBFE5/fqL2TM2isGB4LbJMPdkHTK0beDZB+//ToAFgnLVlxhg7r37tsDQtoGX6pDiuvHIvr1j7LoT2GDu2DsOwyQPAQA/GeYO79y+jWy5nvCEaCrXR8zQtiBkSeQnw9xhfnAg+OTesV0SewQbzK27RwVJEn7BUw4Pj4wMt7zpN6JxJvQfMzoyDFkSH+KFAH/b0OAAU0Rl7LHBjOwcBoD9/FqxdOfIjiF2iFq5jDCSIIDYzi4eACRJZIOgjWNZYUZHhmBZ9vZ6ekwQLZ2AHaZ6CNgjmpNigKlDWCNuSooBhg/wvLVeKjFFtE3KJ0wqk0coKGf4YFCKFtcN5ghmmErZOV4UhSupTJ4dgkNjYDLApDJ5BHj+Eq+trn+c14rsulM9UTaYVHrFWdeND3gAU7GlLBtE00KIBYYQB5mcJgCY4hWVRoOyGI/Fs8wQbVedPmBuxLMYHgpdUFSq1a4jJ+bmk8wQHXdkPGLm5pcc23HfRPUtWJZ9IpnOkXXd8B9BKRNMUTewkl8rG4b1Xh2iqFSTZfHUxSsL/rdE9bnfmEuReYiS8O5N20Elw3phfiGJom74h9gI8AlTLBqYX0zBNMkrtSPrEEWl0W2DA6cuX1lggvATc/naAmRZ/LD5A6CWLdN13Tg2v5hCvrDmK2Kr93rBZJYLiC5miGXZR5tzb4EoKo2GBuTXvj436/janbDFMT1gLkUWHD7A/2njh6U37caXDGtCW13XY/Fl7wg0/hl+YBYWk9C0df2Zt+z6VmlHiKJSTRSE5785N+MQx/GGaHr0irGIg2/Oz5YJcZ7emHNbCAAced141y2Xr16OLPqCaOkefWLOX5yFIAinFZWe7BoCAJZlPzk/n7Qzy5ovCC+YTE7DjWiWGIb1VKd8O0IUlUY5jvvjufOzrleEl5YhxMbpr66VBTEwsdnn7R0hAPDMW/aLhkGSkauL3rrTxrNfD5iZuSWUKZ3+7Rtk07sgNoUAgGXZT1yJRKHrpgdEm1N5F5jC6jquRKIwTdKxS3UNUVR6MRSSXztzZtrpF9G6r9w95syX024oJL+mqPSiZwgAlErWhKbpxdm5pb4QLftQXWJmZ5dgETvz1Ovm77rJsSuIolLNtp1fRy5HXWLZvSN49ITRSyYil6OOaZIj3eTXNaSKmRKFwD/P/nemZ0Stpm4xZ8/MQJbF97e6kaYvCAAYJgmn0wUrm9V6QtRn0l1gEks55PNFS9fN53vJrSeIolItEAi8fPbMTLlXRDcYYju48M11RxSF3/d6B11PEAB4+k3yquuWE1ev3ugaUYutMHOzCVBgJjxpdbydyTcIAFiWHZ6dTji2bXeFaLt03oDRdROz0wmnm2tGu+gLoqh0SpbFz69ejlVe2ArRaR+gCXMtEoMsi593c81oF31BAEDXzWPX55IolcwtWoLevCeFVoyuW1iK54ium8fQZ/QNUVQalSThw2uR2KYtAVSH8iaYS+cXIEnCiW5uwuwUfUMAgBDn6FI8R0q61R7Bo74w64RZzmjIpjViGGTCSy6eIIpKNUkSTlyLxOqvtUOg+rMd5kZ0udYampdcPEEAwDDIn2PRLEqG1RFRb6sNGF23kIjlPLdGrUhPoag0GgxK/5iOxDsjuPaYb6/EIAiBd722RrU472Ga5IVELOfYGzcrgMoEs7ZJV/sDSkGIg1h0GZZlv9KuzF7DF0j1DDafTBRuRoA2ulwTJraQRSgkn/VypmoOXyAAYBjkxevTCQJsQNDK842Y6zMpp1Tq/ib+rcI3iKLSk/q6RbWCfhMCQAsmt7wG1y3rnbZ2+gnfIAAgioH34ouVHcp2u+g1THw+C57j/uZn3b5CLMs+nlzKk/afazQwqWTB9WuQ18JXiKLSi67j6murle9UtZvGp5cKCAT4jF+DvBa+QgBAlIRP4gvLrVOWpue55TUAOOV3vb5DSrr1RjKed2q/b+xaqVieWKZ93O96fYcoKp0ixHFbJpLVWkolE4Q4tN81x2bhOwQAgkHx0sryWr2G2nolvaRBlsVLLOpkAiGW88VKptiYd1VXkGtaCa7rnmRRJxOI47ifppfyLi2jMY3nKJZTqyaxnE9Y1MkEoqh0qlymdiZZAFBplTWtBJvR+AAYQarx70yiUJ8krmSK4HjuJKvKmEHKZfpBKlawAQCUIjqbybtO+X1W9bFskZOuWxYLK0WUSgSmYQf9nCRuDGaQ6qrvo2xi1VmYThcppR+wqgtg2yIA8LaW04V0vAAAJ1lW1Pf32buNPxy9NT3upItH3y4fYFnP/wDRcSf/Xnc6owAAAABJRU5ErkJggg==';
    self.iconBike = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAYAAAAWy4frAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJmElEQVR4nL3bW2wc1RkH8P/cdta7XsfxmiQOCRhECJQgqFJRFbVSkFq1lRBCfSxRRKVoFUBCVZ/gzaBKvLUlD43kVhVFwAOIKrSoEAmKHyihAUpCbtixnfVtL/Zexrt75nLmcvqwO+tde+3s7szp9+I54539vp/PnD1ndsYCYwxBQ4hPDgM4Fo3FjzHP+zEEYdQy9L0AEBtMQK9Vm69VB2J5MFYQRPEjUydTAKYYSWmBa+gXIsQnh9WB2C9lSTlJauvf3T2yzxm9bb+cSCShKBGMJPcBAJgHwGOAKEAQgVIxC9u2USpmUSxm3GqlJMUHd31NauunGUm99n+DCPHJ4WgsPmFb1jNDu0bFg3cclveOjUNRFDC3+a7tCD8amPovBQCA41LkcwtYWpx2KusFT1HVM6ZOJnrtpZ4g6sjrL7qOMzF24O7I+F1HMJRINgvfCcEYIAjbYwSpvl0sZnFj+ito2qoly8pLVunEK6FChPjkuBJRP4lG4wcfOPKoNDI6BviHdYGov1joCgMAxWIW16585lLb/NYy9McZSaUDQ2J73nqKmuZf7j70UOTee4+CgfWBaKbrGgMBuH71PBbT31LXdX7KSGqqb0h09I1nXds+/dDRx6R9e8cDIvrDrCzdwNXL/3YlRXneLBz/Y88QH/GDHz0hDSWS/SMYwMAgNKvvHVOtlHD+07/viOkIEeKTx2RZ+SgsRGOIBMZ89ul7jus4P+l0mombdwjxyXFJks89dPSx8BB+u+2PxtBseqz+Pq3v625sgwGJoREcefCHsihJHzYm4J0h6kDs/TvG74sEGhNbEC37A2BuP3gId47fr6oDsU93hKgjr78YUaL3feeBR0NFsBAx99x7FKIg3h8dfePZ1tqbY0SITw6LkpR75Ps/V/ubJ3ZA+IcDGzNjgDFTLGbxxX8+sDzX3eevAJo9Eo3FJ3YP7+GD8F+/cVCgnkkmxzA8vEeVlcgL/tFNCLXMU/ccPsoFwYTwMYcOH4UgCKfaIEJ88ul4fJecTI5xQiB0TDI5hmg0PijEJ59sQgZiiWfuuvtBiQ+C1V/IAXPgwGFpIJZ4sQkx9Ooje8fGuZ1OzQNCxuzbfweoZTwMAKIQnzw2mNhNb3U9EQSxUWC4mKiagBJRRSE+eUyMxuJPju65PcIbwQuTvG2/LCuRn4lg7HtDgyNcEc3DOWCGBpNQFPVhURTlAwOxBF+EwLhhhnaNAMA9ok4qdyaGRvghWCMpJ4ysROA4dFQEAEWJcOuJ5mGcMInBJGxq7RKbSXggvI3jeGIAf4nCGdFaFBcMWtZaXBGbiuKBEUVJsnS9xhXRBHDAlEpZqAOxvBiJRNOGUeWO4IkBYwVRlpUrpVKWH8LDxtKHA6ZUykIUpUtiraq9X62V+J1OjbHHC1MsZR1Dr70jAphazS/yQbjN1Fwwjk2hlfMygCmRkVRaiUSXVtcWOSKavwwVs1pYQCw+9DUjKa3x8cvOZLI3uCG2fGSGhFlevuG4rv1noDGP2NQ6UyhkqKHXuCHCxhh6FZVq0bNM460mhJGUpijqufmbF/n0hLe5oOCYuZuXIMvKm1u+DrJM/flMdg6GvnG/LywEE1ioGMOsIpudA7XMl/132liikFR6IDZ4bj79DRdEmJj5+W+gRNR3W28AtX1laui1U9nsHCqVUqiITu1+MeVyHvlCmtrUOtlaexuEkVRajcZenZn7wgkLUb/wDO80m09fckRB/P3mm6Vbvo23TH2iRjSyVlgMZ0w0t4NjMpl51HSN2Ou/egGbYguEkZQmy8pz03NfOg6lwU4ntJ9aQTCObWFm/kvPsenTm2vuCAEAo/DUm57nXr259E04iBZAv5iZuf9CluXzjKTOdg0BAJtaT2Zyc7am5cNBBMBo5TxWiwvUMo3j29W7LYSRVFoQhN/NzH3lBkVs1Nc7xqYU12bPe5IkT+x0v/2W99kju/+6ePueQwfHDx4JgGBou5GDxobYvi0wofmn9W8CpZeuILM2e9UqndgooENs2yN+2NR6Ir18BaZFAiDqP3vtmVqtjPTyFVDL3PaU6hrCSOqiqsZevT77udM3ogXQC+b6zc9dVY29ykjqYmAIAFiWPkF0rbqcmekL0Xb12SVmOTMD27byZun4r7upsSsIIynNcexfpFcuu7ZLe0aI6A1jGgTplcsOtcynuqmva0gDMyXJyj+n5y/0jPATdYuZTl+Aoqhv3+pBmr4gAEAt40R5PWdpldWeEP7Y6gZT0FZQrZUs0yTP9VJbTxBGUpokSS9Npy/4ny1dI7rB2A7F7OLXjqwov+n1CbqeIABAtadf8Zi7kl652jUCm9rbYVbyNwCwaat0YtvHmUKDAIBNrRMrazOObdtdIrauCDZjDJNgZW3G6WbO6BR9QRhJTSmK+vFC/mqzzp0Q7RNjZ8xi7hoURf24mzmjU/QFAQDTJKcy+VkYpr4jQmCNgnfAmBZBQVumpklOoc/oG8JIKi0rkXcXc9du0RP17Z0wc8uXIMuRM908hLld9A0BAMemJwvaMjUsvSNCbNneDlOsrEGrrVJKjYkgtQSC1K8mI2cWctda93ZAYFvMWnnB7w0tSC2BIABAqfGH1dIiLEvHdojN842PMSyCgrYSuDeAECCMpNIRNfqPdP76johOmIXct5Bk+c2gvQGEAAEAapnPF7QVx3Hq80rnnmhv2w7FWnkRNrVeRggRCoSRVFqWI3MFLbO1Jxo9sHl/prgIVY1dCPJJ1RqhQACAUuOFleIsrbfaZ3up0WjFZAuzjmXpXT/Ef6sIDcJI6qxFCasQrbGjFYE2jFYpwINLtvtqp58IDQIAkqy8lS8vbUH4c4yPyWpLEETxb2HmDhViU+t0qZKhnRCtmHI164Y1yP0IFcJI6qILlxC9Um93WMbnyjmIkPJhDXI/QoUAgCwqH2TLS22I1u31WgEAzoWdN3SIRfU/lSoZx29vPrVKtSy1Xet02HlDhzCSmnI86uotC0k/iWHqcDzK+r3m2ClChwCAIkcvlSvFZgK3sSTJazkoknqJR04uEMeln6wbxea6S2R1jG5V4LruWR45uUBc1/mwRHKu2zKbiwxY19dMx6Mf8MjJBcJIaop5nr1Wzvt7UNErcDyby/gAOEEa8a9CNQ9/3VWsFiEIwlleybhBGPPeKZOsXd8G8pV0yfPct3nl49kjZz3PVcrVMkyqw3bMaJiLxM3Br0fqV33vrVVWnfTafJUx9g6vXADfHgGA1wjV5LKeA4CzPBP1/f/s3caB/b/NZSr7ql7t5CGeef4HAZv4Ku8sGtwAAAAASUVORK5CYII=';
    self.iconCar = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAYAAAAWy4frAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFyWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgOS4xLWMwMDEgNzkuYThkNDc1MywgMjAyMy8wMy8yMy0wODo1NjozNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDI0LjcgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wNi0xMFQxNDozOToyNCswOTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDgtMjRUMjA6NDA6MDUrMDk6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMjMtMDgtMjRUMjA6NDA6MDUrMDk6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjcyOGQ2MjAzLTAwYjQtODA0Yi05YzI5LWQ5MWJmNGE3NjY1YiIgeG1wTU06RG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmQ0OGRiMGNmLTYyNzUtYzQ0My04OTEzLTJiZjUzOTU0OTJhOSIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjk1YzczMzQwLWE4NzgtZWY0My04NTBjLWY4MDcxMDNlMjM3YyI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTVjNzMzNDAtYTg3OC1lZjQzLTg1MGMtZjgwNzEwM2UyMzdjIiBzdEV2dDp3aGVuPSIyMDIzLTA2LTEwVDE0OjM5OjI0KzA5OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjQuNyAoV2luZG93cykiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjcyOGQ2MjAzLTAwYjQtODA0Yi05YzI5LWQ5MWJmNGE3NjY1YiIgc3RFdnQ6d2hlbj0iMjAyMy0wOC0yNFQyMDo0MDowNSswOTowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDI0LjcgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pk9dqnAAAAdsSURBVHja1dt7UFRVGABwlJfPafM1aFk4ao1WSIKCAcpjeYk2qM1kI2BqzZij/aE1xBSWOCP7BzR77/aPqYugkKZTxqQyaY8pmQlznDQqHBw1EMMX4RPWXTp939l7mWVf7O49H8Qf3ywLu/ec3z3nu+fccw9BjLEgrbEwaKcOIjdzxH5jZnhVY0b4vnZ4zzCyR9Uw9WcM/Bt+Bj+L38HviqiDpspDpTbkjD5wFiu4/PGvrOtn/sKK5l5ixXFXmZxts0cWhtX+Cu+3xrXyz7wRWc+Wjj1kw+8qx3h9QCEIwLOZGrzHgpV/J6qJlad3M3lxDzNl28MVYe2DwVA/W57exfAYeCw8ptJSOlJIRlhlERaW/+T3bFviTaj8v0p4R5gyvWPw+3ic4rg2tmJcLUsevrsbyxIOgTMUmR669zJ2ha3xbQ4A3xBS5iOfMSoIy4Ku24hlC4Fkjaxeha2wfuYZZsphASFkCH8xGOum1TMsGzDJmiCYzGkhZtt7MZc1IWQNmHfnXGBYB6xLQBAVUZJ0SxPiEwRkWDRhtkMd+sN4yolkkQgThAhMSvAeq6du5jaxsV9q7U6OCJMgDHYzvKK5uzy761KNmGRiEd3CMFg3rKNXCF678bInGiGndwvD4OALV9Ie53zpM2JjswU6TnhDqCEKg+OMcxfrheDU4JXxtSQIE0xDRGNwBqAPqTC4QFCIUgqEpO8SjsG6wmyjsw8EZ56YG2QIPQ1GmT3n9kKyR9Y04AyUAiHrH/KgwLw96zzDuju2iE9T8UARVJjSlE4+F+MQHCmXjPncQomQ0h6QYeAixUd7frVaC3drlAg1KDB5T3zHr15BWSP2ndoS9ScpQibEbJp1DvOkLmjxqM+uuL/HFom4T4bBNQCAXOSJXoYHJkKYACGl3ifDbEu4zscTDqFsCY5IpcWgwQ4hRsip90gxDhBahDHlHimmD4QSoQYVhkNwsmhY+A8pQkq5S4YpntfCl2FxQGzCN5QINSgwWHdcSw5aMubg4fXTG8gQJgg5mQ6DdYc7xmo+hX9t0nGylkCEnHyHDLNMdwTnWrl81QSThRZxhwRTDt9VHlfo+DReH1rZUhjdTIwQj9n8fCODKdbZ3vsRGOKLXp10jAwhLeokwSzTfWmFRN/geGOFKygWQ+ItEoQaIjGGxJu4kmJRV1IcF+ZqC6acJEHIBJj8ySdZRlil2d26Fk/60oQbwhFGGHBFYrCOSpJHul1phOtxHUpFIyQIkZjVUEfI68Mel0zVVimZf1UoQhKIKZ57qU9ueFvENi6Hq4EohMxfO4RhVkDd0kLMBl8eK+jgZr6zEO7jxbQEIJI6hGA2zzqPCw2dPj/ogYnkKmgZ68dQuBBEknZM2cLbLC3Y3KOuLPr86A2S6dd1U38QgpCTbmvGFEScwFnuKb+fISpPrh598OJFzQg1AsVshTooCR4Z0FNdTKqlow/atCKkxMAxZTDbgG7eg9MoTc/Z4QAtb0XWa0JIUJlAMVg2jOCNmjcMQHNG8xE//poGxK2AMNtjr6gjeLSQLRxZOLY89oU1YERCYJic0QdsWLbIvSg6yJeOTdNPB4QwJtz0G4NlQZdqE747CJfu4SpmK3vpht8IGcIfTClMkVK9bA7QvM0pPayqduX4r/1GyH5ioBuz7BH7q8n2a2EXS8GHplEX/EJI0Iq+Ygpn/8ZSPOxuELqDTh9aUQQJ2OMPQo3+MGUL2hm0uhVyY8OA7GnkY8tTP/mF8AWDx/RlzBAGURLfWh7f7iPier+Y0tgWNcGjB3SXKSRj3Zop3/qEMC643i8mb1IdJnjdYGyXtd/jw5n0hpAX2MMbpjT2L0xwi6/7F4VCeOKHVBzGM+mtJeyQdq+YleNqWWZYlXFQNjA7XI4teEa9IaT4do+Y95/7Q20N3aBB+N0knMk8XAT3glDDHaYAvqu1NYRA1FwxxFzxgvjbLWZHzGUhrSEEouy8q8Uz6wkhc4grJn/icVyfMg/qJn/nVoEzay2bd9Utwhjniimb1+qyWjjoECVXmjbPOO2CkAEhxV1zwWx8up7BVKdBVPnCILhMkzOyxuIOIUM4YzLDKq2elnYGFWLfTrir+6MXmlwQshOmGGa4+hBzp8iyhUIwcddEnHBBSPPb+mDyJhwTluQkEJzwQZexOCPUUH+XBneaopKcBMLXwoLNHSXYvZwQahQ+c5ZlhO5tE12ucEh2+L7qtdC9HCtvhMuy+vO6iG8QYv7fQ/BeBa9Ijgg18D12vUDvOQYUgpECV68dc5p7AbLyWhrVzPBvFGWSQKB7NWyZ9rMDopW/boJb2azwqoYhA4FbVkPehKO88jIgpNhWjimA36WHVBQNGQi/px++x2ZUEGpAkndR5AcZhI/yw3Z1Fc44oyBaWMns3xn87iFVeZSQo/njj3IExsapP7LU4btrhhwEt0/B1P4RIowxLSwrrOq2yEniQELwP6rZh8+eY6Uw0i8a9ukDqrJIIQrmyJsRJ6yrJx67Cz/vHcqQ3OVjD+GU/S5ltyKHYLw8eXs7JH4zdTn/AVLVXaXbjOWYAAAAAElFTkSuQmCC';
    self.iconMember = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAYAAAAWy4frAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJlElEQVR4nL3bW2wc1RkH8P/c17u2E18KToBcKITQpgIUhJSoDwG1opUqGvWxoBSksDVEikZVH8KbQZV4a00eGmTaCiGSBxBVoKhJJCh+aBMUoImdxI43yWa9tvfu3fFe5j5z+uDd9dre3exlTr+nOaPxnO/nb47nzJkxQwhBt8GMT2wFcMjXGzhEHPcnYJlho6TeDwD+/j6o+UL1WCngT8IlGYZjv9CLpUkAk0QOKl3n0CmEGZ/YKgX8v+Z54WhpZeWpgZERe3j7dr5vcAiCKGJwZAQAQAgAlwAsA4YBsvE4LMtCNh7HcjzmFLJZLrBly5XSyspJIgff/79BmPGJrb7ewJilG6/1Dw+zD+15jL9/9y4IggDiVo4i6xGVKGPKZwIA2JaJ5Pw8FkJzdj6TcQWfdEovlsbarVJbEOndD95wbHts2/cfFnft24f+gaFq4s0QhGANUAfDsKvby4k4bn33HZR0yuB54U1j9MjbnkKY8YldgiR95QsEHvrhgYPc4PZtlXxbQqzuZ1rCAMByIo6Zixcd09BvGiX1F0QORrqG+P965kVT0//28JNPiHue2g8C0gFiLelWMWCA2UuXEL1503Rs+3kiByc7hvje+/B1x7ROPvHcs9zIjl1dIjrDLIVu4cbF/zicKBzXX33pz21DKogDv3yB6x8Y6hzhAoQhYNYybhtTyGZx6bPPmmLqQpjxiUO8IHzhFQIuABZdYy5+9qntWPZP611mbB3ELo7nLzzx3LPeISrtdb80gmrTrdmunNetORcB+gYHse/gj3mW586Xb8DNIVLA//mOvXvFrsbEJkTN/i4wD+x5FDv3Pi5JAf+/m0Kkdz94Q5R8e39w4KCnCMJ4h3lk/36wDPu4770PX6/NvTpGmPGJrSzPJZ55/udSZ/eJJohqZwDY8o4uxsxyIo5vzp8zXNsZqcwAqhXx9QbGBobvo4Nwysd7VJmhkW3Y+r37JF4UT1SOqkJMXR995On9VBCE9R7z6P79YBhmdB2EGZ94OdC/hR8a2UYH4cJzzNDINvgCgV5mfOJwFdLT1/fa7n0/4qggHLL6QxQwD+55jOvp63ujCtEKhWfu372LDqJ6Ou8xIzt3wNS0JwGAZcYnDvUODJj3ep7oBlFN0GOML9AHQZJYZnziEOvrDRwefvABkTaCFmboge08L4o/Y+GSp/u3DlJFVO9nFDD9A0MQJOlJluX5B3v6+igjCDVM/+AgADzCqvn8zr7VBh1EtVM6GF4SYZvmMAsAgihSq0TlWFqYvoEhWIaxha12QgNR0yFNDFCZolBG1CZFBYOauRZVxIakaGBYlucMtVikiqgCKGCyiTikgD/JipIvohUK1BE0MXBJhuUF4Xo2EaeHcLE29aGAySbiYDluii0qyueFXJbe5VROihZmORm3tWLxYxbAZGohSgfhVrOkgrFNE0oyyQOYZIkcjAg+30JqMUoPsXYCTzGpxXn4+/uvEDmosOUDT8XCt6gh1g1MDzGLt2/ZjmX9BSjfRyzDOJWJx0ytWKSG8Bqj5QvI55ZdQ9POVCFEDiqCKF0IT1+lU4mNCXmAuXNjCjwvnN60HGSo6vFY5A60mvd9XiFIvYS6wGjFAuJ378DU9bcqp1qbosjBSE9v74XwzDQVhJeY8PVpCJL0Se0LoHVLplqxOBq/ewf5XNZTRL12p5hcKonkUsS0DONobe7rIEQORiS//53Q1W9szxDu2vFeYMI3pmyWYf+08WXpptV4Q1XHiitKKb0U9aYSlV+GB5hYJIxiXilZx145gQ2xCULkoMILwrG5qW9t2zC7Q9Q8W3eLsQ0DoalvXds0X96Yc10IAGivvnjadZwbd2enPUFUNrrBhKb/C57nLxE5eLZlCABYhnE4Nn/HUlJJTxDdYJRkEqnYvGlo2kuN8m0IIXIwwjDMH0PT3zndItaGS/sYyzQxc+WSy3H8WLP37Q0hAGAde+WEaWixuzPXukMQ0jFm8fYciOvOmq/9pulXEE0hAGAZxguR0HXoWqljRGVfu5iikkMkdB2mrje8pFqGEDl4VerxvzN75Wu7U0R14LaJmZ3+2pF6/O8QOXi1awgAGJo6VsorhcVwqDMEQ9rGLIZDsAwjqf/2JbmVHFuCEDmo2Jb1q0jommNZZtsItInRSyVEQtdsU9dfbCW/liFlzCQnCP+cm77cPgJoCzN37TIEUfroXh/SdAQBAFPTjuTSCUPJpNpCVMdWC5hMagkFJWvoaulYO7m1BSFyUOE47s25a5fddhGtYGzLxO3ZKzYvCL9r9wu6tiAAYL7+8tuu6yzN37rRMqIS98IsRW4BhMwZo0cafs7kGQQALMM4shQN2ZZltYZw17YbYXS1hKVoyG7lnlEvOoIQOTgpiNKX0XC5KvdArFtUQ31MNDwDQZS+bOWeUS86ggCArpZGY/O3oWtqcwRBeTkWDTGGVkImuWjqamkUHUbHECIHI7wofhINzzSvRHm7GSYcmgIviKda+QizUXQMAQDbNI9mkoumoasNLiesv7TqYBQlDSWXMk1dG+sml64gq0+T4qno3Zm1nfUQQENMOjZfqYbSTS5dQQDA1LXxVDwKQ1UbIjY9f5cxhlZCJr3UdTUADyBEDkZEn+8fC/OzTRH1MNHITXA8f7rbagAeQADA1PXjmfSSbZtW40psaNuWiXQiCssw3tp8xvbDEwiRgxFeEO/kMrHNSZcnghv3p1JRSD3+y938paoNTyAAYOraiaWl2yaAzSuEdS63+MJt29DUlj/iv1d4BiFy8KyhlUipoKy2axHAOkxeycB1nVKjpZ1OwjMIAHCCcCadXtiEWDclcQlSmQUwHPt3L/v2FGIZxslsOmbWQ9Ricpm449Ugr4SnECIHrzrEKanF/Gq7zjQ+l0uAZbmkV4O8Ep5CAIDnhHPpzMI6RO12XskAwAWv+/UcYujqe9lMzK60N15a2VzctCzjpNf9eg4hcnDStkzHMNT1gxyArqmwLZN0+szRLDyHAIAg+abyK8urjZqHLkVJQBClKRp9UoHYtvlVobi8iqisqjMEqpqH4zhnafRJBeLY9vlsLuGAWVshIS6wspLWbds8R6NPKhAiByeJ61q5bLK8Y7UatmNRGR8AJUg5/pVbSVZXWQrFZTBgztLqjBqEuO7HuVzcAlbXvpKpSNZ1nY9o9UezImddxxEKhRxMXYVl6j4vJ4kbg15FVp/6Pl3Jp+xEKlwghHxMqy+AbkUA4P2SqvA5JQEAZ2l21PH/s7ca23//h0Rix0jBPX70UZr9/A+M1IaL/Fa9XwAAAABJRU5ErkJggg==';
    self.iconOther = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAABSCAYAAAD5CezVAAAACXBIWXMAAAsTAAALEwEAmpwYAAAHm0lEQVR4nM2bP2/bSBqHH/tgFVERsbBziIwkRgxlCwOnzlsYWF9qApf2qjjlVbvurju4vGqdT+DkE5wCTHNVtKU7yTYSh7FsyY4tR5IlKpHERBSiKzzUMYr+kZrJ7gsIHFIk5330e2c4My81gyIzTfMesA78BUgCMbntt7z8ZIAskBFCZFT4MDPNxRJgA/hbJBJJzs/PYxgG8/PzAL1tt9u9rmxmBtu2cV2XcrlMrVajXC7TbrfzQAp4Pg1YKBjTNNeBnyORyKN4PI738cxzvr8M10D9Zdu2OTk5IZ/P026308CWECId1K9AMKZpxoBfI5HIRiKRIJFIEIlEhjrfD9KrdAAQgOu6nJ+fc3BwQLPZTANPhBD5Sf2bGMY0zUfAzoMHD2IrKyvMzc2NdH5QeRhEf7ndbmNZFgcHBwCbQohtZTCmae5EIpGNtbU1FhYWJna+vzwOor9s2za7u7vUarUU1yrZoWFkWO0YhvHo4cOHPTXCgoQBcl2X3d1d3r17lwH+OgpodhQM8FIFSLfbDXUNwNzcHGtraywtLSWBl/IHDgZjmuaOYRhJFSAqyqurqz2gYT7/aQjIL4Zh/FMliN+ChJm/vLi4yPn5+Z/v3LkTsyzrv2NhTNNMAv9ZX18nGo0qAel2u185Nw3Q3bt3yeVyP96/fz9rWdah/56DwmxnZWWFWCymDKT/2DT3mZubY3V1FeDX/vbzFYxpmhvRaDS5srKiFGSS/SDleDzOwsLCPeCXoTDAvyS1cpBhx8PeV/r5s1+dHoxpmhuGYdzzHorfS5Gw949GoywtLcXwqeNX5nEikdAGogNoaWkJ4PFXMN5cJB6P/26KhKljfn6eaDR6T/bAPWUexeNxpc+UMG0mTF1y6vHYD/PT4uKiNpBRw5lpgSTMuh9m/ebNm9pAdF3X7Xa9UXwSYFZ2bTHDMLS3EV1AhmFgmub6LJBcWFj4boroAJJtPfbNcEYXiE4guXCSnAWSN27c0Aqi81x/eRaIRaPR79bYdcF7MIEdClLRoDDTBTQL2M1mUyuIyu9GnTsLZJrN5ncLLR1AfhjbdV3tIDqBSqUSQGZWCJGxbZt2u/27NfZpgWzbvoaRxzLlclkbiM6Qa7VauK5rCyHyHky6UqloBdEFdHFxAdcZhF7X/Pzi4kKrIrqACoUCwG89GCFEptls5iWlchBdP0Kr1cK+bjCpHoy0p7lcThuIDqDXr18DpLz1Zz/Ms3K53HuAqgTRcX273fbay5Z3rAcj6Z4eHh4OvPiPUPbv53I5XNdN+ZNR/WOz7UKhYNfrdeXOqARqt9scHR0BbPqPfwXjqbO3t6cURPW99vf3cV33WX+KcNBa83alUrGLxaJSEFVA9Xqd09NT8LWVoTBSnU1JrwSk2w2fbOov7+/vw3U2Oj8WRgI9a7Va6Vwu94cJrW63y+npKZVKJQ9sD/J7VBpw882bN9TrdSUg00DAdW5TZp+HJmoHZs4ALMu6TCQSM81mcz3oAuEwEL8FTTbt7e1Rq9VSQoh/D7vnuATtdqVSyR8fH490flxo9beZSe/lla+urjg7O7OBJ6OcHQkj5XxiWRatVisUyLDvJoGA6/DKZDJw3ejtUf4ODTPPLMvKLy8vxxzH+fH27duhQfwWJL95eHhIqVRKCyH+Mc7XcWHm2dbl5aV9eXk51Nmwioy6tl6vc3x8bDMmvDybCMYLt2w2S6fTCQUSBiibzQI8nfRloLFh5pllWYfLy8vJL1++/OC9RzbMiWH7MDrE/Ptv376lWCxmhBB/n9THScPMs82TkxO7Wq1+4+ykXfMkCjmOQz6fh76B5DgLBCPl3nr16lXgNhLk/Gw2i+u620FfoAuqDEKI7Q8fPmTkL6ek+/XvV6tVqtWqzYCB5DgLDCNt8+joCP/i4SgHhx0bdL4cSG6Oe6YMsok7AL/JZ0/y8+fPP9y6dWuoY/3lccOWo6Mj3r9/nxZCBGornoVVBmDz/Pwcx3GUjJRd1/WWjQKHl2ehYbzOQIbFV44NKo/7vlAoeHP6dFifplEGYLtarY7tqsdNzhzHGTinD2pTwXhrBtNO4iTIN3P6oDatMiDVqdVqQPAwcxznm/WvsDY1jKdOoVAIFWay0U+tCqhRBmC7VCrZjuMAwXqwYrEIClQBRTBSndTx8XGgMCsWi7ium1ahCqhTBmCrXC7T6XSAycJMrn89VeWAMhghRN513XSpVJoozGq1Go7j5IUQKVU+qFQG4PnZ2VlvZ1SYyR7sucrKlcIIIZ59/PjR7h/i9IdZp9NB5lCfqaxftTIAqWHJXq9cKpXodDoZVQ3fMx0wL2R3OzTMZDJYaYiBBhghRKrRaPSeOfAtlFQupbpuHcrAgFS8t5XHlYcY6IN5Ydv2wHmOHMOldVSqTRlv4Nlv8tWQFzoq1QIjhMh3Op18o9EA/t81dzodGo0G00zARpkuZQDS/aHmvbCjq0KdML9J53umM8RAL0yqUqn0Bp7Q68lSuirUBiOnBZlKpUK326XRaPDp0ydb1Z+yB5lOZQCeX11dASDTISmdlemG6YWahFI2dxlkU/2FfhIzTfNlLBZbt207L4RY0lmXbmUAtmQvNtWa2CT2P5MVWGctYMdiAAAAAElFTkSuQmCC';
  }

  self.onPaneChanged = function (pane) {
    if (pane == "plugin-liveLocation")
      self.showList(true);
    else
      if (!self.dialog) {
        $("#locationList").remove()
      }
  };

  self.saveChatFilter = function () {
    window.localStorage[self.STORAGE_KEY + '-filterChat'] = JSON.stringify(self.filterChat);
  }

  self.loadChatFilter = function () {
    try {
      self.filterChat = JSON.parse(window.localStorage[self.STORAGE_KEY + '-filterChat']);
    } catch (err) {
      console.warn(err);
      self.filterChat = {};
    }
  }

  self.layerAdded = function (obj) {
    const chat = Object.values(self.chats).find(chat => chat.layer == obj.layer);
    if (chat) {
      self.filterChat[chat.chatid] = false;
      self.redrawAllMarkers();
      self.updateList();
    }
  };

  self.layerRemoved = function (obj) {
    const chat = Object.values(self.chats).find(chat => chat.layer == obj.layer);
    if (chat) {
      self.filterChat[chat.chatid] = true;
      self.redrawAllMarkers();
      self.updateList();
    }
  };

  self.setup = function () {
    self.STORAGE_KEY = 'iitc-plugin-live-location';
    self.APIKEY = 'f9cbeb05-da97-f62a-9269-fdd1169721dc';

    try {
      self.apitag = window.localStorage[self.STORAGE_KEY + '-apitag'] || '';
    } catch (err) {
      console.error(err);
      self.apitag = '';
    }
    console.info("apitag:", self.apitag);

    self.setupCSS();
    self.setupImages();

    self.loadChatFilter();
    self.loadOptions();
    self.setupToolbar();

    if (window.useAndroidPanes && window.useAndroidPanes()) {
      android.addPane("plugin-liveLocation", "Live Location list", "ic_action_view_as_list");
      window.addHook("paneChanged", self.onPaneChanged);
    }
    $('#toolbox').append('<a onclick="window.plugin.liveLocation.showList()" title="Location list">Location list</a>');
    $('#toolbox').append('<a onclick="window.plugin.liveLocation.setTag()" title="LiveLoc SetTag">LiveLoc SetTag</a>');

    map.on('layeradd', obj => {
      self.layerAdded(obj);
    });
    map.on('layerremove', obj => {
      self.layerRemoved(obj);
    });

    map.on('zoomstart', obj => {
      Object.values(self.locations).forEach(location => {
        if (!location.marker || !location.marker._icon) return;
        location.marker._icon.style.transition = '';
        location.marker._icon.style.transform = '';
        location.marker.setLatLng(location.latLng);
      })
    });

    map.on('spiderfy', ev => {
      console.debug('spiderfy:', ev);
    });

    map.on('unspiderfy', ev => {
      console.debug('unspiderfy:', ev);
    });

    self.setupSocket();
    if (self.error) return;

    self.initTimer = setTimeout(() => {
      self.initTimer = null;
      self.connectSocket();
      if (self.error) return;

      setInterval(() => {
        self.updateAllMarkers();
        if (self.dialog) {
          self.updateList();
        }
      }, 3000);
    }, 3000);
  };

  var setup = self.setup;

  setup.info = plugin_info; //add the script info data to the function as a property
  if (!window.bootPlugins) window.bootPlugins = [];
  window.bootPlugins.push(setup);
  // if IITC has already booted, immediately run the 'setup' function
  if (window.iitcLoaded && typeof setup === 'function') setup();

  // PLUGIN END //////////////////////////////////////////////////////////
}
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
