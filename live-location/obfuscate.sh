#!/bin/sh -x

usage() {
    echo "usage: $0 <inputfile> [ -o <outputfile> ] [ <obfuscator_options> ]"
}

TMP="/tmp/.obfuscator.$$"

if [ -z "$1" -o "$1" = "--help" ]; then
    usage
    exit
fi
filepath=$1
filename=`basename $filepath`
shift
if [ "$1" = "-o" -a -n "$2" ]; then
    userpath=$2
    shift 2
else
    userpath="obfuscated_$filename"
fi
metapath=`echo $userpath | sed 's/user\.js$/meta.js/'`

date=`date +%Y%m%d.%H%M%S`

rm -f ${TMP}-*
sed -e '/@version/s/\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\)/\1.'"$date/" \
    -e '/@description/s/\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\)/\1.'"$date/" \
    $filepath > ${TMP}-source.js

sed '/^$/,$d' ${TMP}-source.js > ${TMP}-header.js
sed -n '/^$/,$p' ${TMP}-source.js > ${TMP}-body.js

sed '/PLUGIN START/,$d' ${TMP}-body.js > ${TMP}-body1.js
sed -n '/PLUGIN START/,/PLUGIN END/p' ${TMP}-body.js > ${TMP}-body2.js
echo "" > ${TMP}-body3.js
sed '1,/PLUGIN END/d' ${TMP}-body.js >> ${TMP}-body3.js

npx javascript-obfuscator ${TMP}-body2.js --output ${TMP}-obfuscated.js "$@"

outdir=`dirname $userpath`
if [ ! -d $outdir ]; then
    mkdir -p $outdir
fi
rm -f $userpath
cat ${TMP}-header.js ${TMP}-body1.js ${TMP}-obfuscated.js ${TMP}-body3.js > $userpath
if [ $userpath != $metapath ]; then
    rm -f $metapath
    cp ${TMP}-header.js $metapath
fi
rm -f ${TMP}-*
